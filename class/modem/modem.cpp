/****************************************************************************
 *                               Includes
*****************************************************************************/
#include "extern.h"
#include "stdlib.h"
#include "string.h"
#include "modem.h"
#include <stdio.h>
#include <string.h>
#include <time.h>
//#include "iwdg.h"
#include "usart.h"
#include <map>
#define xDBG_MODEM

using namespace std;


MODEM *pMODEM;
extern RTC_API *pRTC_API;
extern DATA_CLASS *pDataClass;
extern UART_HandleTypeDef huart2;
extern uint8_t uart2_rx_modem[512];
extern uint16_t uart2_rx_cnt;
extern void delay_ms(uint16_t time);

void ReplaceStringInPlace(std::string& subject, const std::string& search, const std::string& replace);
vector<string> split2(string s, string divid);

int pick_integer(char* str);

//Mutable Replace 원본 문자열을 수정한다. 속도가 우선일 경우 사용하자.
void ReplaceStringInPlace(std::string& subject, const std::string& search, const std::string& replace) {
  size_t pos = 0;
  while ((pos = subject.find(search, pos)) != std::string::npos) {
    subject.replace(pos, search.length(), replace); pos += replace.length();
  }
}

vector<string> split2(string s, string divid) {
	vector<string> v;
	int start = 0;
	int d = s.find(divid);
	while (d != -1){
		v.push_back(s.substr(start, d - start));
		start = d + 1;
		d = s.find(divid, start);
	}
	v.push_back(s.substr(start, d - start));

	return v;
}

int pick_integer(char* str) { /* 문자열에서 숫자만 추출하는 함수 */
	int res=0; //숫자를 추출해 저장
	int i;
	for(i=0; str[i]!='\0';i++){
		if(str[i] >= 48 && str[i] <=57){
			res = res*10 + (str[i]-48);
		}
	}
	return res;
}

MODEM::MODEM(){
  memset(&modem_uart_process,0,sizeof(modem_uart_process));
  modem_info.telephon_number="";
  modem_info.ready=0;
  modem_uart_process.uart_recv_timeout=10;
  modem_process_seq=MODEM_SEQ_FAILE;//
}

MODEM::~MODEM(){
}

uint8_t MODEM::AT_CHECK_ERROR(string str){
	uint8_t err=1;
	if (str.find("OK") != std::string::npos) {
		err=0;
	}
	return err;
}

string MODEM::AT_CMD(const char *req){
	string str;
	uint8_t pData[512]={0,};
	int strLen= strlen(req);
	uart2_rx_cnt=0;
	memset(&uart2_rx_modem,0,sizeof(uart2_rx_modem));
	HAL_NVIC_DisableIRQ(USART2_IRQn);
	HAL_UART_Transmit(&huart2, (uint8_t*)req, strLen, strLen);
	HAL_UART_Receive(&huart2, pData, 512, 100);
	str = (char *)pData;

	HAL_NVIC_EnableIRQ(USART2_IRQn);
	__HAL_UART_ENABLE_IT(&huart2,UART_IT_RXNE);
	return str;
}
//return RET_OK;
//return RET_ERR;
uint8_t MODEM::MODEM_isRDY(void){
  string res;
  string req="AT\r\n";
  uint8_t ok=0;
  res=AT_CMD(req.c_str());
  if (res.find("OK") != std::string::npos) {
	ok=1;
	if(modem_info.ready==0) modem_info.ready=1;
  }
#ifdef  DBG_MODEM
 if(ok==0) printf("RDY -->Failure!!\r\n");
#endif
  return ok;
}

TYPEDEF_TIME_DATA MODEM::MODEM_GET_DATETIIME(void){
	TYPEDEF_TIME_DATA ltime_date;
	struct tm timeinfo;
	struct tm *pTimeinfo;

	time_t  now,modem_local_time=0;
	string str;
	string modem_current_date_time;
	vector<string> v,w;
	int sz;
	char temp[64]={0,};
	uint16_t date_time[16]={0,};
	str=AT_CMD("AT+CCLK?\r\n");
#ifdef  DBG_MODEM
//	printf("time str -->%s\r\n",str.c_str());
#endif
	ReplaceStringInPlace(str,"\n","");//"\r"제거
	ReplaceStringInPlace(str,"\r","");//"\n"제거
	ReplaceStringInPlace(str,"+","\"");//"\n"제거
	//ReplaceStringInPlace(str,",","\\");//"\n"제거
	ReplaceStringInPlace(str,"/",",");//"/"->,
	ReplaceStringInPlace(str,":",",");// :->,
	if (str.find("CCLK") != string::npos) {
		w=split2(str, "\"");
		v=split2("0,"+w[3], ",");//LGU+ 7개배열형식맞춤
		sz=v.size();
		if(sz==7){
		  for(int i=0;i<7;i++){
			  sprintf(temp,"%s",v[i].c_str());
			  int val=pick_integer(temp);
			  //printf("[%d][%d]\r\n",i,val);
			  date_time[i]=val;
		  }

		}
		if(date_time[1]>2099) goto ERR;
		if(date_time[2]>12) goto ERR;
		if(date_time[3]>31) goto ERR;
		if(date_time[4]>60) goto ERR;
		if(date_time[5]>60) goto ERR;
		if(date_time[6]>60) goto ERR;

		//timeinfo.tm_wday = myDate.WeekDay;
		//1900년 기준
		timeinfo.tm_year =(2000+date_time[1])-1900;
		timeinfo.tm_mon = date_time[2];
		timeinfo.tm_mday =date_time[3];
		timeinfo.tm_hour =date_time[4];
		timeinfo.tm_min = date_time[5];
		timeinfo.tm_sec = date_time[6];
		//modem_local_time = mktime(&timeinfo);
		//RTC_DateToBinary// 70년/01웡/01일기준
		modem_local_time = (time_t)pRTC_API->RTC_DateToBinary(&timeinfo); //unit of second
		//	printf("(1)modem_local_time (%x)\r\n",(uint32_t)modem_local_time);
		//UTC->KST
		modem_local_time +=(9*3600);//
		timeinfo =pRTC_API->RTC_BinaryToDate((uint32_t)modem_local_time);
	}
					  //2085892096 //1478183379
	//if(modem_local_time>2000000000) goto ERR;
#ifdef  DBG_MODEM
//	printf("tm_year -->%d\r\n",timeinfo.tm_year);
//	printf("tm_mon  -->%d\r\n",timeinfo.tm_mon);
//	printf("tm_mday -->%d\r\n",timeinfo.tm_mday);
//	printf("tm_hour -->%d\r\n",timeinfo.tm_hour);
//	printf("tm_min  -->%d\r\n",timeinfo.tm_min);
//	printf("tm_sec  -->%d\r\n",timeinfo.tm_sec);
//	printf("(2)GetTime -->%d  \r\n",(uint32_t)modem_local_time);
#endif
	ltime_date.ltime=(uint32_t)modem_local_time;
	ltime_date.timeinfo=timeinfo;
	return ltime_date;
ERR:
	ltime_date.ltime=0;
	return ltime_date;
}

uint8_t MODEM::MQTT_SET_CONFIG(void){
	uint8_t err=0;
	err= err|AT_CHECK_ERROR(AT_CMD("AT+QSSLCFG=\"cacert\",0,\"ca.pem\"\r\n"));
	err= err|AT_CHECK_ERROR(AT_CMD("AT+QSSLCFG=\"clientcert\",0,\"cc.pem\"\r\n"));
	err= err|AT_CHECK_ERROR(AT_CMD("AT+QSSLCFG=\"clientkey\",0,\"ck.pem\"\r\n"));
	err= err|AT_CHECK_ERROR(AT_CMD("AT+QSSLCFG=\"seclevel\",0,2\r\n"));
	err= err|AT_CHECK_ERROR(AT_CMD("AT+QMTCFG=\"SSL\",0,1,0\r\n"));
	err= err|AT_CHECK_ERROR(AT_CMD("AT+QMTCFG=\"version\",0,4\r\n"));
//AT_CMD("AT+QMTCFG=\"keepalive\",0,3600\r\n");
	return err;
}

void MODEM::modem_init(void){
  uint8_t time_out_cnt=0;
  modem_info.ready=0;
}
void MODEM::MQ_DISCONN(void){
   string command="AT+QMTDISC=0\r\n";
   int strLen = command.length();
  HAL_UART_Transmit(&huart2, (uint8_t*)command.c_str(), strLen, strLen);
}

void MODEM::MQ_OPEN_URL(void){
   string command="AT+QMTOPEN=0,\"a14xbcv33glzp0-ats.iot.ap-northeast-2.amazonaws.com\",8883\r\n";//sokoban
   int strLen = command.length();
  HAL_UART_Transmit(&huart2, (uint8_t*)command.c_str(), strLen, strLen);
}

void MODEM::MQ_CONNECTION(void){
   //string command="AT+QMTCONN=0,\"client0\"\r\n";
   string command=format("AT+QMTCONN=0,\"%s\"\r\n",modem_info.telephon_number.c_str());
   int strLen = command.length();
   HAL_UART_Transmit(&huart2, (uint8_t*)command.c_str(), strLen, strLen);
}

void MODEM::MQ_SUBSCRIBE(void){
	string	command=format("AT+QMTSUB=0,1,\"Hskb/%s\",0\r\n",modem_info.cpuid.c_str());
   int strLen = command.length();
   HAL_UART_Transmit(&huart2, (uint8_t*)command.c_str(), strLen, strLen);
}

//+MTRECV: 0,0,"Hskb/066AFF495282494867214927","{"b":"0080","t":"FFFE","jp":"0100","jt":"0120","o":"0990"}"
void MODEM::MqttSubscribe_parse(char *message){
	string str_msg= message;
	string res;
	printf("#RECV#\r\n%s\r\n",str_msg.c_str());
	if (str_msg.find("MTSTAT:") != std::string::npos && modem_process_seq==MODEM_SEQ_MQTT) {
		modem_process_seq=MODEM_SEQ_CONN;
		modem_process_wait=0;
		mqtt_conn_wait=0;
		modem_uart_process.b.mt_conn=0;
		modem_uart_process.b.mt_open=0;
	}
	else if(str_msg.find("MTCONN: 0,0,0") != std::string::npos) {
		modem_uart_process.b.mt_conn=1;
		printf("---MTCONN OK---\r\n");
	}
	else if(str_msg.find("MTOPEN:") != std::string::npos)
	{
		 if(str_msg.find("MTOPEN: 0,0") != std::string::npos) {
			modem_uart_process.b.mt_open=1;
		}
		else {
			modem_uart_process.b.mt_open=0;
			modem_uart_process.b.mt_conn=0;
			modem_process_wait=0;
		}

	}
	else if(str_msg.find("MTRECV:") != std::string::npos) {
		recv_message=MQTT_RECV(str_msg);
	}

}

//===============================================
//Bit 0 : system reset
//Bit 1 : LCD ON-OFF
//Bit 2 : JaeSang ON/OFF
//===============================================
//void MODEM::MQTT_SUBSCRIBE_CTRL(uint16_t data){
//	MODEM_RELAY rData;
//	uint8_t i;
//	//modem_uart_process.b.mq_reponse_redraw=1;
//	rData.u16=data;
//	uint8_t mask=rData.u8[1];
//	//uint8_t ctrl=rData.u8[0];
//	printf("mask[%02x]\r\n", mask);
//	for(i=0;i<8;i++){
//		if(_CheckBit(mask,i)){
//			switch(i){
//				case 0:	if(rData.b.ctrlb0)	NVIC_SystemReset(); break;//SW RESET
//				case 1://LCD ON-OFF
//					modem_uart_process.b.mq_reponse_redraw=0;
//					if(rData.b.ctrlb1)	pSTM32_IO->vKEY_BUFFER.push_back(MQ_LCD_ON_KEY);
//					else pSTM32_IO->vKEY_BUFFER.push_back(MQ_LCD_OFF_KEY);
//					break;
//				case 2://jesang mode control
//					modem_uart_process.b.mq_reponse_redraw=0;
//					if(rData.b.ctrlb2)	pSTM32_IO->vKEY_BUFFER.push_back(MQ_JESANG_ON_KEY);
//					else pSTM32_IO->vKEY_BUFFER.push_back(MQ_JESANG_OFF_KEY);
//					break;
//			}
//		}
//	}
//}

string MODEM::MQTT_RECV(string message){
	uint8_t i;
	uint8_t key_value;
	string str,strId, sData;
	map< string, int > m;
	enum RECV_ENUM {
		ENUM_PWR=0,
		ENUM_RES,
		ENUM_TMP,
		ENUM_OZO,
		ENUM_OZC,
		ENUM_OZD,
		ENUM_OZP,
		ENUM_JSP,
		ENUM_JSD,
		ENUM_JSF,
		ENUM_JSC,

	};
	m["PWR@"] = ENUM_PWR;
	m["RES@"] = ENUM_RES;
	m["TMP@"] = ENUM_TMP;
	m["OZO@"] = ENUM_OZO;
	m["OZC@"] = ENUM_OZC;
	m["OZD@"] = ENUM_OZD;
	m["OZP@"] = ENUM_OZP;
	m["JSP@"] = ENUM_JSP;
	m["JSD@"] = ENUM_JSD;
	m["JSF@"] = ENUM_JSF;
	m["JSC@"] = ENUM_JSC;

	vector<string> v;
	v=split2(message, ",");
	short iData;
	modem_uart_process.b.mq_reponse_redraw=1;//REDRAW ENABLE
	for(i=0;i< v.size();i++){
		str=v[i];
		if(v[i].find("@") != std::string::npos){;}
		else continue;
		ReplaceStringInPlace(str,"{","");
		ReplaceStringInPlace(str,"}","");
		ReplaceStringInPlace(str,"\n","");
		ReplaceStringInPlace(str,"\r","");
		ReplaceStringInPlace(str,"\"","");
		ReplaceStringInPlace(str,":","");
		ReplaceStringInPlace(str," ","");

		strId=str.substr(0,4);
		sData=str.substr(4);
		iData = stoi(sData);
		printf("[%s][%d]\r\n", strId.c_str(), iData);
		switch(m[strId])
		{
		case ENUM_RES:
			modem_uart_process.b.mq_reponse_redraw=0;
			if(iData)NVIC_SystemReset();//SW RESET
			break;
		case ENUM_TMP://온도 설정
			printf("ENUM_TMP iData[0x%04x][%d]\r\n", iData, iData);
			pMain_page->UI_Data.target.temp = iData;
			break;
		case ENUM_OZO://oz 설정
			printf("ENUM_OZO iData[0x%04x][%d]\r\n", iData, iData);
			pMain_page->UI_Data.target.o3 = iData;
			break;
		case ENUM_JSP://js period
			if (iData < 0)	iData = 0;
			else if (iData > MAX_DEF_PERIODE)iData = MAX_DEF_PERIODE;
			pMain_page->UI_Data.data_arry[JAESANG_PAGE].idata[2]=iData;
			break;
		case ENUM_JSD://js duration
			if (iData < 0)iData = 0;
			else if (iData > MAX_DEF_TIME) iData = MAX_DEF_TIME;
			pMain_page->UI_Data.data_arry[JAESANG_PAGE].idata[3]=iData;
			break;
		case ENUM_OZP://oz period
			if (iData < 0)	iData = 0;
			else if (iData > MAX_O3_GEN_PERIODE)iData = MAX_O3_GEN_PERIODE;
			pMain_page->UI_Data.data_arry[JAESANG_PAGE].idata[5]=iData;
			break;
		case ENUM_OZD://oz duration
			if (iData < 0)iData = 0;
			else if (iData > MAX_O3_GEN_TIME) iData = MAX_O3_GEN_TIME;
			pMain_page->UI_Data.data_arry[OZONE_PAGE].idata[6]=iData;
			break;
		case ENUM_PWR://power on/off
			modem_uart_process.b.mq_reponse_redraw=0;
			key_value=(iData)? MQ_LCD_ON_KEY:MQ_LCD_OFF_KEY;
			pSTM32_IO->vKEY_BUFFER.push_back(key_value);
			break;
		case ENUM_JSF://강제제상
			modem_uart_process.b.mq_reponse_redraw=0;
			key_value=(iData)? MQ_JESANG_ON_KEY:MQ_JESANG_OFF_KEY;
			pSTM32_IO->vKEY_BUFFER.push_back(key_value);
			break;
		case ENUM_JSC://js control mode
			pMain_page->UI_Data.data_arry[JAESANG_PAGE].idata[1] = iData%2;
			break;
		case ENUM_OZC://js control mode
			pMain_page->UI_Data.data_arry[OZONE_PAGE].idata[1] = iData%2;
			break;
		}
	}

	if(modem_uart_process.b.mq_reponse_redraw)pSTM32_IO->vKEY_BUFFER.push_back(ADC_MENUX1_KEY);
	modem_uart_process.b.mq_reponse_flag=1;//Flash writer
	return str;
}

void MODEM::MQTT_PUBLISH_RESPONSE(string message){

  string str_res_pup=format(
	  "AT+QMTPUBEX=0,0,0,0,\"skb\",\""
	  "\"cpu\":\"%s\","
	  "\"inx\":200,"
	  "\"res\":\"%s\""
	  "}\" \r\n",
	  modem_info.cpuid.c_str(),
	  message.c_str()
  );
  AT_CMD(str_res_pup.c_str());
}

string MODEM::MQTT_MK_DATA_CHUNK(){
	uint8_t i,sum,idx=0x01;
	char pData[512]={0,};
	RELAY_DATA relay_state;
	relay_state.b.RY1=pDataClass->reg.relay[0].on;
	relay_state.b.RY2=pDataClass->reg.relay[1].on;
	relay_state.b.RY3=pDataClass->reg.relay[2].on;
	relay_state.b.RY4=pDataClass->reg.relay[3].on;
	relay_state.b.RY5=pDataClass->reg.relay[4].on;
	relay_state.b.RY6=pDataClass->reg.relay[5].on;
  string str_chunk=format(
	  "%s|"//\","
	  "%s|"//\"imei\":
	  "%s|"//"\"cpu\":\"%s\","
	  "%02X|"//"\"inx\":%02X,"
	  "%04X|%04X|%04X|%04X|"//"\"av\":%04X, \"aa\":%04X, \"ap\":%04X, \"af\":%04X,"
	  "%02X|"//"\"jm\":%02X,"
	  "%02X|"//"\"om\":%02X,"
	  "%04X|"//"\"jp\":%04X,"
	  "%04X|"//\"jd\":%04X,"
	  "%04X|"//\"op\":%04X,"
	  "%04X|"//\"od\":%04X,"
	  "%02X|"//\"rs\":%02X,"
	  "%04X|"//\"o1\":%04X,"
	  "%04X|%04X|%04X|"//\"t1\":%04X,\"t2\":%04X,\"t3\":%04X,"
	  "%04X|"//\"h1\":%04X,"
	  "%04X|"//\"w1\":%04X,"
	  "%04X|%04X|%04X|"//\"sjt\":%04X, \"sjo\":%04X, \"sjw\":%04X,"
	  "%04x|",//\"ss\":%04X"
	  modem_info.telephon_number.c_str(),//pub.id.c_str(),
	  modem_info.imei.c_str(),//pub.imei.c_str(),modem_info.imei.
	  modem_info.cpuid.c_str(),//pub.cpuid.c_str(),
	  idx,//
	  0&0xFFFF,0&0xFFFF,0&0xFFFF,0&0xFFFF,//pub.ac_v, pub.ac_a,  pub.ac_p, pub.ac_f,
	  (int)pDataClass->reg.parm.jaesang_mode&0xFF,//pub.js_mode,
	  (int)pDataClass->reg.parm.ozone_control_mode&0xFF,//pub.o3_mode,
	  (int)pDataClass->reg.parm.jaesang_total_peroid&0xFFFF,//pub.js_period,
	  (int)pDataClass->reg.parm.jaesang_on_duration&0xFFFF,//pub.js_duration,
	  (int)pDataClass->reg.parm.ozone_total_period&0xFFFF,//pub.o3_period,
	  (int)pDataClass->reg.parm.ozone_on_duration&0xFFFF,
	  (int)relay_state.u8&0xFF,
	  (int)pDataClass->measured_ozone&0xFFFF,//pub.o31,
	  (int)pDataClass->measured_temperature&0xFFFF,(int)pDataClass->measured_inp_temp&0xFFFF,pDataClass->measured_oup_temp&0xFFFF,
	  (int)pDataClass->measured_humidity&0xFFFF,
	  (int)pDataClass->measured_wind&0xFFFF,
	  pMain_page->UI_Data.target.temp&0xFFFF,//pub.temperature_seljung,
	  pMain_page->UI_Data.target.o3&0xFFFF,//pub.ozone_seljung,
	  pMain_page->UI_Data.target.wind&0xFFFF,//pub.wind_seljung,
	  pDataClass->reg.sysFlag.u16&0xFFFF//pub.sys_state.u16
);
  uint8_t len=str_chunk.length();
  sprintf(pData,"%s",str_chunk.c_str());
  sum=0;
  for(i=0;i<len;i++){
	 sum=sum+ pData[i];
  }
  str_chunk=format("%s%02X",str_chunk.c_str(),sum);
//  printf("#####[%d][%d] sum[%x]\r\n",len, str_chunk.length(), sum);
//  printf("%s\r\n",str_chunk.c_str());
//  printf("#####\r\n");
  return str_chunk;
}

string MODEM::MQTT_PUBLISH_EX(){

	string res;
	string zz;
	RELAY_DATA relay_state;
//	FLAG_STATE sys_state;
//	modem_info.mqtt_retry++;
	relay_state.b.RY1=pDataClass->reg.relay[0].on;
	relay_state.b.RY2=pDataClass->reg.relay[1].on;
	relay_state.b.RY3=pDataClass->reg.relay[2].on;
	relay_state.b.RY4=pDataClass->reg.relay[3].on;
	relay_state.b.RY5=pDataClass->reg.relay[4].on;
	relay_state.b.RY6=pDataClass->reg.relay[5].on;

	zz= MQTT_MK_DATA_CHUNK();//for test
	string str_pup=format( "AT+QMTPUBEX=0,0,0,0,\"skb\",\"{\"zz\":\"%s\"}\"\r\n", zz.c_str());

//  string str_pup=format(
//	  "AT+QMTPUBEX=0,0,0,0,\"skb\",\""
//	  "{\"id\":\"%s\","//0
//	  "\"imei\":\"%s\","//1
//	  "\"cpu\":\"%s\","//2
//	  "\"inx\":1,"//3
//	  "\"av\":%d, \"aa\":%d, \"ap\":%d, \"af\":%d,"
//	  "\"jm\":%d,"//8
//	  "\"om\":%d,"//9
//	  "\"jp\":%d,"//10
//	  "\"jd\":%d,"//11
//	  "\"op\":%d,"//12
//	  "\"od\":%d,"//13
//	  "\"rs\":%d,"//14
//	  "\"o1\":%d,"//15
//	  "\"t1\":%d,\"t2\":%d,\"t3\":%d,"
//	  "\"h1\":%d,"//19
//	  "\"w1\":%d,"//20
//	  "\"sjt\":%d, \"sjo\":%d, \"sjw\":%d,"
//	  "\"ss\":%d",//24
//	  "\"zz\":\"%s\""
//	  "}\" \r\n",
//	  modem_info.telephon_number.c_str(),//pub.id.c_str(),
//	  modem_info.imei.c_str(),//pub.imei.c_str(),modem_info.imei.
//	  modem_info.cpuid.c_str(),//pub.cpuid.c_str(),
//	  0,0,0,0,//pub.ac_v, pub.ac_a,  pub.ac_p, pub.ac_f,
//	  (int)pDataClass->reg.parm.jaesang_mode,//pub.js_mode,
//	  (int)pDataClass->reg.parm.ozone_control_mode,//pub.o3_mode,
//	  (int)pDataClass->reg.parm.jaesang_total_peroid,//pub.js_period,
//	  (int)pDataClass->reg.parm.jaesang_on_duration,//pub.js_duration,
//	  (int)pDataClass->reg.parm.ozone_total_period,//pub.o3_period,
//	  (int)pDataClass->reg.parm.ozone_on_duration,
//	  (int)relay_state.u8,
//	  (int)pDataClass->measured_ozone,//pub.o31,
//	  (int)pDataClass->measured_temperature,(int)pDataClass->measured_inp_temp,pDataClass->measured_oup_temp,
//	  (int)pDataClass->measured_humidity,
//	  (int)pDataClass->measured_wind,
//	  pMain_page->UI_Data.target.temp,//pub.temperature_seljung,
//	  pMain_page->UI_Data.target.o3,//pub.ozone_seljung,
//	  pMain_page->UI_Data.target.wind,//pub.wind_seljung,
//	  pDataClass->reg.sysFlag.u16,//pub.sys_state.u16
//	  zz.c_str()
//);
   // printf("#####[%s]",str_pup.c_str());
  //  printf("%s\r\n",str_chunk.c_str());
  //  printf("#####\r\n");
 // MQTT_MK_DATA_CHUNK();//for test

  res=AT_CMD(str_pup.c_str());
  return res;
}

#if 0
      return RET_OK;
      return RET_ERR;
#endif


//AT+CNUM
//+CNUM: ,"01231329318",129
//OK
string MODEM::MODEM_GetPhoneNumber(void){
 string res="";
 string str="@";
 vector<string> v;
 res=AT_CMD("AT+CNUM\r\n");
 v=split2(res, ",");
 for (string s : v) {
	 if (s.find("\"") != string::npos) {
        str=s;
        ReplaceStringInPlace(str,"\"","");//"""제거
        ReplaceStringInPlace(str,"+","");//"+" 제거
        str=(str.length()!=11)? "@":str;
       // printf("-----[%d] [%s]---\r\n",str.length(), str.c_str());
        break;
      }
 }
  return str;
}

string MODEM::MODEM_GetIMEI(void){
	string res="@";
	vector<string> v;
	res=AT_CMD("AT+GSN\r\n");
	ReplaceStringInPlace(res,"\n","");//"""제거
	v=split2(res, "\r");
	if(v.size()>2){
		res =v[2];
		ReplaceStringInPlace(res," ","");//"""제거
	}
	return res;
}

 void MODEM::MODEM_POWER_ON(uint8_t on){
 	//modem_info.mqtt_retry=0;
 	if(on)HAL_GPIO_WritePin(pMODEM_EN_GPIO_Port, pMODEM_EN_Pin, GPIO_PIN_RESET);//2N3904
 	else HAL_GPIO_WritePin(pMODEM_EN_GPIO_Port, pMODEM_EN_Pin, GPIO_PIN_SET);
 }

 void MODEM::MODEM_HW_RESET(){
 #ifdef  DBG_MODEM
 		printf("---Modem_HW_RESET---\r\n");
 #endif
 	//------power on-------
 	MODEM_POWER_ON(0);
 	HAL_Delay(150);
 	MODEM_POWER_ON(1);
 	//--------------------
 	modem_info.ready=0;
 	modem_process_seq=MODEM_SEQ_INIT;
 	modem_process_wait=0;
 }

void MODEM::Modem_Process(void){
	uint8_t res;
	uint8_t err=0;
	string tel_number;
#ifdef  DBG_MODEM
printf("Modem_Prosess modem_process_seq[%d] modem_process_wait[%d] mqtt_periode_wait[%d]\r\n",modem_process_seq, modem_process_wait, mqtt_pub_periode_wait );
#endif
	switch (modem_process_seq){
	case MODEM_SEQ_INIT:
		modem_process_wait++;
		if(modem_process_wait>MODEM_POWER_ON_DELAY){
			modem_info.ready=0;
			//modem_info.mqtt_retry=0;
			modem_process_wait=0;
			modem_process_seq=MODEM_SEQ_IMEI;
		}
		break;
	case MODEM_SEQ_IMEI:
		modem_process_wait++;
		res=MODEM_isRDY();
		if(res>0){
			for(int i=0;i<4;i++){
				modem_info.telephon_number=MODEM_GetPhoneNumber();
				modem_info.imei=MODEM_GetIMEI();
				if (modem_info.telephon_number.find("@") != std::string::npos || modem_info.imei.find("@") != std::string::npos) continue;
				else {
						printf("##TEL :%s\r\n",modem_info.telephon_number.c_str());
						printf("##IMEI:%s\r\n",modem_info.imei.c_str());
						//modem_info.telephon_number=tel_number;
						modem_process_seq=MODEM_SEQ_TIME;
						break;
				}
			}
		}
		else{
			if(modem_process_wait>2){
				modem_process_seq=MODEM_SEQ_FAILE;
			}
		}
		break;
	case MODEM_SEQ_TIME://set time
		pDataClass->nowTimeInfo = MODEM_GET_DATETIIME();
		err=MQTT_SET_CONFIG();
		if(pDataClass->nowTimeInfo.ltime !=0 && err==0) {
			if((pDataClass->nowTimeInfo.timeinfo.tm_year+1900)<2022){
				modem_process_seq=MODEM_SEQ_IMEI;
				modem_process_wait=0;
				break;
			}
#ifdef  DBG_MODEM
		printf("##Get_Modem_Current_Date_Time year[%d][%d][%d]\r\n",
				pDataClass->nowTimeInfo.timeinfo.tm_year+1900,
				pDataClass->nowTimeInfo.timeinfo.tm_mon,
				pDataClass->nowTimeInfo.timeinfo.tm_mday);
#endif
			//set_RTC
			pHYM8563->Set_RTC(pDataClass->nowTimeInfo.timeinfo);
			modem_process_seq=MODEM_SEQ_CONN;
			modem_process_wait=0;
			mqtt_conn_wait=0;
			modem_disconn_cnt=0;
			modem_uart_process.b.mt_conn=0;
			modem_uart_process.b.mt_open=0;
		}
		break;
	case MODEM_SEQ_CONN://mqtt_open url
		if(modem_process_wait==0){
			modem_uart_process.b.mt_open=0;
			modem_uart_process.b.mt_conn=0;
			MQ_DISCONN();
			if(modem_disconn_cnt++>5) modem_process_seq=MODEM_SEQ_FAILE;
		}
		if(modem_process_wait==5){
			MQ_OPEN_URL();
		}
		if(modem_uart_process.b.mt_open){
			MQ_CONNECTION();
			modem_uart_process.b.mt_open=0;
			if(mqtt_conn_wait++>5) modem_process_seq=MODEM_SEQ_FAILE;
			break;
		};

		if(modem_uart_process.b.mt_conn){
			MQ_SUBSCRIBE();
			mqtt_pub_periode_wait=0;
			modem_info.ready=1;
			modem_process_seq=MODEM_SEQ_MQTT;
		}

		if(modem_process_wait++>20){
			modem_process_wait=0;
		}
		break;
	case MODEM_SEQ_MQTT:
		if(mqtt_pub_periode_wait==0){
			printf("--MQTT_PUBLISH_EX--\r\n");
			err= AT_CHECK_ERROR(MQTT_PUBLISH_EX());
			if(err){
				printf("MTPUB ERROR!! ;)\r\n");
				MQ_DISCONN();
				mqtt_conn_wait=0;
				modem_uart_process.b.mt_conn=0;
				modem_uart_process.b.mt_open=0;
				mqtt_pub_periode_wait=0;
				modem_process_seq=MODEM_SEQ_CONN;
			}
		}
		if(mqtt_pub_periode_wait++>MQTT_PUB_PERIODE){
			mqtt_pub_periode_wait=0;
			if(!MODEM_isRDY())modem_process_seq=MODEM_SEQ_FAILE;
		}
		if(modem_uart_process.b.mq_reponse_flag){
			modem_uart_process.b.mq_reponse_flag=0;
			MQTT_PUBLISH_RESPONSE(recv_message);
			pFlashClass->SPI_FLASH_UPDATE_SELJUNG();
		}

		break;
//----------------------------------
//Modem Power OFF Process (9,10,11)
//----------------------------------
	case MODEM_SEQ_FAILE:
		//POWER OFF
		modem_process_wait++;
		if(modem_process_wait>1){
			MODEM_POWER_ON(0);
			modem_process_wait=0;
			modem_process_seq=MODEM_SEQ_POWERON;
		}
		break;
	case MODEM_SEQ_POWERON:
		//POWER ON
		modem_process_wait++;
		if(modem_process_wait>2){
			MODEM_POWER_ON(1);
			modem_process_seq=MODEM_SEQ_RESET;
		}
		break;
	case MODEM_SEQ_RESET:
		MODEM_HW_RESET();
		modem_process_seq=MODEM_SEQ_INIT;
		break;
	default:
		break;

	}
}



