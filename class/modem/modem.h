#ifndef MODEM_H
#define MODEM_H
#include "stm32f1xx_hal.h"
#include <string>
#include <ctime>
#include "extern.h"

using namespace std; 

//#define MQTT_PUB_PERIODE 30*SECOND
#define MQTT_PUB_PERIODE     1*MINUTE
#define MODEM_POWER_ON_DELAY 20*SECOND

typedef struct {
	uint8_t on_rx:1;
	uint8_t mq_discon:1;
	uint8_t mt_open:1;
	uint8_t mt_conn:1;
	uint8_t mq_reponse_redraw:1;
	uint8_t mq_reponse_flag:1;
}MODEM_DOING_BIT;

typedef struct _MODEM_RELAY_BIT
{
	uint16_t ctrlb0:1;
	uint16_t ctrlb1:1;
	uint16_t ctrlb2:1;
	uint16_t ctrlb3:1;
	uint16_t ctrlb4:1;
	uint16_t ctrlb5:1;
	uint16_t ctrlb6:1;
	uint16_t ctrlb7:1;
	uint16_t maskb0:1;
	uint16_t maskb1:1;
	uint16_t maskb2:1;
	uint16_t maskb3:1;
	uint16_t maskb4:1;
	uint16_t maskb5:1;
	uint16_t maskb6:1;
	uint16_t maskb7:1;
}MODEM_RELAY_BIT;

typedef union _MODEM_RELAY
{
	MODEM_RELAY_BIT b;
	uint16_t u16;
	uint8_t  u8[2];
}MODEM_RELAY;

typedef struct _MODEM_LM5
{
  uint8_t ready;
  string telephon_number;
  string imei;
  string cpuid;
}MODEM_LM5;

typedef struct _MODEM_POST
{
  MODEM_DOING_BIT b;
  uint16_t uart_recv_timeout;
  uint16_t uart_recv_try_cnt;
}MODEM_POST;

//+MTRECV: 0,0,"Hskb/066AFF495282494867214927","{"b":"0080","t":"FFFE","jp":"0100","jt":"0120","o":"0990"}"
//typedef struct _MQTT_SUB
//{
//  MODEM_RELAY control;
//  short d1;//sjtemp
//  short d2;//sjo3
//  short d3;//sjjsperiod
//  short d4;//sjjsduration
//}MQTT_SUB_RCV;

class MODEM
{
	private:
		//MQTT_SUB_RCV mq_rcv;
		bool mq_reponse_flag=false;
		int modem_disconn_cnt=0;
		int modem_openurl_cnt=0;
		string recv_message;

		void MODEM_POWER_ON(uint8_t on);
		void MODEM_HW_RESET();
		void get_cpuid();
		string AT_CMD(const char *req);
		uint8_t AT_CHECK_ERROR(string str);
		uint8_t MODEM_isRDY(void);
		uint8_t MQTT_SET_CONFIG(void);
		string MQTT_PUBLISH_EX(void);
		void MQTT_PUBLISH_RESPONSE(string message);
		string MODEM_GetPhoneNumber(void);
		string MODEM_GetIMEI(void);
		TYPEDEF_TIME_DATA MODEM_GET_DATETIIME(void);
		void MQ_DISCONN(void);
		void MQ_OPEN_URL(void);
		void MQ_CONNECTION(void);
		void MQ_SUBSCRIBE(void);
		string MQTT_RECV(string message);
		//void MQTT_SUBSCRIBE_CTRL(uint16_t data);
		string MQTT_MK_DATA_CHUNK();
	public:
		MODEM_POST modem_uart_process;
		MODEM_LM5 modem_info;
		int modem_process_seq;
		int modem_process_wait=0;
		int mqtt_pub_periode_wait=0;
		int mqtt_conn_wait=0;

		MODEM();
		virtual ~MODEM();
		void modem_init(void);
		void Modem_Process(void);
		void MqttSubscribe_parse(char *message);

};

#endif 
