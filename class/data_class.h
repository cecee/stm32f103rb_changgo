/*
 * GUILIB.h
 *
 *  Created on: Jan 19, 2022
 *      Author: thpark
 */

#ifndef DATA_CLASS_H_
#define DATA_CLASS_H_

#define ONE_DATA_PUSH_DELAY  30*MINUTE

//typedef enum{
// // SYSTEM_STATE_NORMAL = 0,
// // SYSTEM_STATE_TEST,
// // SYSTEM_STATE_POWER_OFF
//}SYSTEM_STATE;





class DATA_CLASS {
private:
	void display_sysFlag_message();
public:

	TYPEDEF_TIME_DATA nowTimeInfo;
	vector<RECORD_UNIT> vRec3month;
	vector<RECORD_UNIT> vRec;
	REGISTER reg_alarm[2];
	STATUS_REGISTER reg;
	int sys_sec_tick;
	uint8_t try_jaesang_sensor=0;
	uint8_t try_ozone_sensor=0;
	int measured_temperature;
	int measured_humidity;
	int measured_inp_temp;
	int measured_oup_temp;
	int measured_wind;
	int measured_ozone;
	uint8_t RS485_temperature_sensor_exist=0;
	uint8_t RS485_ozone_sensor_exist=0;
	uint8_t RS485_wind_sensor_exist=0;
	uint16_t record_periode_tick=0;

    bool Buzzer=0;
	DATA_CLASS();
	virtual ~DATA_CLASS();
	void SYSTEM_POWER(uint8_t on);
	void SYSTEM_ChangeToNormal();
	void SET_CTRL_REGISTER(RELAY relay, uint16_t count, uint8_t relay_on);
	uint32_t U32_between(uint32_t A, uint32_t B);
	void update_hangon_condition(int measure);
	void OneSecond_prosess(void);
	void update_draw_calrendar(void);
	void RecDataPush(void);
	void Get_PushedRecData_FromEEPROM(void);
	//void Get_AdcData();
	void Modem_Prosess(void);
	void update_OneDay_Recoder();
	void update_system_local_time();
	//void Exchange_System_State();
	void update_relay_action();
	void update_error_check();
	void update_local_mode();
	void get_wind_parameter(int raw_wind, int wind_temp_adc, uint8_t sensor);
	void Get_Parameter();
	void define_alarm(int measure, int seljung);
};

#endif /* DATA_CLASS_H_ */
