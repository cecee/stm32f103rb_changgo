/*
 * JAESANG.h
 *
 *  Created on: 2022. 3. 12.
 *      Author: thpark
 */

#ifndef JAESANG_JAESANG_H_
#define JAESANG_JAESANG_H_


#include "extern.h"


typedef enum{
  JS_PERIOD  = 0,
  JS_SENSOR  = 1
}jsMODE;


class JAESANG
{
public:
	//JAESANG_INFO jaesang_info;
	uint8_t vWind_sz=0;
	uint8_t jaesang_period_seq=0;
	uint8_t jaesang_sensor_seq=0;
	uint16_t jaesang_period_wait=0;
	uint16_t jaesang_sensor_wait=0;
	//uint16_t jaesang_max_wind_reference=0;
	uint16_t jaesang_wind_off_avr=0;

	JAESANG();
	virtual ~JAESANG();
	void ctrl_jaesang_heater();
	//void update_jaesang_parameter();
	void Jaesang_mode_exchange(uint8_t system_state);
	uint8_t Jaesang_period_control(void);
	uint8_t Jaesang_force_control();
};

#endif /* JAESANG_JAESANG_H_ */
