/*
 * JAESANG.cpp
 *
 *  Created on: 2022. 3. 12.
 *      Author: thpark
 */
#include "extern.h"
#include "jaesang_heater.h"

#define xDBG_JAESANG
#define REFRENECE_MAX 30
JAESANG *pJAESANG;

JAESANG::JAESANG()
{
	vWind_sz=0;
}

JAESANG::~JAESANG()
{
	// TODO Auto-generated destructor stub
}



void JAESANG::ctrl_jaesang_heater(){
	uint8_t jaesang_ctrl=0x80;
	if(pDataClass->reg.local_jeasang_ctrl_mode==0){//주기모드
		jaesang_ctrl=Jaesang_period_control();

	}
	else{
		jaesang_ctrl = (pDataClass->reg.parm.decision_heater_on_flag) ? 1:0;
	}
	//온도에 따라서
	//측정온도가 설정온도보다 낮고 온도편차가 2도(?) 이상나면
	if(pDataClass->reg.sysFlag.b.SUPER_COOL_PROTECT){
		jaesang_ctrl=1;
	}

	//온도센서이상++
	if(pDataClass->reg.sysFlag.b.TEMPERATURE_SENSOR_ERROR || pDataClass->reg.sysFlag.b.WIND_SENSOR_ERROR){
		jaesang_ctrl=0;
	}
	//온도센서이상--

	if(pDataClass->measured_oup_temp>150){
		jaesang_ctrl=0;
	}

	//강제재상 ++
	if(pDataClass->reg.sysFlag.b.JAESANG_FORCE_ON){
		jaesang_ctrl=Jaesang_force_control();
	}
	//강제재상 --

	//테스터모드 ++

	//테스터모드 --

#ifdef DBG_JAESANG
	printf("\r\nJAESANG_HEATER[%d] HANGON_ON_FLAG[%d] JAESANG_FORCE_ON[%d]\r\n",jaesang_ctrl, pDataClass->reg.sysFlag.b.SUPER_COOL_PROTECT, pDataClass->reg.sysFlag.b.JAESANG_FORCE_ON);
#endif
	switch(jaesang_ctrl){
		case 0: //off
			if(pDataClass->reg.parm.jaesang_heater_on_flag==ON){
				pDataClass->reg.parm.jaesang_heater_on_flag=OFF;
				pSTM32_IO->PutRelayEventVector(HEATER_RY,OFF,0);
			}
			break;
		case 1:
			if(pDataClass->reg.parm.jaesang_heater_on_flag==OFF){
				pDataClass->reg.parm.jaesang_heater_on_flag=ON;
				pSTM32_IO->PutRelayEventVector(HEATER_RY,ON,0);
			}
			break;
		default:
			break;

	}



}


uint8_t  JAESANG::Jaesang_period_control(){
	uint8_t jaesang_ctrl=0x80;

	int jaesang_period=pDataClass->reg.parm.jaesang_total_peroid;//pMain_page->UI_Data.data_arry[1].idata[2] * MINUTE;//on time(분)
	int jaesang_on_period=pDataClass->reg.parm.jaesang_on_duration;//pMain_page->UI_Data.data_arry[1].idata[3]* MINUTE;//total제상주기(분)
	int jaesang_start_time=jaesang_period-jaesang_on_period;

#ifdef DBG_JAESANG
	printf("제상주기모드 총주기[%d] 제상시간[%d] 제상시작시간[%d] tick[%d] jaesang_period_seq[%d]\r\n",jaesang_period, jaesang_on_period, jaesang_start_time, jaesang_period_wait, jaesang_period_seq, jaesang_info.jaesang_current);
#endif
	if(jaesang_start_time<10){
		//No Control JAESANG
		jaesang_ctrl=0;
		jaesang_period_seq=10;
	}
	pDataClass->reg.parm.jaesang_current++;
	switch(jaesang_period_seq){
		case 0:	//제상 OFF
			jaesang_ctrl=0;
			pDataClass->reg.parm.jaesang_current=0;
			jaesang_period_seq=1;
			break;
		case 1:
			jaesang_period_wait++;
			if(jaesang_period_wait<jaesang_start_time){
				jaesang_ctrl=0;
#ifdef DBG_JAESANG
	//			printf("제상 ON 기다림 [%d/%d]\r\n",jaesang_period_wait,jaesang_start_time);
#endif
			}
			else{
				//pDataClass->reg.sysFlag.b.JAESANG_EVENT_FLAG=1;//ON
				jaesang_ctrl=1;
				//pSTM32_IO->PutRelayEventVector(HEATER_RY,ON,0);
				jaesang_period_wait=0;
				jaesang_period_seq=2;
			}
			break;
	case 2:
		jaesang_period_wait++;
		if(jaesang_period_wait<jaesang_on_period){
			jaesang_ctrl=1;
#ifdef DBG_JAESANG
	//		printf("제상 OFF 기다림 주기[%d/%d]\r\n",jaesang_period_wait,jaesang_on_period);
#endif
		}
		else{
			jaesang_ctrl=0;
			jaesang_period_wait=0;
			jaesang_period_seq=0; //return 처음으로
		}
		break;
	default:
		break;
	}
	return jaesang_ctrl;
}

uint8_t JAESANG::Jaesang_force_control(){
	uint8_t jaesang_ctrl=1;
	if(pDataClass->reg.parm.jaesang_heater_on_flag) pDataClass->reg.parm.jaesang_force_on_count++;
	if(pDataClass->reg.parm.jaesang_force_on_count>pDataClass->reg.parm.jaesang_on_duration){
		pDataClass->reg.sysFlag.b.JAESANG_FORCE_ON=0;
		jaesang_ctrl=0;
		pDataClass->reg.parm.jaesang_force_on_count=0;
		pDataClass->reg.parm.jaesang_heater_on_flag=0;
	}
	return jaesang_ctrl;
}
