/*
 * GUILIB.cpp
 *
 *  Created on: Jan 19, 2022
 *      Author: thpark
 */
#include "extern.h"
#include "data_class.h"
#include "adc.h"
#include "math.h"

#define xDBG_DATACLASS
#define xDBG_UPDATE

extern SPI1_FLASH *pFlashClass;
extern RS485 *pRS485;
extern RTC_API *pRTC_API;
extern MODEM *pMODEM;
extern JAESANG *pJAESANG;
extern OZONE_CTRL *pOZONE;
extern COMP_CTRL *pCOMP;
extern I2C_BM8563 *pHYM8563;


DATA_CLASS *pDataClass;


DATA_CLASS::DATA_CLASS() {
	// TODO Auto-generated constructor stub
	vRec.clear();
	vRec3month.clear();
	memset(&reg,0,sizeof(reg));
	sys_sec_tick=0;
}

DATA_CLASS::~DATA_CLASS() {
	// TODO Auto-generated destructor stub
}


void DATA_CLASS::SET_CTRL_REGISTER(RELAY relay, uint16_t count, uint8_t relay_on) {
	int i;
	uint8_t lRelay=(uint8_t)relay;
	lRelay%=7;
	if(lRelay<6)	{
		pDataClass->reg.relay[lRelay].on_count = count;
		//pDataClass->reg.ry[relay].off_count = off_count;
		pDataClass->reg.relay[lRelay].on = relay_on;
		pDataClass->reg.relay[lRelay].flag = 0;
	}
	else {
		for(i=0;i<6;i++) {
			pDataClass->reg.relay[i].on_count = count;
			//pDataClass->reg.ry[i].off_count = off_count;
			pDataClass->reg.relay[i].on = relay_on;
			pDataClass->reg.relay[i].flag = 0;
		}

	}

}


uint32_t DATA_CLASS::U32_between(uint32_t A, uint32_t B){
	//A-B Between second
	uint32_t mm;
	uint32_t diff;
	if(B<A){
		mm=0xFFFFFFFF-A;
		diff=mm+B;
	}
	else diff=B-A;
	return diff;
}


void DATA_CLASS::update_OneDay_Recoder(){
	record_periode_tick++;
#ifdef DBG_UPDATE
	printf("update_OneDay_Recoder record_periode_tick[%d] \r\n",record_periode_tick);
#endif
	if(record_periode_tick>ONE_DATA_PUSH_DELAY){
		record_periode_tick=0;
		RecDataPush();
#ifdef DBG_UPDATE
	printf("update_OneDay_Recoder top_mode[%d] top_title[%d]\r\n",	pSTM32_IO->navi_state.top_mode,	pSTM32_IO->navi_state.top_title);
#endif
		if (pSTM32_IO->navi_state.top_mode == 0 && (pSTM32_IO->navi_state.top_title == TOP_GRAPH1)){
			pMain_page->draw_graphFrame(GS_Y_START);
			pMain_page->draw_RecVector(GS_Y_START);
		}
	}
}

void DATA_CLASS::SYSTEM_POWER(uint8_t on){
	sys_sec_tick=0;
	if(on){
		printf("---POWER(ON)---\r\n");
		reg.sysFlag.b.POWER_ON=1;
		pDataClass->SYSTEM_ChangeToNormal();
		pIli9488->LCD_BACK_LIGHT(ON);
	}
	else {//power off
		printf("---POWER(OFF)---\r\n");
		reg.sysFlag.b.POWER_ON=0;
		pDataClass->SYSTEM_ChangeToNormal();
		reg.parm.comp_on_flag=OFF;
		reg.parm.evafan_on_flag=OFF;
		reg.parm.jaesang_heater_on_flag=OFF;
		pDataClass->reg.parm.ozone_gen_on=OFF;
		SET_CTRL_REGISTER(RELAY_ALL, 0, OFF);	//all relay off
		pIli9488->LCD_BACK_LIGHT(OFF);
	}
}

void DATA_CLASS::SYSTEM_ChangeToNormal(){
	if(reg.sysFlag.b.CHECK_RELAY_ON){
		reg.sysFlag.b.CHECK_RELAY_ON=0;
		pSTM32_IO->PutRelayEventVector(COMP_RY  ,reg.parm.comp_on_flag,0);
		pSTM32_IO->PutRelayEventVector(HEATER_RY,pDataClass->reg.parm.jaesang_heater_on_flag,0);
		pSTM32_IO->PutRelayEventVector(EVAFAN_RY,reg.parm.evafan_on_flag,0);
		pSTM32_IO->PutRelayEventVector(O3GEN_RY, reg.parm.ozone_gen_on,0);
		if(reg.parm.ozone_gen_on){
			pSTM32_IO->PutRelayEventVector(O3GEN_RY, ON, 0);
			pSTM32_IO->PutRelayEventVector(O3FAN_RY, ON, 0);
		}
		else{
			pSTM32_IO->PutRelayEventVector(O3GEN_RY, OFF, 0);
			pSTM32_IO->PutRelayEventVector(O3FAN_RY, OFF, 0);
		}
	}
	if(reg.sysFlag.b.JAESANG_FORCE_ON){
		reg.sysFlag.b.JAESANG_FORCE_ON=0;
		pDataClass->reg.parm.jaesang_heater_on_flag=0;
		pSTM32_IO->PutRelayEventVector(HEATER_RY,OFF,0);
	}


}
void DATA_CLASS::update_relay_action(){
	//RELAY CONTROL EACH SECOND
	for (int i = 0; i < 6; i++) {
		reg.relay[i].on_count--;
		if (reg.relay[i].on_count <= 0) {
			reg.relay[i].on_count = 0;
			if(reg.relay[i].flag){
				reg.relay[i].flag=0;
				//printf("\n\r\r\n RELAY[%d] on[%d]ACTION\r\n",i, reg.relay[i].on);
				pSTM32_IO->Public_RELAY_CTRL_Immediately( i, reg.relay[i].on);
			}
		}
	}

}

void DATA_CLASS::update_hangon_condition(int measure){

	//int seljung = reg.parm.seljung_temp;//(int) pMain_page->UI_Data.target.temp;
	int hangon_ref_temp = reg.parm.super_cool_ref;//seljung - pCOMP->comp_info.super_cool_protect*10;//항온설정오도
	if(hangon_ref_temp>=measure){
		reg.sysFlag.b.SUPER_COOL_PROTECT=1;
	}
	else{
		reg.sysFlag.b.SUPER_COOL_PROTECT=0;
	}
//	printf(	"### hangon_ref_temp[%d] measure[%d] HANGON_ON_FLAG[%d]\r\n",hangon_ref_temp, measure, reg.sysFlag.b.HANGON_ON_FLAG);

}


void DATA_CLASS::display_sysFlag_message(){
	printf(	"\r\n=============================\r\n");
	printf(	"Ozone MODE   ------------[%d]\r\n",reg.local_ozone_ctrl_mode);
	printf(	"JaeSang MODE ------------[%d]\r\n",reg.local_jeasang_ctrl_mode);
	printf(	"COMPRES_EVENT_FLAG-------[%d]\r\n",reg.sysFlag.b.COMPRES_EVENT_FLAG);
	printf(	"JAESANG_EVENT_FLAG-------[%d]\r\n",reg.sysFlag.b.JAESANG_EVENT_FLAG);
	printf(	"HIGH_TEMPER_ALARM_ON-----[%d]\r\n",reg.sysFlag.b.HIGH_TEMPER_ALARM_ON);
	printf(	"LOW_TEMPER_ALARM_ON------[%d]\r\n",reg.sysFlag.b.LOW_TEMPER_ALARM_ON);
	printf(	"OZONE_PERIOD_CTRL_ON-----[%d]\r\n",reg.sysFlag.b.OZONE_PERIOD_CTRL_ON);
	printf(	"OZONE_SENSOR_CTRL_ON-- --[%d]\r\n",reg.sysFlag.b.OZONE_SENSOR_CTRL_ON);
//	printf(	"THERMOSTAT_ON------------[%d]\r\n",reg.sysFlag.b.THERMOSTAT_ON);
	printf(	"SUPER_COOL_PROTECT-------[%d]\r\n",reg.sysFlag.b.SUPER_COOL_PROTECT);
	printf(	"SELJUNG_BUZZER-----------[%d]\r\n",reg.parm.seljung_buzzer);
}


void DATA_CLASS::update_system_local_time(){

//	pMain_page->UI_Data.data_arry[5].idata[1]=nowTimeInfo.timeinfo.tm_year;
//	pMain_page->UI_Data.data_arry[5].idata[2]=nowTimeInfo.timeinfo.tm_mon;
//	pMain_page->UI_Data.data_arry[5].idata[3]=nowTimeInfo.timeinfo.tm_mday;
//	pMain_page->UI_Data.data_arry[5].idata[4]=nowTimeInfo.timeinfo.tm_hour;
//	pMain_page->UI_Data.data_arry[5].idata[5]=nowTimeInfo.timeinfo.tm_min;
}

void DATA_CLASS::update_error_check(){
	reg.sysFlag.b.TEMPERATURE_SENSOR_ERROR= (RS485_temperature_sensor_exist) ? 0:1;
	//reg.sysFlag.b.OZONE_SENSOR_ERROR      = (measured_ozone_sensor) ? 0:1;
	//reg.sysFlag.b.WIND_SENSOR_ERROR       = (measured_wind_sensor) ? 0:1;
}

void DATA_CLASS::update_local_mode(){
	//reg.local_jeasang_ctrl_mode =(reg.sysFlag.b.WIND_SENSOR_ERROR==0  && reg.parm.jaesang_mode==1) ? 1 : 0;
	reg.local_jeasang_ctrl_mode =0;
	reg.local_ozone_ctrl_mode   =(reg.sysFlag.b.OZONE_SENSOR_ERROR==0 && reg.parm.ozone_control_mode==1) ? 1 : 0;
}

void DATA_CLASS::get_wind_parameter(int t2, int t3, uint8_t sensor){
	int dwind;
	//int offset=0;
	//dx= +65.0 ~ -10.0 = 750  dy= 1467 ~ 220  dy/dx=1.66
	//measured_inp_temp=(int)((raw_wind*5000)/1024)-500;//tmp36
	//measured_oup_temp=(int)((wind_temp_adc*5000)/1024)-500;//tmp36
	measured_inp_temp=(int)t2;
	measured_oup_temp=(int)t3;
	RS485_wind_sensor_exist=sensor;
	measured_wind=abs(measured_oup_temp-measured_inp_temp);
	//float temp_offset= 1317-(1.66*measured_wind_temp); //y=-ax+b
    //dwind = (int)(raw_wind - temp_offset);
    printf("adc0[%d] adc1[%d]  [%d][%d]-[%d]\r\n",t2, t3, measured_inp_temp, measured_oup_temp, measured_wind);
   // printf("raw_wind[%d] STH20[%d] TMP36[%d] wind[%d]\r\n",raw_wind, measured_temperature, measured_wind_temp, dwind);
//	return 1000+dwind;
}

void DATA_CLASS::Get_Parameter(){

	reg.parm.seljung_temp  = (int)pMain_page->UI_Data.target.temp;
	reg.parm.seljung_ozone = (int)pMain_page->UI_Data.target.o3;
	reg.parm.seljung_wind  = (int)pMain_page->UI_Data.target.wind;


	reg.parm.comp_on_latency=      pMain_page->UI_Data.data_arry[COMP_PAGE].idata[1];//"콤프지연"
	reg.parm.evafan_off_latency=   pMain_page->UI_Data.data_arry[COMP_PAGE].idata[2];//"EVA팬지연"
	reg.parm.comp_yendong_evafan = pMain_page->UI_Data.data_arry[COMP_PAGE].idata[3];//"COMP작동중 EVA팬"

	reg.parm.jaesang_mode=          pMain_page->UI_Data.data_arry[JAESANG_PAGE].idata[1];
	reg.parm.jaesang_total_peroid = pMain_page->UI_Data.data_arry[JAESANG_PAGE].idata[2]*MINUTE;//total제상주기(분)
	reg.parm.jaesang_on_duration  = pMain_page->UI_Data.data_arry[JAESANG_PAGE].idata[3]*MINUTE;//on time(분)
	reg.parm.jaesang_sensor_period = pMain_page->UI_Data.data_arry[JAESANG_PAGE].idata[4]*MINUTE;//on time(분)
	reg.parm.comp_on_at_jaesang   = pMain_page->UI_Data.data_arry[JAESANG_PAGE].idata[8];	//제상시 COMP_ON

	reg.parm.ozone_control_mode= pMain_page->UI_Data.data_arry[OZONE_PAGE].idata[1];
	reg.parm.ozone_gen_on_delay= pMain_page->UI_Data.data_arry[OZONE_PAGE].idata[2];
	reg.parm.ozone_fan_off_delay=pMain_page->UI_Data.data_arry[OZONE_PAGE].idata[3];
	reg.parm.ozone_diff =        pMain_page->UI_Data.data_arry[OZONE_PAGE].idata[4];
	reg.parm.ozone_total_period= pMain_page->UI_Data.data_arry[OZONE_PAGE].idata[5]*MINUTE; //주기
	reg.parm.ozone_on_duration = pMain_page->UI_Data.data_arry[OZONE_PAGE].idata[6];//발생시간

	reg.parm.abnormal_elap_time= pMain_page->UI_Data.data_arry[SELJUNG_PAGE].idata[1]*MINUTE;
	reg.parm.abnormal_high_temp= reg.parm.seljung_temp + (pMain_page->UI_Data.data_arry[SELJUNG_PAGE].idata[2] * 10);
	reg.parm.abnormal_low_temp= reg.parm.seljung_temp + (pMain_page->UI_Data.data_arry[SELJUNG_PAGE].idata[3] * 10);
	reg.parm.comp_hysteresis= pMain_page->UI_Data.data_arry[SELJUNG_PAGE].idata[4];
	reg.parm.temp_compensation=(int) pMain_page->UI_Data.data_arry[SELJUNG_PAGE].idata[5];
	reg.parm.super_cool_protect=(int) pMain_page->UI_Data.data_arry[SELJUNG_PAGE].idata[6];
	reg.parm.seljung_buzzer  =  pMain_page->UI_Data.data_arry[SELJUNG_PAGE].idata[7];
	reg.parm.jaesang_max_wind_reference = pMain_page->UI_Data.data_arry[SELJUNG_PAGE].idata[8];

	reg.parm.ozone_start_time= reg.parm.ozone_total_period - pDataClass->reg.parm.ozone_on_duration;
	reg.parm.seljung_jaesang=(reg.parm.jaesang_max_wind_reference*pMain_page->UI_Data.target.wind)/100;//target.wind(%)
	//seljung_wind=(pJAESANG->jaesang_max_wind_reference * UI_Data.target.wind)/100;

	reg.parm.comp_on_ref =	(int) pMain_page->UI_Data.target.temp + pDataClass->reg.parm.comp_hysteresis;
	reg.parm.comp_off_ref= reg.parm.seljung_temp;
	reg.parm.super_cool_ref= reg.parm.comp_off_ref-(reg.parm.super_cool_protect*10);

}

void DATA_CLASS::OneSecond_prosess(void) {
	int i=0;
	// |--p0----|         |----p0---
	// |--p1--------------|----p1---
	// |--flag0-|--flag1--|
	SENSOR_BOX sht20;
	uint8_t  ljaesang_control_mode;//=pJAESANG->jaesang_info.jaesang_mode;
	if(pIli9488->Ignore_Draw==1)return;
	//system_wait++;
	sys_sec_tick++;
	if(sys_sec_tick>SYSTEM_INIT_TIMEOUT)sys_sec_tick=100;

	//nowTimeInfo=pRTC_API->RTC_GetRTC();
	nowTimeInfo=pHYM8563->RTC_GetRTC();
#ifdef DBG_DATACLASS
	//printf("[%d-%d-%d %02d:%02d:%02d\r\n",nowTimeInfo.timeinfo.tm_year, nowTimeInfo.timeinfo.tm_mon, nowTimeInfo.timeinfo.tm_mday,
	//		nowTimeInfo.timeinfo.tm_hour, nowTimeInfo.timeinfo.tm_min, nowTimeInfo.timeinfo.tm_sec);
#endif


	pMODEM->Modem_Process();
	update_OneDay_Recoder();
	//update_system_local_time();

	reg.sysFlag.b.CHANGGO_DOOR=pSTM32_IO->DOOR_SW_Read();
	reg.sysFlag.b.DP_VALVE=pSTM32_IO->DP_SW_Read();
// measured_temperature=(int)pWind->update_thermist.ntc_temp1;
// 에바팬 on 상태에서 wind 측정
// sht20= pSHT20->Get_SHT20();
	pRS485->rs485_req(BD_SHT20);
	measured_temperature= pRS485->rs485_sht20.sub_sensor1 + (reg.parm.temp_compensation);
	measured_humidity=pRS485->rs485_sht20.sub_sensor2;
	RS485_temperature_sensor_exist=pRS485->rs485_sht20.exist;

	//printf("measured_temperature[%d] raw_temp[%d] temp_compensation[%d] DP[%d]\r\n",measured_temperature, pRS485->rs485_sht20.sub_sensor1, reg.parm.temp_compensation, reg.sysFlag.b.DP_VALVE);


	if(reg.parm.ozone_control_mode==1){ //설정이 ozon 센서모드인 경우에만 리케스트
		pRS485->rs485_req(BD_ZE27_AND_DHT20);
		measured_ozone=(int)pRS485->rs485_ze27_and_dht22.sub_sensor0;
		RS485_ozone_sensor_exist=pRS485->rs485_ze27_and_dht22.exist;
		if(RS485_ozone_sensor_exist){
					//if(try_ozone_sensor>=5) pSTM32_IO->vKEY_BUFFER.push_back(eLEFTX1_KEY);
					try_ozone_sensor=0;
					reg.sysFlag.b.OZONE_SENSOR_ERROR=0;
		}
		else {
			try_ozone_sensor++;
			if(try_ozone_sensor>5){
				try_ozone_sensor=100;
				measured_ozone=0;
				if(reg.sysFlag.b.OZONE_SENSOR_ERROR==0){
					reg.sysFlag.b.OZONE_SENSOR_ERROR=1;
					//pSTM32_IO->vKEY_BUFFER.push_back(eLEFTX1_KEY);
				}
			}

		}
	}
	//printf("exist[%d] measured_ozone_sensor[%d]\r\n",measured_ozone_sensor, measured_ozone);
	pRS485->rs485_req(BD_WIND);
	get_wind_parameter(pRS485->rs485_wind_speed.sub_sensor0, pRS485->rs485_wind_speed.sub_sensor1, pRS485->rs485_wind_speed.exist);
	if(reg.parm.jaesang_mode==JS_SENSOR){ //설정이 센서모드인 경우에만 리케스트
		//pRS485->rs485_req(BD_WIND);
		//measured_wind =(int)pRS485->rs485_wind_speed.sub_sensor0;
		//measured_wind_sensor=pRS485->rs485_wind_speed.exist;
		if(RS485_wind_sensor_exist){
			//if(try_jaesang_sensor>=5) pSTM32_IO->vKEY_BUFFER.push_back(eRIGHTX1_KEY);
			try_jaesang_sensor=0;
			reg.sysFlag.b.WIND_SENSOR_ERROR=0;
		}
		else {
			try_jaesang_sensor++;
			if(try_jaesang_sensor>5){
				try_jaesang_sensor=100;
				measured_wind=0;
				if(reg.sysFlag.b.WIND_SENSOR_ERROR==0){
					reg.sysFlag.b.WIND_SENSOR_ERROR=1;
					//pSTM32_IO->vKEY_BUFFER.push_back(eRIGHTX1_KEY);
				}
			}

		}
		//printf("try_jaesang_sensor[%d] WIND_SENSOR_ERROR[%d] measured_wind[%d]\r\n",try_jaesang_sensor, reg.sysFlag.b.WIND_SENSOR_ERROR, measured_wind);
	}

	update_local_mode();
	define_alarm(measured_temperature, reg.parm.seljung_temp);
	pMain_page->update_DrawMainMenu();

	//printf("exist[%d] measured_ozone_sensor[%d]\r\n",measured_wind_sensor, measured_wind);
#ifdef DBG_DATACLASS
	printf("power[%d] tho[%03d,%03d,%03d] ozone[%d][%d] Wind_Exist[%d] Wind_Value[%d] ljaesang_mode[%d]\r\n",
			reg.sysFlag.b.POWER_ON,
			RS485_temperature_sensor_exist, measured_temperature, measured_humidity,
			RS485_ozone_sensor_exist,measured_ozone,
			RS485_wind_sensor_exist,measured_wind,
			pJAESANG->jaesang_info.jaesang_mode );
#endif

	pSTM32_IO->RelayEventVector_Process();
	update_relay_action();

	//제상일때는 콤프제어 하지 않는다
	//if(!pJAESANG->jaesang_info.jaesang_RELAY_ON_STATUS)
	 if(reg.sysFlag.b.POWER_ON) {
		Get_Parameter();
		update_hangon_condition(measured_temperature);
		pCOMP->control_comp(measured_temperature);
		pEVAFAN->ctrl_evafan(measured_temperature, measured_wind);
		pJAESANG->ctrl_jaesang_heater();
		pOZONE->ctrl_ozone(measured_ozone);
	}
//error check++
	 update_error_check();
//error check--
	//-----------------------------------
	if(reg.parm.seljung_buzzer){//부저 설정
		//uint8_t sensor_error=pWind->update_thermist.tc_error.b.tc1;//TC1만 체크
		uint8_t sensor_error=RS485_temperature_sensor_exist;
		uint8_t temp_alarm = (reg.sysFlag.b.HIGH_TEMPER_ALARM_ON || reg.sysFlag.b.LOW_TEMPER_ALARM_ON) ? 1 : 0;
		Buzzer= (sensor_error||temp_alarm) ? true:false;
		if(Buzzer)pSTM32_IO->BEEP_ON();
	}
	//Exchange_System_State();
	//display_sysFlag_message();

}

void DATA_CLASS::RecDataPush(void) {
	// TODO Auto-generated constructor stub
	uint8_t i, lsz, sz;
	uint32_t ltime = nowTimeInfo.ltime;//nowTimeInfo.lsecOneDay;
	RECORD_UNIT recUnit;
	RECORD_UNIT lrec;
	uint16_t unit_sz= sizeof(RECORD_UNIT);
	uint8_t rec_buf[600]={0,}; //12바이트 48개 576byte정도 maybe~~
	int16_t temp;
	uint16_t o3;
	uint16_t humidity;

	temp= (reg.sysFlag.b.TEMPERATURE_SENSOR_ERROR==0 && (measured_temperature >(-100) && measured_temperature < 500)) ? measured_temperature : -500;
	humidity= (reg.sysFlag.b.TEMPERATURE_SENSOR_ERROR==0) ?  measured_humidity : 0;
	o3  = (reg.sysFlag.b.OZONE_SENSOR_ERROR ==0  && (measured_ozone<1000 && measured_ozone > 0)) ? measured_ozone : 0;

#ifdef DBG_UPDATE
		printf("(2)--ltime[%d] temperature[%d] humidity[%d] ozone[%d]\r\n",ltime,  temp, humidity, o3);
#endif
	sz = vRec.size();
	recUnit.ltime=ltime;
	recUnit.o3=o3;
	recUnit.temperature=temp;
	recUnit.humidity=humidity;
	recUnit.power_on=0xAA;//power on 표시
	if (sz < 48) {
		vRec.push_back(recUnit);
		lsz = vRec.size();
		if (lsz < 48) {
			for (i = lsz; i < 48; i++) {
				lrec.ltime = ltime;// -(i*gap);
				lrec.temperature = 0;
				lrec.humidity = 0;
				lrec.o3 = 0;
				lrec.power_on = 0;
				vRec.push_back(lrec);
			}
		}
	}
	else{
		vRec.erase(vRec.begin());
		vRec.push_back(recUnit);
	}

	sz = vRec.size();
	for (i = 0; i < sz; i++) {
		lrec=vRec[i];
		memcpy(&rec_buf[(i* unit_sz)], &lrec, unit_sz);
#ifdef DBG_UPDATE
		printf("--(W)Read vRec[%02d][%08d] power[%02x] temp[%03d] humid[%03d] o3[%03d]\r\n",i, lrec.ltime, lrec.power_on, lrec.temperature, lrec.humidity, lrec.o3);
#endif
	}
    pFlashClass->SPI_FLASH_SAVE_RECORD(rec_buf, (unit_sz * sz));
}

void DATA_CLASS::Get_PushedRecData_FromEEPROM(void) {

	uint8_t i;
	RECORD_UNIT lrec;
	uint8_t rData[16]={0,};
	uint16_t unit_sz= sizeof(RECORD_UNIT);
	printf("[BOOT(3)] RecoveryRecDataPush\r\n");
	vRec.clear();
	for(i=0;i<48;i++)
	{
		pFlashClass->flash_read(&hspi1, FLASH_ONE_DAY_RECORD_ADD+(i*unit_sz), (uint8_t*)&rData, unit_sz);
		memcpy(&lrec, &rData[0], unit_sz);
		vRec.push_back(lrec);
#ifdef DBG_UPDATE
		printf("--(R)Read vRec[%02d]:[%08d] power[%02x] temp[%03d] humidi[%03d] o3[%d]\r\n",i, lrec.ltime, lrec.power_on, lrec.temperature,  lrec.humidity, lrec.o3);
#endif
	}

}

void DATA_CLASS::define_alarm(int measure, int seljung)
{

	int KEEP_TIME_ref = pDataClass->reg.parm.abnormal_elap_time;
	int HI_alarm_ref = pDataClass->reg.parm.abnormal_high_temp;
	int LO_alarm_ref = pDataClass->reg.parm.abnormal_low_temp;
#ifdef DBG_TEMP_ALARM
		printf("control_alarm measure[%d] KEEP_TIME_ref[%d] HI_alarm_ref[%d] LO_alarm_ref[%d]\r\n",measure, KEEP_TIME_ref, HI_alarm_ref, LO_alarm_ref );
#endif
	if (measure >= HI_alarm_ref) reg_alarm[0].flag = 1;
	else{
		reg_alarm[0].flag =0;
		reg_alarm[0].on_count = 0;
		reg_alarm[0].on =0;
	}

	if (measure <= LO_alarm_ref) reg_alarm[1].flag = 1;
	else
	{
		reg_alarm[1].flag =0;
		reg_alarm[1].on_count = 0;
		reg_alarm[1].on = 0;
	}
	for (int i = 0; i < 2; i++)
	{
		if(reg_alarm[i].flag)
		{
			reg_alarm[i].on_count++;
			if (reg_alarm[i].on_count >= KEEP_TIME_ref)
			{
				reg_alarm[i].on = 1;
				reg_alarm[i].on_count = KEEP_TIME_ref;
			}
			else reg_alarm[i].on = 0;
		}
		reg_alarm[i].toggle ^=1;//toggle
	}
	pDataClass->reg.sysFlag.b.HIGH_TEMPER_ALARM_ON = (reg_alarm[0].on) ? 1 : 0;
	pDataClass->reg.sysFlag.b.LOW_TEMPER_ALARM_ON =  (reg_alarm[1].on) ? 1 : 0;
//온도에 따른 온도에 따른 알람조작 --
#ifdef DBG_TEMP_ALARM
		printf("HIGH_TEMPER_ALARM_ON[%d] LOW_TEMPER_ALARM_ON[%d]\r\n",pDataClass->reg.sysFlag.b.HIGH_TEMPER_ALARM_ON, pDataClass->reg.sysFlag.b.LOW_TEMPER_ALARM_ON);
#endif
}


