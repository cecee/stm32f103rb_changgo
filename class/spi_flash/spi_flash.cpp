/**
  ******************************************************************************
  * @file SPI/M25P64_FLASH/spi_flash.c 
  * @author  MCD Application Team
  * @version  V3.0.0
  * @date  04/06/2009
  * @brief  This file provides a set of functions needed to manage the
  *         communication between SPI peripheral and SPI M25P64 FLASH.
  ******************************************************************************
  * @copy
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2009 STMicroelectronics</center></h2>
  */ 

/* Includes ------------------------------------------------------------------*/
#include "extern.h"
#include "spi_flash.h"
#include<string.h>

//extern CONSOLE_VALUE gVal;
extern SPI_HandleTypeDef hspi1;   

//O3_MAIN_PAGE *pMain_page;

void save_eeprom(CONTROL_TARGET eep);

/** @addtogroup StdPeriph_Examples
  * @{
  */

/** @addtogroup SPI_M25P64_FLASH
  * @{
  */ 

/* Private typedef -----------------------------------------------------------*/
#define SPI_FLASH_PageSize    0x100

/* Private define ------------------------------------------------------------*/
#define WRITE      0x02  /* Write to Memory instruction */
#define WRSR       0x01  /* Write Status Register instruction */
#define WREN       0x06  /* Write enable instruction */
#define READ       0x03  /* Read from Memory instruction */
#define RDSR       0x05  /* Read Status Register instruction  */
#define RDID       0x9F  /* Read identification */
#define SE         0xD8  /* Sector Erase instruction */
#define BE         0xC7  /* Bulk Erase instruction */
#define WIP_Flag   0x01  /* Write In Progress (WIP) flag */
#define Dummy_Byte 0xA5

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Initializes the peripherals used by the SPI FLASH driver.
  * @param  None
  * @retval : None
  */
SPI1_FLASH *pFlashClass;


/**
  * @brief  Reads current status of the W25Q128FV.
  * @retval W25Q128FV memory status
  */
uint8_t SPI1_FLASH::BSP_W25Qx_GetStatus(void)
{
	uint8_t cmd[] = {READ_STATUS_REG1_CMD};
	uint8_t status;

	W25Qx_Enable();
	/* Send the read status command */
	HAL_SPI_Transmit(&hspi1, cmd, 1, W25Qx_TIMEOUT_VALUE);
	/* Reception of the data */
	HAL_SPI_Receive(&hspi1,&status, 1, W25Qx_TIMEOUT_VALUE);
	W25Qx_Disable();

	/* Check the value of the register */
  if((status & W25Q128FV_FSR_BUSY) != 0)
  {
    return W25Qx_BUSY;
  }
	else
	{
		return W25Qx_OK;
	}
}

/**
  * @brief  This function send a Write Enable and wait it is effective.
  * @retval None
  */
uint8_t SPI1_FLASH::BSP_W25Qx_WriteEnable(void)
{
	uint8_t cmd[] = {WRITE_ENABLE_CMD};
	uint32_t tickstart = HAL_GetTick();

	/*Select the FLASH: Chip Select low */
	W25Qx_Enable();
	/* Send the read ID command */
	HAL_SPI_Transmit(&hspi1, cmd, 1, W25Qx_TIMEOUT_VALUE);
	/*Deselect the FLASH: Chip Select high */
	W25Qx_Disable();

	/* Wait the end of Flash writing */
	while(BSP_W25Qx_GetStatus() == W25Qx_BUSY)
	{
		/* Check for the Timeout */
    if((HAL_GetTick() - tickstart) > W25Qx_TIMEOUT_VALUE)
    {
			return W25Qx_TIMEOUT;
    }
	}

	return W25Qx_OK;
}

/**
  * @brief  Read Manufacture/Device ID.
	* @param  return value address
  * @retval None
  */
void SPI1_FLASH::BSP_W25Qx_Read_ID(uint8_t *ID)
{
	uint8_t cmd[4] = {READ_ID_CMD,0x00,0x00,0x00};

	W25Qx_Enable();
	/* Send the read ID command */
	HAL_SPI_Transmit(&hspi1, cmd, 4, W25Qx_TIMEOUT_VALUE);
	/* Reception of the data */
	HAL_SPI_Receive(&hspi1,ID, 2, W25Qx_TIMEOUT_VALUE);
	W25Qx_Disable();

}
/**
  * @brief  Reads an amount of data from the QSPI memory.
  * @param  pData: Pointer to data to be read
  * @param  ReadAddr: Read start address
  * @param  Size: Size of data to read
  * @retval QSPI memory status
  */
uint8_t SPI1_FLASH::BSP_W25Qx_Read(uint8_t* pData, uint32_t ReadAddr, uint32_t Size)
{
	uint8_t cmd[4];

	/* Configure the command */
	cmd[0] = READ_CMD;
	cmd[1] = (uint8_t)(ReadAddr >> 16);
	cmd[2] = (uint8_t)(ReadAddr >> 8);
	cmd[3] = (uint8_t)(ReadAddr);

	W25Qx_Enable();
	/* Send the read ID command */
	HAL_SPI_Transmit(&hspi1, cmd, 4, W25Qx_TIMEOUT_VALUE);
	/* Reception of the data */
	if (HAL_SPI_Receive(&hspi1, pData,Size,W25Qx_TIMEOUT_VALUE) != HAL_OK)
  {
    return W25Qx_ERROR;
  }
	W25Qx_Disable();
	return W25Qx_OK;
}

/**
  * @brief  Writes an amount of data to the QSPI memory.
  * @param  pData: Pointer to data to be written
  * @param  WriteAddr: Write start address
  * @param  Size: Size of data to write,No more than 256byte.
  * @retval QSPI memory status
  */
uint8_t SPI1_FLASH::BSP_W25Qx_Write(uint8_t* pData, uint32_t WriteAddr, uint32_t Size)
{
	uint8_t cmd[4];
	uint32_t end_addr, current_size, current_addr;
	uint32_t tickstart = HAL_GetTick();

	/* Calculation of the size between the write address and the end of the page */
  current_addr = 0;

  while (current_addr <= WriteAddr)
  {
    current_addr += W25Q128FV_PAGE_SIZE;
  }
  current_size = current_addr - WriteAddr;

  /* Check if the size of the data is less than the remaining place in the page */
  if (current_size > Size)
  {
    current_size = Size;
  }

  /* Initialize the adress variables */
  current_addr = WriteAddr;
  end_addr = WriteAddr + Size;

  /* Perform the write page by page */
  do
  {
		/* Configure the command */
		cmd[0] = PAGE_PROG_CMD;
		cmd[1] = (uint8_t)(current_addr >> 16);
		cmd[2] = (uint8_t)(current_addr >> 8);
		cmd[3] = (uint8_t)(current_addr);

		/* Enable write operations */
		BSP_W25Qx_WriteEnable();

		W25Qx_Enable();
    /* Send the command */
    if (HAL_SPI_Transmit(&hspi1,cmd, 4, W25Qx_TIMEOUT_VALUE) != HAL_OK)
    {
      return W25Qx_ERROR;
    }

    /* Transmission of the data */
    if (HAL_SPI_Transmit(&hspi1, pData,current_size, W25Qx_TIMEOUT_VALUE) != HAL_OK)
    {
      return W25Qx_ERROR;
    }
			W25Qx_Disable();
    	/* Wait the end of Flash writing */
		while(BSP_W25Qx_GetStatus() == W25Qx_BUSY);
		{
			/* Check for the Timeout */
			if((HAL_GetTick() - tickstart) > W25Qx_TIMEOUT_VALUE)
			{
				return W25Qx_TIMEOUT;
			}
		}

    /* Update the address and size variables for next page programming */
    current_addr += current_size;
    pData += current_size;
    current_size = ((current_addr + W25Q128FV_PAGE_SIZE) > end_addr) ? (end_addr - current_addr) : W25Q128FV_PAGE_SIZE;
  } while (current_addr < end_addr);


	return W25Qx_OK;
}


/**
  * @brief  Erases the specified block of the QSPI memory.
  * @param  BlockAddress: Block address to erase
  * @retval QSPI memory status
  */
uint8_t SPI1_FLASH::BSP_W25Qx_Erase_Block(uint32_t Address)
{
	uint8_t cmd[4];
	uint32_t tickstart = HAL_GetTick();
	cmd[0] = SECTOR_ERASE_CMD;
	cmd[1] = (uint8_t)(Address >> 16);
	cmd[2] = (uint8_t)(Address >> 8);
	cmd[3] = (uint8_t)(Address);

	/* Enable write operations */
	BSP_W25Qx_WriteEnable();

	/*Select the FLASH: Chip Select low */
	W25Qx_Enable();
	/* Send the read ID command */
	HAL_SPI_Transmit(&hspi1, cmd, 4, W25Qx_TIMEOUT_VALUE);
	/*Deselect the FLASH: Chip Select high */
	W25Qx_Disable();

	/* Wait the end of Flash writing */
	while(BSP_W25Qx_GetStatus() == W25Qx_BUSY)
	{
		/* Check for the Timeout */
    if((HAL_GetTick() - tickstart) > W25Q128FV_SECTOR_ERASE_MAX_TIME)
    {
			return W25Qx_TIMEOUT;
    }
	}
	return W25Qx_OK;
}

/**
  * @brief  Erases the entire QSPI memory.This function will take a very long time.
  * @retval QSPI memory status
  */
uint8_t SPI1_FLASH::BSP_W25Qx_Erase_Chip(void)
{
	uint8_t cmd[4];
	uint32_t tickstart = HAL_GetTick();
	cmd[0] = SECTOR_ERASE_CMD;

	/* Enable write operations */
	BSP_W25Qx_WriteEnable();

	/*Select the FLASH: Chip Select low */
	W25Qx_Enable();
	/* Send the read ID command */
	HAL_SPI_Transmit(&hspi1, cmd, 1, W25Qx_TIMEOUT_VALUE);
	/*Deselect the FLASH: Chip Select high */
	W25Qx_Disable();

	/* Wait the end of Flash writing */
	while(BSP_W25Qx_GetStatus() != W25Qx_BUSY);
	{
		/* Check for the Timeout */
    if((HAL_GetTick() - tickstart) > W25Q128FV_BULK_ERASE_MAX_TIME)
    {
			return W25Qx_TIMEOUT;
    }
	}
	return W25Qx_OK;
}

SPI1_FLASH::SPI1_FLASH() {
	// TODO Auto-generated constructor stub
}

SPI1_FLASH::~SPI1_FLASH() {
	// TODO Auto-generated destructor stub
}

void SPI1_FLASH::SPI_FLASH_Init(void)
{
//  /* Deselect the FLASH: Chip Select high */
//  SPI_FLASH_CS_HIGH();
//  flash_read(&hspi1, FLASH_SELJUNG_ADDRESS, (uint8_t*)&eeprom, sizeof(eeprom));
//  printf("[%x][%x]\r\n",eeprom.header, eeprom.nheader);
//  if(eeprom.header!=0xAAAA || eeprom.nheader!=0x5555){
//    printf("xxxx[%x][%x]\r\n",eeprom.header, eeprom.nheader);
//    eeprom.header=0xAAAA;
//    eeprom.nheader=0x5555;
//    eeprom.on_time=25;
//    eeprom.o3=100;
//    eeprom.temperature=20;
//    eeprom.off_time=10;//10��
//    SPI_FLASH_UPDATE_SELJUNG();
//  }
  
}

void SPI1_FLASH::SPI_FLASH_RETRIEVE_SELJUNG(void)
{
  uint8_t rdata[512]={0,};
  MENU_SET_DATA rUI_Data;
  flash_read(&hspi1, FLASH_SELJUNG_ADDRESS, (uint8_t*)&rdata, sizeof(MENU_SET_DATA));
  memcpy(&rUI_Data, rdata, sizeof(MENU_SET_DATA));
  if(rUI_Data.sys_info.crc!='@'){
	  SPI_FLASH_UPDATE_SELJUNG();
	  printf("[BOOT(1)] === LOSS EEPROM DATA!! System config From INIT Value ==\r\n");
	  printf("[BOOT(2)] === %s ==\r\n",rUI_Data.sys_info.build_date);
  }
  else{
	  printf("[BOOT(1)] === Recover System config From EEPROM ==\r\n");
	  printf("[BOOT(2)] === %s ==\r\n",rUI_Data.sys_info.build_date);
	  memcpy(&pMain_page->UI_Data, &rUI_Data, sizeof(MENU_SET_DATA));

  }
  //printf("=== read_eeprom ====[%x][%x][%x][%x]\r\n", rdata[0], rdata[1], rdata[2], rdata[3]);
  //printf("===> SPI_FLASH_test [%s]\r\n",rUI_Data.sys_info.build_date);

}

void SPI1_FLASH::SPI_FLASH_UPDATE_SELJUNG(void)
{
  uint8_t w_data[512];
  pMain_page->UI_Data.sys_info.crc='@';
  memcpy(w_data, &pMain_page->UI_Data, sizeof(MENU_SET_DATA));
  //printf("=== save_eeprom ====[%x][%x][%x][%x]\r\n", w_data[0], w_data[1], w_data[2], w_data[3]);
  flash_sector_erase(&hspi1,FLASH_SELJUNG_ADDRESS);
  SPI_FLASH_WRITER_SECTOR(FLASH_SELJUNG_ADDRESS, w_data, sizeof(MENU_SET_DATA));
  HAL_Delay(50);
}


void SPI1_FLASH::SPI_FLASH_SAVE_RECORD(uint8_t *wdata, uint16_t size)
{
  //printf("=== SPI_FLASH_SAVE_RECORD [%d]====[%02x][%02x][%02x][%02x]\r\n",size, wdata[572], wdata[573], wdata[574], wdata[575]);
  //SPI_FLASH_WRITER_SECTOR(FLASH_ONE_DAY_RECORD_ADD, wdata, size );
  flash_sector_erase(&hspi1, FLASH_ONE_DAY_RECORD_ADD);
  SPI_FLASH_BufferWrite(wdata, FLASH_ONE_DAY_RECORD_ADD, size);

}


void SPI1_FLASH::SPI_FLASH_WRITER_SECTOR(uint32_t wadd, uint8_t *wdata, uint16_t size)
{
  //int i;
  flash_sector_erase(&hspi1, wadd);
  SPI_FLASH_BufferWrite(wdata, wadd, size);
 // HAL_Delay(10);
  //printf("=== wwww_eeprom ====[%x]-[%x]-[%x]-[%x]\r\n", w_data[0], w_data[1], w_data[2], w_data[3]);
  //printf("=== read_eeprom ====[%x]-[%x]-[%x]-[%x]\r\n", r_data[0], r_data[1], r_data[2], r_data[3]);

}


//RECORD_POST SPI1_FLASH::SPI_FLASH_Read_RECORD()
//{
//  RECORD_POST post;
//  flash_read(&hspi1, FLASH_ONE_DAY_RECORD_ADD, (uint8_t*)&post.imsi, 48*sizeof(RECORD_UNIT));
////  printf("=== SPI_FLASH_Read_RECORD post.imsi[%02x][%02x][%02x][%02x]\r\n", post.imsi[572], post.imsi[573], post.imsi[574], post.imsi[575]);
//  return post;
//}


uint8_t SPI1_FLASH::flash_busy(SPI_HandleTypeDef *SPI){
	uint8_t status_reg;
	flash_read_status(SPI,1,&status_reg);
	return status_reg & 0x01;
}

void SPI1_FLASH::flash_write_enable(SPI_HandleTypeDef *SPI){
	uint8_t buf;
	buf=0x06;
	SPI_FLASH_CS_LOW();
	HAL_SPI_Transmit(SPI,&buf,1,1);
	SPI_FLASH_CS_HIGH();
}

void SPI1_FLASH::flash_write_disable(SPI_HandleTypeDef *SPI){
	uint8_t buf;
	buf=0x04;
	SPI_FLASH_CS_LOW();
	HAL_SPI_Transmit(SPI,&buf,1,1);
	SPI_FLASH_CS_HIGH();
}

void SPI1_FLASH::flash_read_status(SPI_HandleTypeDef *SPI,uint8_t num,uint8_t* out_buf){
	uint8_t mosi_buf[2];
	uint8_t miso_buf[2];
	if(num==1)
		mosi_buf[0]=0x05;
	else if(num==2)
		mosi_buf[0]=0x35;
	else
		return;

	SPI_FLASH_CS_LOW();
	HAL_SPI_TransmitReceive(SPI,mosi_buf,miso_buf,2,2);
	SPI_FLASH_CS_HIGH();

	out_buf[0]=miso_buf[1];
	//outbuf[1]=miso_buf[2];
}

void SPI1_FLASH::flash_read_jedec_id(uint8_t* out_buf){
	uint8_t mosi_buf[4];
	uint8_t miso_buf[4];

	mosi_buf[0]=0x9f;

	SPI_FLASH_CS_LOW();
	HAL_SPI_TransmitReceive(&hspi1,mosi_buf,miso_buf,4,4);
	SPI_FLASH_CS_HIGH();

	for(int i=0;i<3;i++)
		out_buf[i]=miso_buf[i+1];
}

void SPI1_FLASH::flash_read_unique_id(SPI_HandleTypeDef *SPI,int8_t* out_buf){
	uint8_t mosi_buf[5];
	uint8_t miso_buf[8];

	mosi_buf[0]=0x4b;

	SPI_FLASH_CS_LOW();
	HAL_SPI_Transmit(SPI,mosi_buf,5,5);
	HAL_SPI_Receive(SPI,miso_buf,8,8);
	SPI_FLASH_CS_HIGH();

	for(int i=0;i<8;i++)
		out_buf[i]=miso_buf[i];
}

void SPI1_FLASH::flash_read(SPI_HandleTypeDef *SPI, uint32_t ReadAddr, uint8_t* out_buf, uint16_t size){
	uint8_t mosi_buf[4];
	while(flash_busy(SPI)==0x01);
	mosi_buf[0] = READ_CMD;
	mosi_buf[1] = (uint8_t)(ReadAddr >> 16);
	mosi_buf[2] = (uint8_t)(ReadAddr >> 8);
	mosi_buf[3] = (uint8_t)(ReadAddr);
	SPI_FLASH_CS_LOW();
	HAL_SPI_Transmit(SPI,mosi_buf,4,W25Qx_TIMEOUT);
	HAL_SPI_Receive(SPI,out_buf,size,W25Qx_TIMEOUT);
	SPI_FLASH_CS_HIGH();

}
//sector 0000-0FFF 4096BYTE (4KB)
void SPI1_FLASH::flash_sector_erase(SPI_HandleTypeDef *SPI,uint32_t Address){

	uint8_t cmd[4];
	uint32_t tickstart = HAL_GetTick();
	cmd[0] = SECTOR_ERASE_CMD;
	cmd[1] = (uint8_t)(Address >> 16);
	cmd[2] = (uint8_t)(Address >> 8);
	cmd[3] = (uint8_t)(Address);

	/* Enable write operations */
	//BSP_W25Qx_WriteEnable();
	flash_write_enable(SPI);

	/*Select the FLASH: Chip Select low */
	//W25Qx_Enable();
	SPI_FLASH_CS_LOW();
	/* Send the read ID command */
	HAL_SPI_Transmit(&hspi1, cmd, 4, W25Qx_TIMEOUT_VALUE);
	/*Deselect the FLASH: Chip Select high */
	//W25Qx_Disable();
	SPI_FLASH_CS_HIGH();
	/* Wait the end of Flash writing */
	while(BSP_W25Qx_GetStatus() == W25Qx_BUSY);
	{
		/* Check for the Timeout */
    if((HAL_GetTick() - tickstart) > W25Q128FV_SECTOR_ERASE_MAX_TIME)
    {
			return ;//W25Qx_TIMEOUT;
    }
	}
	//return W25Qx_OK;

}
void SPI1_FLASH::flash_erase_chip()
{
    uint8_t Write_Enable = 0x06;
    uint8_t Erase_Chip = 0xC7;

    SPI_FLASH_CS_LOW();
    HAL_SPI_Transmit(&hspi1,&Write_Enable,1,1); // Write Enable Command
    SPI_FLASH_CS_HIGH();

    SPI_FLASH_CS_LOW();
    HAL_SPI_Transmit(&hspi1,&Erase_Chip,1,1);   // Erase Chip Command
    SPI_FLASH_CS_HIGH();
}



//1page 256byte
void SPI1_FLASH::SPI_FLASH_PageWrite(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t size){
	SPI_HandleTypeDef *SPI=&hspi1;
	uint8_t mosi_buf[4];
	mosi_buf[0]=0x02;
	while(flash_busy(SPI)==0x01);

	for(int i=0;i<3;i++){
		mosi_buf[3-i]=WriteAddr&0x000000ff;
		WriteAddr=WriteAddr>>8;
	}

	flash_write_enable(SPI);
	SPI_FLASH_CS_LOW();
	HAL_SPI_Transmit(SPI,mosi_buf,4,4);
	HAL_SPI_Transmit(SPI,pBuffer,size,10);
	SPI_FLASH_CS_HIGH();
	//flash_write_disable();
}

/**
  * @brief  Writes block of data to the FLASH. In this function, the
  *         number of WRITE cycles are reduced, using Page WRITE sequence.
  * @param pBuffer : pointer to the buffer  containing the data to be
  *                  written to the FLASH.
  * @param WriteAddr : FLASH's internal address to write to.
  * @param NumByteToWrite : number of bytes to write to the FLASH.
  * @retval : None
  */
void SPI1_FLASH::SPI_FLASH_BufferWrite(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite)
{
  uint8_t NumOfPage = 0, NumOfSingle = 0, Addr = 0, count = 0, temp = 0;

  Addr = WriteAddr % SPI_FLASH_PageSize;
  count = SPI_FLASH_PageSize - Addr;
  NumOfPage =  NumByteToWrite / SPI_FLASH_PageSize;
  NumOfSingle = NumByteToWrite % SPI_FLASH_PageSize;

  if (Addr == 0) /* WriteAddr is SPI_FLASH_PageSize aligned  */
  {
    if (NumOfPage == 0) /* NumByteToWrite < SPI_FLASH_PageSize */
    {
      SPI_FLASH_PageWrite(pBuffer, WriteAddr, NumByteToWrite);
    }
    else /* NumByteToWrite > SPI_FLASH_PageSize */
    {
      while (NumOfPage--)
      {
        SPI_FLASH_PageWrite(pBuffer, WriteAddr, SPI_FLASH_PageSize);
        WriteAddr +=  SPI_FLASH_PageSize;
        pBuffer += SPI_FLASH_PageSize;
      }

      SPI_FLASH_PageWrite(pBuffer, WriteAddr, NumOfSingle);
    }
  }
  else /* WriteAddr is not SPI_FLASH_PageSize aligned  */
  {
    if (NumOfPage == 0) /* NumByteToWrite < SPI_FLASH_PageSize */
    {
      if (NumOfSingle > count) /* (NumByteToWrite + WriteAddr) > SPI_FLASH_PageSize */
      {
        temp = NumOfSingle - count;

        SPI_FLASH_PageWrite(pBuffer, WriteAddr, count);
        WriteAddr +=  count;
        pBuffer += count;

        SPI_FLASH_PageWrite(pBuffer, WriteAddr, temp);
      }
      else
      {
        SPI_FLASH_PageWrite(pBuffer, WriteAddr, NumByteToWrite);
      }
    }
    else /* NumByteToWrite > SPI_FLASH_PageSize */
    {
      NumByteToWrite -= count;
      NumOfPage =  NumByteToWrite / SPI_FLASH_PageSize;
      NumOfSingle = NumByteToWrite % SPI_FLASH_PageSize;

      SPI_FLASH_PageWrite(pBuffer, WriteAddr, count);
      WriteAddr +=  count;
      pBuffer += count;

      while (NumOfPage--)
      {
        SPI_FLASH_PageWrite(pBuffer, WriteAddr, SPI_FLASH_PageSize);
        WriteAddr +=  SPI_FLASH_PageSize;
        pBuffer += SPI_FLASH_PageSize;
      }

      if (NumOfSingle != 0)
      {
        SPI_FLASH_PageWrite(pBuffer, WriteAddr, NumOfSingle);
      }
    }
  }
}



