/**
  ******************************************************************************
  * @file SPI/M25P64_FLASH/spi_flash.h 
  * @author  MCD Application Team
  * @version  V3.0.0
  * @date  04/06/2009
  * @brief  Header for spi_flash.c file.
  ******************************************************************************
  * @copy
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2009 STMicroelectronics</center></h2>
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SPI_FLASH_H
#define __SPI_FLASH_H
#include "stm32f1xx_hal.h"
//#include "../view_page/o3_main_page.h"
#include "typedef.h"
#include "spi.h"

#define W25Q128FV_FLASH_SIZE                  0x1000000 /* 128 MBits => 16MBytes */
#define W25Q128FV_SECTOR_SIZE                 0x10000   /* 256 sectors of 64KBytes */
#define W25Q128FV_SUBSECTOR_SIZE              0x1000    /* 4096 subsectors of 4kBytes */
#define W25Q128FV_PAGE_SIZE                   0x100     /* 65536 pages of 256 bytes */

#define W25Q128FV_DUMMY_CYCLES_READ           4
#define W25Q128FV_DUMMY_CYCLES_READ_QUAD      10

#define W25Q128FV_BULK_ERASE_MAX_TIME         250000
#define W25Q128FV_SECTOR_ERASE_MAX_TIME       3000
#define W25Q128FV_SUBSECTOR_ERASE_MAX_TIME    800
#define W25Qx_TIMEOUT_VALUE 1000

/**
  * @brief  W25Q128FV Commands
  */
/* Reset Operations */
#define RESET_ENABLE_CMD                     0x66
#define RESET_MEMORY_CMD                     0x99

#define ENTER_QPI_MODE_CMD                   0x38
#define EXIT_QPI_MODE_CMD                    0xFF

/* Identification Operations */
#define READ_ID_CMD                          0x90
#define DUAL_READ_ID_CMD                     0x92
#define QUAD_READ_ID_CMD                     0x94
#define READ_JEDEC_ID_CMD                    0x9F


/* Read Operations */
#define READ_CMD                             0x03
#define FAST_READ_CMD                        0x0B
#define DUAL_OUT_FAST_READ_CMD               0x3B
#define DUAL_INOUT_FAST_READ_CMD             0xBB
#define QUAD_OUT_FAST_READ_CMD               0x6B
#define QUAD_INOUT_FAST_READ_CMD             0xEB

/* Write Operations */
#define WRITE_ENABLE_CMD                     0x06
#define WRITE_DISABLE_CMD                    0x04

/* Register Operations */
#define READ_STATUS_REG1_CMD                  0x05
#define READ_STATUS_REG2_CMD                  0x35
#define READ_STATUS_REG3_CMD                  0x15
#define WRITE_STATUS_REG1_CMD                 0x01
#define WRITE_STATUS_REG2_CMD                 0x31
#define WRITE_STATUS_REG3_CMD                 0x11

/* Program Operations */
#define PAGE_PROG_CMD                        0x02
#define QUAD_INPUT_PAGE_PROG_CMD             0x32

/* Erase Operations */
#define SECTOR_ERASE_CMD                     0x20
#define CHIP_ERASE_CMD                       0xC7
#define PROG_ERASE_RESUME_CMD                0x7A
#define PROG_ERASE_SUSPEND_CMD               0x75

/* Flag Status Register */
#define W25Q128FV_FSR_BUSY                    ((uint8_t)0x01)    /*!< busy */
#define W25Q128FV_FSR_WREN                    ((uint8_t)0x02)    /*!< write enable */
#define W25Q128FV_FSR_QE                      ((uint8_t)0x02)    /*!< quad enable */

#define W25Qx_Enable() 	HAL_GPIO_WritePin(SPI1_CS_GPIO_Port, SPI1_CS_Pin, GPIO_PIN_RESET)
#define W25Qx_Disable() HAL_GPIO_WritePin(SPI1_CS_GPIO_Port, SPI1_CS_Pin, GPIO_PIN_SET)

#define W25Qx_OK        ((uint8_t)0x00)
#define W25Qx_ERROR     ((uint8_t)0x01)
#define W25Qx_BUSY      ((uint8_t)0x02)
#define W25Qx_TIMEOUT	((uint8_t)0x03)

#define SUBSECTOR_SIZE 0x1000   /* 4096 subsectors of 4kBytes */
#define MAX_FLASH_ADDRESS 0x7FFFF //4MSPI FLASH
#define FLASH_SECTOR_SIZES 0x1000 //4KB(4096)

//#define ICON_FLASH_ADDRESS 0x50000;
#define ICON_FLASH_ADDRESS       0x00000;//0x00000-0x09000  icon
#define FLASH_ONE_DAY_RECORD_ADD 0x10000 //1 sector 4096Byte 확보
#define FLASH_SELJUNG_ADDRESS    0x7E000 //2 sector 8192Byte 확보
//#define FLASH_ONE_DAY_RECORD_ADD 0x7D000 //1 sector 4096Byte 확보

//#define FLASH_ONE_DAY_RECORD_ADD 0x01000 //2 sector 8192Byte 확보

#define SPI_FLASH_CS_LOW() HAL_GPIO_WritePin(SPI1_CS_GPIO_Port, SPI1_CS_Pin, GPIO_PIN_RESET)
#define SPI_FLASH_CS_HIGH() HAL_GPIO_WritePin(SPI1_CS_GPIO_Port, SPI1_CS_Pin, GPIO_PIN_SET)



typedef struct {
  uint16_t header; // 1
  uint16_t nheader;	//1
  uint16_t o3;
  int temperature;
  uint16_t factor;
  uint16_t on_time;
  uint16_t off_time;
  uint16_t total_time;
}CONTROL_TARGET;


class SPI1_FLASH {
public:
	CONTROL_TARGET eeprom;
	SPI1_FLASH();
	virtual ~SPI1_FLASH();
	void SPI_FLASH_Init(void);
	void SPI_FLASH_RETRIEVE_SELJUNG(void);
	uint8_t flash_busy(SPI_HandleTypeDef *SPI);
	void flash_write_enable(SPI_HandleTypeDef *SPI);
	void flash_write_disable(SPI_HandleTypeDef *SPI);
	void flash_read_status(SPI_HandleTypeDef *SPI,uint8_t num,uint8_t* out_buf);
	//void flash_read_jedec_id(SPI_HandleTypeDef *SPI,uint8_t* out_buf);
	void flash_read_jedec_id(uint8_t* out_buf);

	void flash_read_unique_id(SPI_HandleTypeDef *SPI,int8_t* out_buf);
	void flash_page_write(SPI_HandleTypeDef *SPI,uint32_t address ,uint8_t* data_buf, uint16_t size);
	void flash_read(SPI_HandleTypeDef *SPI, uint32_t ReadAddr, uint8_t* out_buf, uint16_t size);
	void flash_sector_erase(SPI_HandleTypeDef *SPI,uint32_t Address);
	void flash_erase_chip();
	void SPI_FLASH_UPDATE_SELJUNG(void);
	void SPI_FLASH_SAVE_RECORD(uint8_t *wdata, uint16_t size);
	//RECORD_POST SPI_FLASH_Read_RECORD();
	void SPI_FLASH_WRITER_SECTOR(uint32_t wadd, uint8_t *wdata, uint16_t size);
	//void SPI_FLASH_PageWrite(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite);
	//void SPI_FLASH_PageWrite(uint32_t address ,uint8_t* data_buf, uint16_t size);
	void SPI_FLASH_PageWrite(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t size);
	void SPI_FLASH_BufferWrite(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite);
	void SPI_FLASH_WriteEnable(void);
	void SPI_FLASH_WaitForWriteEnd(void);

	uint8_t BSP_W25Qx_GetStatus(void);
	uint8_t BSP_W25Qx_WriteEnable(void);
	void BSP_W25Qx_Read_ID(uint8_t *ID);
	uint8_t BSP_W25Qx_Read(uint8_t* pData, uint32_t ReadAddr, uint32_t Size);
	uint8_t BSP_W25Qx_Write(uint8_t* pData, uint32_t WriteAddr, uint32_t Size);
	uint8_t BSP_W25Qx_Erase_Block(uint32_t Address);
	uint8_t BSP_W25Qx_Erase_Chip(void);
};


#endif /* SPI_FLASH_H_ */

/******************* (C) COPYRIGHT 2009 STMicroelectronics *****END OF FILE****/
