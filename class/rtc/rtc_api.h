/*
 * CALRENDER.h
 *
 *  Created on: Jan 17, 2022
 *      Author: thpark
 */
#ifndef CALRENDAR_H_
#define CALRENDAR_H_

#include "typedef.h"
//#include "./lcd/driver/ili9488_drv.h"



#define UPDATE_GRAPH_MINUTE_PERIOD 2


class RTC_API
{

public:
	TYPEDEF_TIME_DATA rtc_time;
	struct tm rtc_timeinfo;
	time_t rtc_current_ltime;

	uint8_t ex_sec=0;
	uint16_t sec_cnt;
	RTC_API();
	virtual ~RTC_API();
	TYPEDEF_TIME_DATA RTC_GetRTC(void);
	void Update_system_time(void);
	void Update_system_time_to_setup_menu(void);
	uint32_t RTC_DateToBinary(struct tm *datetime);
	struct tm RTC_BinaryToDate(uint32_t binary);
	//void RTC_BinaryToDate(uint32_t binary, struct tm *datetime);
	void Set_RTC(struct tm timeinfo);

};

#endif /* CALRENDAR_H_ */
