/*
 * CALRENDER.cpp
 *
 *  Created on: Jan 17, 2022
 *      Author: thpark
 */
#include "extern.h"
#include "rtc_api.h"
#include "rtc.h"
#include "typedef.h"
#include <ctime>
#include <stdlib.h>

#define DBG_RTC_API

RTC_API *pRTC_API;
extern RTC_HandleTypeDef hrtc;
//extern WIND_SENSOR *pWind;
extern RS485 *pRS485;
extern DATA_CLASS *pDataClass;



RTC_API::RTC_API() {
	// TODO Auto-generated constructor stub

}

RTC_API::~RTC_API() {
	// TODO Auto-generated destructor stub
}

// this array represents the number of days in one non-leap year at
// the beginning of each month
uint32_t RTC_API::RTC_DateToBinary(struct tm *datetime) {
	uint32_t iday;
	uint32_t val;
	uint32_t DaysToMonth[13] = { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304,	334, 365 };

	iday = 365 * (datetime->tm_year - 70) + DaysToMonth[datetime->tm_mon - 1]+ (datetime->tm_mday - 1);
	iday = iday + (datetime->tm_year - 69) / 4;
	if ((datetime->tm_mon > 2) && ((datetime->tm_year % 4) == 0)) {
		iday++;
	}
	//printf("DAYS[%d]\r\n", iday);
	val = datetime->tm_sec + 60 * datetime->tm_min
			+ 3600 * (datetime->tm_hour + 24 * iday);
	return val;
}

struct tm RTC_API::RTC_BinaryToDate(uint32_t binary) {
	struct tm datetime;
	uint32_t DaysToMonth[13] = { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304,	334, 365 };
#if 0
	uint32_t hour;
	uint32_t day;
	uint32_t minute;
	uint32_t second;
	uint32_t month;
	uint32_t year;

	uint32_t whole_minutes;
	uint32_t whole_hours;
	uint32_t whole_days;
	uint32_t whole_days_since_1968;
	uint32_t leap_year_periods;
	uint32_t days_since_current_lyear;
	uint32_t whole_years;
	uint32_t days_since_first_of_year;
	uint32_t days_to_month;
	uint32_t day_of_week;
	//uint32_t DaysToMonth[13] = { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304,	334, 365 };
	whole_minutes = binary / 60;
	second = binary - (60 * whole_minutes); // leftover seconds

	whole_hours = whole_minutes / 60;
	minute = whole_minutes - (60 * whole_hours); // leftover minutes
	whole_days = whole_hours / 24;
	hour = whole_hours - (24 * whole_days); // leftover hours
	printf("RTC_BinaryToDate whole_days[%d] [%d:%d:%d]\r\n",whole_days, hour, minute, second );

	whole_days_since_1968 = whole_days + 365 + 366;
	leap_year_periods = whole_days_since_1968 / ((4 * 365) + 1);

	days_since_current_lyear = whole_days_since_1968 % ((4 * 365) + 1);

// if days are after a current leap year then add a leap year period
	if ((days_since_current_lyear >= (31 + 29))) {
		leap_year_periods++;
	}
	whole_years = (whole_days_since_1968 - leap_year_periods) / 365;
	days_since_first_of_year = whole_days_since_1968 - (whole_years * 365)
			- leap_year_periods;

	if ((days_since_current_lyear <= 365) && (days_since_current_lyear >= 60)) {
		days_since_first_of_year++;
	}
	year = whole_years + 68;

	printf("RTC_BinaryToDate year[%d] whole_years[%d]\r\n",year, whole_years );
// setup for a search for what month it is based on how many days have past
// within the current year
	month = 13;
	days_to_month = 366;
	printf("days_since_first_of_year[%d] days_to_month[%d]\r\n",days_since_first_of_year, days_to_month );

	while (days_since_first_of_year < days_to_month) {
		month--;
		days_to_month = DaysToMonth[month - 1];
		printf("while--- days_since_first_of_year[%d] month[%d] days_to_month[%d]\r\n",days_since_first_of_year, month, days_to_month );

		if ((month > 2) && ((year % 4) == 0)) {
			days_to_month++;
		}
		days_to_month++;
	}

	day = days_since_first_of_year - days_to_month + 1;
	printf("---day[%d] month[%d]\r\n",day, month);
	day_of_week = (whole_days + 4) % 7;
	datetime.tm_yday = days_since_first_of_year; /* days since January 1 - [0,365] */
	datetime.tm_sec = second; /* seconds after the minute - [0,59] */
	datetime.tm_min = minute; /* minutes after the hour - [0,59] */
	datetime.tm_hour = hour; /* hours since midnight - [0,23] */
	datetime.tm_mday = day; /* day of the month - [1,31] */
	datetime.tm_wday = day_of_week; /* days since Sunday - [0,6] */
	datetime.tm_mon = month; /* months since January - [0,11] */
	datetime.tm_year = year; /* years since 1900 */
#endif

	unsigned long hour;
	     unsigned long day;
	     unsigned long minute;
	     unsigned long second;
	     unsigned long month;
	     unsigned long year;

	     unsigned long whole_minutes;
	     unsigned long whole_hours;
	     unsigned long whole_days;
	     unsigned long whole_days_since_1968;
	     unsigned long leap_year_periods;
	     unsigned long days_since_current_lyear;
	     unsigned long whole_years;
	     unsigned long days_since_first_of_year;
	     unsigned long days_to_month;
	     unsigned long day_of_week;

	     whole_minutes = binary / 60;
	     second = binary - (60 * whole_minutes);                 // leftover seconds

	     whole_hours  = whole_minutes / 60;
	     minute = whole_minutes - (60 * whole_hours);            // leftover minutes

	     whole_days   = whole_hours / 24;
	     hour         = whole_hours - (24 * whole_days);         // leftover hours

	     whole_days_since_1968 = whole_days + 365 + 366;
	     leap_year_periods = whole_days_since_1968 / ((4 * 365) + 1);

	     days_since_current_lyear = whole_days_since_1968 % ((4 * 365) + 1);

	     // if days are after a current leap year then add a leap year period
	     if ((days_since_current_lyear >= (31 + 29))) {
	         leap_year_periods++;
	     }
	     whole_years = (whole_days_since_1968 - leap_year_periods) / 365;
	     days_since_first_of_year = whole_days_since_1968 - (whole_years * 365) - leap_year_periods;

	     if ((days_since_current_lyear <= 365) && (days_since_current_lyear >= 60)) {
	         days_since_first_of_year++;
	     }
	     year = whole_years + 68;

	     // setup for a search for what month it is based on how many days have past
	     //   within the current year
	     month = 13;
	     days_to_month = 366;
	     while (days_since_first_of_year < days_to_month) {
	         month--;
	         days_to_month = DaysToMonth[month];
	         if ((month >= 2) && ((year % 4) == 0))
	         {
	             days_to_month++;
	         }
	     }
	     day = days_since_first_of_year - days_to_month + 1;

	     day_of_week = (whole_days  + 4) % 7;

	     datetime.tm_yday = days_since_first_of_year;      /* days since January 1 - [0,365]       */
	     datetime.tm_sec  = second;        /* seconds after the minute - [0,59]    */
	     datetime.tm_min  = minute;        /* minutes after the hour - [0,59]      */
	     datetime.tm_hour = hour;          /* hours since midnight - [0,23]        */
	     datetime.tm_mday = day;           /* day of the month - [1,31]            */
	     datetime.tm_wday = day_of_week;   /* days since Sunday - [0,6]            */
	     datetime.tm_mon  = month;         /* months since January - [0,11]        */
	     datetime.tm_year = year;          /* years since 1900                     */
		return datetime;
}


TYPEDEF_TIME_DATA  RTC_API::RTC_GetRTC(void) {
	TYPEDEF_TIME_DATA time_data;
	struct tm timeinfo;
	RTC_TimeTypeDef myTime;
	RTC_DateTypeDef myDate;
	HAL_RTC_GetTime(&hrtc, &myTime, FORMAT_BIN);
	HAL_RTC_GetDate(&hrtc, &myDate, FORMAT_BIN);
	timeinfo.tm_wday = myDate.WeekDay;
	timeinfo.tm_mon = myDate.Month;
	timeinfo.tm_mday = myDate.Date;
	timeinfo.tm_year = myDate.Year + 2000;//STM32 RTC는 녕도는 한바이트처리하고 다시 시작하면 날짜정보는 없어져 버린다.
	timeinfo.tm_hour = myTime.Hours;
	timeinfo.tm_min = myTime.Minutes;
	timeinfo.tm_sec = myTime.Seconds;
	//rtc_current_ltime = mktime(&timeinfo); //unit of second
	time_data.ltime = RTC_DateToBinary(&timeinfo); //unit of second
	//time_data.ltime = mktime(&timeinfo); //unit of second
	time_data.timeinfo=timeinfo;
	return time_data;

}

void RTC_API::Update_system_time(void) {
	static uint8_t ex_min = 0xff;
	static uint8_t minCnt = 0;

	uint8_t min, lsec;

	RTC_GetRTC();
	//ltime = mktime(&timeinfo); //unit of second
	min = rtc_timeinfo.tm_min;
	lsec= rtc_timeinfo.tm_sec;
}


void RTC_API::Update_system_time_to_setup_menu(void) {
	struct tm timeinfo;
	timeinfo=pDataClass->nowTimeInfo.timeinfo;
#ifdef DBG_RTC_API
	printf("Update_system_time_to_setup_menu [%d]\r\n", timeinfo.tm_year);
#endif
}

void RTC_API::Set_RTC(struct tm timeinfo) {

	RTC_TimeTypeDef sTime;
	RTC_DateTypeDef sDate;

	sDate.Year  = timeinfo.tm_year+1900;
	sDate.Month = timeinfo.tm_mon+1;
	sDate.Date  = timeinfo.tm_mday;

	sTime.Hours = timeinfo.tm_hour;
	sTime.Minutes = timeinfo.tm_min;
	sTime.Seconds = timeinfo.tm_sec;

#ifdef DBG_RTC_API
	printf("Set_RTC : %d-%d-%d\r\n", timeinfo.tm_year, timeinfo.tm_mon, timeinfo.tm_mday );
#endif
	//sDate.WeekDay = RTC_WEEKDAY_MONDAY;
	HAL_RTC_SetTime(&hrtc, &sTime, FORMAT_BIN);//RTC_Format_BIN
	HAL_RTC_SetDate(&hrtc, &sDate, FORMAT_BIN);
	//0x32F0x32F2
	//HAL_RTCEx_BKUPWrite(&hrtc, RTC_BKP_DR1, 0x32F2);

}
