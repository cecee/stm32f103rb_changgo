/*
 * TEMPI2C.cpp
 *
 *  Created on: Mar 31, 2022
 *      Author: thpark
 */
#if 0
#include "extern.h"
#include "TEMPI2C.h"

#define xDBG_STH20

TEMP_I2C *pSHT20;

TEMP_I2C::TEMP_I2C()
{
	// TODO Auto-generated constructor stub
	I2c_Init();
	SHT20_init();
}

TEMP_I2C::~TEMP_I2C()
{
	// TODO Auto-generated destructor stub
}


void TEMP_I2C::I2c_Init ()
//==============================================================================
{
	SET_MASTER_SDA(LOW);//SDA=LOW;                // Set port as output for configuration
	SET_MASTER_SCL(LOW);//SCL=LOW;                // Set port as output for configuration
  //SDA_CONF=LOW;           // Set SDA level as low for output mode
  //SCL_CONF=LOW;           // Set SCL level as low for output mode
	SET_MASTER_SDA(HIGH);//SDA=HIGH;               // I2C-bus idle mode SDA released (input)
	SET_MASTER_SCL(HIGH);//SCL=HIGH;               // I2C-bus idle mode SCL released (input)
}


void TEMP_I2C::SHT20_init()
{
	uint8_t error;
	uint8_t  userRegister;        //variable for user register
	//  bt   endOfBattery;        //variable for end of battery
	 //float   temperatureC=0;    //variable for temperature[캜] as float
	 //float   humidityRH=0;      //variable for relative humidity[%RH] as float
//	 char humitityOutStr[21];     //output string for humidity value
	// I2c_Init ();
	error=SHT2x_SoftReset();
	error |= SHT2x_GetSerialNumber(SerialNumber_SHT2x);
	// --- Set Resolution e.g. RH 10bit, Temp 13bit ---
	error |= SHT2x_ReadUserRegister(&userRegister);  //get actual user reg
    userRegister = (userRegister & ~SHT2x_RES_MASK) | SHT2x_RES_10_13BIT;
    error |= SHT2x_WriteUserRegister(&userRegister); //write changed user reg
	//printf("### error[%d] userRegister[%d]\r\n",error, userRegister);
	//printf("### error[%d] SerialNumber_SHT2x[%x][%x][%x][%x]\r\n",error, SerialNumber_SHT2x[0], SerialNumber_SHT2x[1], SerialNumber_SHT2x[2], SerialNumber_SHT2x[3]);
	    // --- measure humidity with "Hold Master Mode (HM)"  ---
	   // error |= SHT2x_MeasureHM(HUMIDITY, &sRH);
#if 0
	    // --- measure temperature with "Polling Mode" (no hold master) ---
	    error |= SHT2x_MeasurePoll(TEMP, &sT);
	    error |= SHT2x_MeasurePoll(HUMIDITY, &sRH);
	    //-- calculate humidity and temperature --
	    temperatureC = SHT2x_CalcTemperatureC(sT.u16);
	    humidityRH   = SHT2x_CalcRH(sRH.u16);
#endif
	//printf("SHT20_init error[%d][%.1f][%.1f]\r\n",error, temperatureC, humidityRH);
}

SENSOR_BOX TEMP_I2C::Get_SHT20(void){
	SENSOR_BOX sht20;
	uint8_t error;
	 float   temperatureC=0;     //variable for temperature[캜] as float
	 float   humidityRH=0;       //variable for relative humidity[%RH] as float
//	 char humitityOutStr[21];     //output string for humidity value
	 nt16 sT;                     //variable for raw temperature ticks
	 nt16 sRH;                    //variable for raw humidity ticks

    error |= SHT2x_MeasurePoll(TEMP, &sT);
    error |= SHT2x_MeasurePoll(HUMIDITY, &sRH);
    //-- calculate humidity and temperature --
    if(error==0){
    temperatureC = SHT2x_CalcTemperatureC(sT.u16);
    humidityRH   = SHT2x_CalcRH(sRH.u16);
    sht20.temperature=(int)(temperatureC*10);
    sht20.humidityRH=(int)(humidityRH*10);
    sht20.exist=1;
    }
    else{
        sht20.temperature=-99;
        sht20.humidityRH=-99;
        sht20.exist=0;
    }
#ifdef DBG_STH20
    printf("SHT20_init error[%d][%.1f][%.1f]\r\n",error, temperatureC, humidityRH);
#endif
    return sht20;
}


void  TEMP_I2C::SET_MASTER_SDA(uint8_t SDA_status){
   RELEASE_MASTER_SDA(0);//out put mode
  if(SDA_status==1)  HAL_GPIO_WritePin(SHT_SDA_GPIO_Port, SHT_SDA_Pin, GPIO_PIN_SET);
  else HAL_GPIO_WritePin(SHT_SDA_GPIO_Port, SHT_SDA_Pin, GPIO_PIN_RESET);
  //RELEASE_MASTER_SDA(1);//input mode
}
void  TEMP_I2C::SET_MASTER_SCL(uint8_t SCL_status){
  if(SCL_status==1)  HAL_GPIO_WritePin(SHT_SCL_GPIO_Port, SHT_SCL_Pin, GPIO_PIN_SET);
  else HAL_GPIO_WritePin(SHT_SCL_GPIO_Port, SHT_SCL_Pin, GPIO_PIN_RESET);
}

void TEMP_I2C::RELEASE_MASTER_SDA(uint8_t input){
	if(input){
		SHT_SDA_GPIO_Port->CRL&=0xFFF0FFFF;//0x4444 4444
		SHT_SDA_GPIO_Port->CRL|=0x00040000;//4->floating 8->pullup (SHT_SDA_Pin);//0x4444 4444
	}
	else{
		SHT_SDA_GPIO_Port->CRL&=0xFFF0FFFF;//0x4444 4444
		SHT_SDA_GPIO_Port->CRL|=0x00030000;//0011b maxspeed 50MHZ 0x4444 4444
	}
}

uint8_t TEMP_I2C::GET_MASTER_SDA(void){
	uint8_t ret=0;
//	uint32_t tt;
#if 0
	//uint32_t tt;
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = SHT_SDA_Pin;
//	//GPIO_InitStruct.Pull= GPIO_PULLUP;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	HAL_GPIO_Init(SHT_SDA_GPIO_Port, &GPIO_InitStruct);
//	//DelayMicroSeconds(3);
	ret=HAL_GPIO_ReadPin(SHT_SDA_GPIO_Port,SHT_SDA_Pin);
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
//	//GPIO_InitStruct.Pin = SHT_SDA_Pin;
//	GPIO_InitStruct.Pull= GPIO_PULLUP;
	HAL_GPIO_Init(SHT_SDA_GPIO_Port, &GPIO_InitStruct);
	//tt=SHT_SDA_GPIO_Port->CRL;
	//printf("SHT_SDA_GPIO_Port[%08x]\r\n", tt);
#else
	ret = _CheckBit(SHT_SDA_GPIO_Port->IDR, 4);// SHT_SDA_GPIO_Port->IDR;
#endif
	return ret;
}
void TEMP_I2C::DelayMicroSeconds (u32t nbrOfUs)
//==============================================================================
{
  for(u32t i=0; i<nbrOfUs; i++)
  {
	//for(int ix=0;ix<20;ix++)
	  __asm("nop");  //nop's may be added for timing adjustment
	//__asm("nop");
	//__asm("nop");
	//__asm("nop");
  }
}
//void TEMP_I2C::DelayMicroSeconds(uint16_t uscount)
//{
//  uint8_t  x;
//  for (x=0; x<uscount; x++){;}
//}


//==============================================================================
void TEMP_I2C::I2c_StartCondition()
//==============================================================================
{
	SET_MASTER_SDA(HIGH);//SDA=HIGH;
	SET_MASTER_SCL(HIGH);//SCL=HIGH;
	SET_MASTER_SDA(LOW);//SDA=LOW;
	DelayMicroSeconds(10);  // hold time start condition (t_HD;STA)
	SET_MASTER_SCL(LOW);//SCL=LOW;
	DelayMicroSeconds(10);
}

//==============================================================================
void TEMP_I2C::I2c_StopCondition ()
//==============================================================================
{
	SET_MASTER_SDA(LOW);//SDA=LOW;
	SET_MASTER_SCL(LOW);//SCL=LOW;
	SET_MASTER_SCL(HIGH);//SCL=HIGH;
	DelayMicroSeconds(10);  // set-up time stop condition (t_SU;STO)
	SET_MASTER_SDA(HIGH);//SDA=HIGH;
	DelayMicroSeconds(10);
}

//==============================================================================
uint8_t TEMP_I2C::I2c_WriteByte (volatile uint8_t txByte)
//==============================================================================
{
  uint8_t mask,error=0;
  for (mask=0x80; mask>0; mask>>=1)   //shift bit for masking (8 times)
  {
	if ((mask & txByte) == 0)SET_MASTER_SDA(LOW);// SDA=LOW;//masking txByte, write bit to SDA-Line
    else SET_MASTER_SDA(HIGH);//SDA=HIGH;
    DelayMicroSeconds(1);             //data set-up time (t_SU;DAT)
    SET_MASTER_SCL(HIGH);//SCL=HIGH;                         //generate clock pulse on SCL
    DelayMicroSeconds(5);             //SCL high time (t_HIGH)
    SET_MASTER_SCL(LOW);//SCL=LOW;
    DelayMicroSeconds(1);             //data hold time(t_HD;DAT)
  }
  //SET_MASTER_SDA(HIGH);//SDA=HIGH;   //release SDA-line
  RELEASE_MASTER_SDA(1);
  DelayMicroSeconds(1);
  SET_MASTER_SCL(HIGH);//SCL=HIGH;   //clk #9 for ack
  //DelayMicroSeconds(1);              //data set-up time (t_SU;DAT)
  //if(SDA_CONF==HIGH) error=ACK_ERROR; //check ack from i2c slave
  if (GET_MASTER_SDA() != LOW) error=ACK_ERROR;
  //RELEASE_MASTER_SDA(0);
  DelayMicroSeconds(1);
  SET_MASTER_SCL(LOW);//SCL=LOW;
  DelayMicroSeconds(20);              //wait time to see byte package on scope
 // printf("I2c_WriteByte error[%d] txByte[%d]\r\n",error, txByte);
  return error;                       //return error code

}

//==============================================================================
uint8_t TEMP_I2C::I2c_ReadByte (etI2cAck ack)
//==============================================================================
{
	uint8_t mask,rxByte=0;
 // SDA=HIGH;                           //release SDA-line
  //SET_MASTER_SDA(HIGH);
  RELEASE_MASTER_SDA(1);
  for (mask=0x80; mask>0; mask>>=1)   //shift bit for masking (8 times)
  {
	//RELEASE_MASTER_SDA(1);
	SET_MASTER_SCL(HIGH);//SCL=HIGH;  //start clock on SCL-line

    DelayMicroSeconds(1);             //data set-up time (t_SU;DAT)
    DelayMicroSeconds(3);             //SCL high time (t_HIGH)
    if (GET_MASTER_SDA() != LOW) rxByte=(rxByte | mask); //read bit
    //if (SDA_CONF==1) rxByte=(rxByte | mask); //read bit
    SET_MASTER_SCL(LOW);//SCL=LOW;
    DelayMicroSeconds(1);             //data hold time(t_HD;DAT)
  }
 // RELEASE_MASTER_SDA(0);

  SET_MASTER_SDA(ack);//SDA=ack;      //send acknowledge if necessary
  DelayMicroSeconds(1);               //data set-up time (t_SU;DAT)
  SET_MASTER_SCL(HIGH);//SCL=HIGH;    //clk #9 for ack
  DelayMicroSeconds(5);               //SCL high time (t_HIGH)
  SET_MASTER_SCL(LOW);//SCL=LOW;
 // SET_MASTER_SDA(HIGH);//SDA=HIGH;    //release SDA-line
  RELEASE_MASTER_SDA(1);
  DelayMicroSeconds(20);              //wait time to see byte package on scope
  return rxByte;                      //return error code
}



//===========================================================================
uint8_t TEMP_I2C::SHT2x_SoftReset()
//===========================================================================
{
  uint8_t  error=0;           //error variable
  I2c_StartCondition();
  error |= I2c_WriteByte(I2C_ADR_W); // I2C Adr
  error |= I2c_WriteByte(SOFT_RESET);// Command
  I2c_StopCondition();
  DelayMicroSeconds(15000); // wait till sensor has restarted
  return error;
}


//==============================================================================
uint8_t TEMP_I2C::SHT2x_CheckCrc(uint8_t data[], uint8_t nbrOfBytes, uint8_t checksum)
//==============================================================================
{
	uint8_t crc = 0;
	uint8_t byteCtr;
  //calculates 8-Bit checksum with given polynomial
  for (byteCtr = 0; byteCtr < nbrOfBytes; ++byteCtr)
  { crc ^= (data[byteCtr]);
    for (uint8_t bit = 8; bit > 0; --bit)
    { if (crc & 0x80) crc = (crc << 1) ^ POLYNOMIAL;
      else crc = (crc << 1);
    }
  }
  if (crc != checksum) return CHECKSUM_ERROR;
  else return 0;
}

//===========================================================================
uint8_t TEMP_I2C::SHT2x_ReadUserRegister(uint8_t *pRegisterValue)
//===========================================================================
{
	uint8_t checksum;   //variable for checksum byte
	uint8_t error=0;    //variable for error code
	uint8_t rdata;
  I2c_StartCondition();
  error |= I2c_WriteByte (I2C_ADR_W);
  error |= I2c_WriteByte (USER_REG_R);
  I2c_StartCondition();
  error |= I2c_WriteByte (I2C_ADR_R);
 // rdata= I2c_ReadByte(ACK);
  *pRegisterValue = I2c_ReadByte(ACK);
  checksum=I2c_ReadByte(NO_ACK);

  //printf("error[%d] rdata[%x] checksum[%x]\r\n",error, rdata, checksum);
  error |= SHT2x_CheckCrc (pRegisterValue,1,checksum);
  //error |= SHT2x_CheckCrc (&rdata,1,checksum);
  I2c_StopCondition();
  return error;
}

//===========================================================================
uint8_t TEMP_I2C::SHT2x_WriteUserRegister(uint8_t *pRegisterValue)
//===========================================================================
{
	uint8_t error=0;   //variable for error code

  I2c_StartCondition();
  error |= I2c_WriteByte (I2C_ADR_W);
  error |= I2c_WriteByte (USER_REG_W);
  error |= I2c_WriteByte (*pRegisterValue);
  I2c_StopCondition();
  return error;
}

//===========================================================================
uint8_t TEMP_I2C::SHT2x_MeasureHM(etSHT2xMeasureType eSHT2xMeasureType, nt16 *pMeasurand)
//===========================================================================
{
	uint8_t  checksum;   //checksum
	uint8_t  data[2];    //data array for checksum verification
	uint8_t  error=0;    //error variable
	uint16_t i;          //counting variable

  //-- write I2C sensor address and command --
  I2c_StartCondition();
  error |= I2c_WriteByte (I2C_ADR_W); // I2C Adr
  switch(eSHT2xMeasureType)
  { case HUMIDITY: error |= I2c_WriteByte (TRIG_RH_MEASUREMENT_HM); break;
    case TEMP    : error |= I2c_WriteByte (TRIG_T_MEASUREMENT_HM);  break;
    default: assert(0);
  }
  //-- wait until hold master is released --
  I2c_StartCondition();
  error |= I2c_WriteByte (I2C_ADR_R);
  SET_MASTER_SCL(HIGH);//SCL=HIGH;                     // set SCL I/O port as input
  for(i=0; i<1000; i++)         // wait until master hold is released or
  { DelayMicroSeconds(1000);    // a timeout (~1s) is reached
    //if (SCL_CONF==1) break;
    if (GET_MASTER_SDA() != LOW) break;
  }
  //-- check for timeout --
  //if(SCL_CONF==0) error |= TIME_OUT_ERROR;
  if (GET_MASTER_SDA() == LOW) error=ACK_ERROR;
  //-- read two data bytes and one checksum byte --
  //pMeasurand->s16.u8H =
		  data[0] = I2c_ReadByte(ACK);
 // pMeasurand->s16.u8L =
		  data[1] = I2c_ReadByte(ACK);
  checksum=I2c_ReadByte(NO_ACK);


  //-- verify checksum --
  error |= SHT2x_CheckCrc (data,2,checksum);
  I2c_StopCondition();
  return error;
}

//===========================================================================
uint8_t TEMP_I2C::SHT2x_MeasurePoll(etSHT2xMeasureType eSHT2xMeasureType, nt16 *pMeasurand)
//===========================================================================
{
	uint8_t  checksum;   //checksum
	uint8_t  data[2];    //data array for checksum verification
	uint8_t  error=0;    //error variable
	uint16_t i=0;        //counting variable

  //-- write I2C sensor address and command --
  I2c_StartCondition();
  error |= I2c_WriteByte (I2C_ADR_W); // I2C Adr
  switch(eSHT2xMeasureType)
  { case HUMIDITY: error |= I2c_WriteByte (TRIG_RH_MEASUREMENT_POLL); break;
    case TEMP    : error |= I2c_WriteByte (TRIG_T_MEASUREMENT_POLL);  break;
    default: assert(0);
  }
  //-- poll every 10ms for measurement ready. Timeout after 20 retries (200ms)--
  do
  { I2c_StartCondition();
    //DelayMicroSeconds(10000);  //delay 10ms
    HAL_Delay(10);
    if(i++ >= 20) break;
  } while(I2c_WriteByte (I2C_ADR_R) == ACK_ERROR);
  if (i>=20) error |= TIME_OUT_ERROR;

  //printf("---- SHT2x_MeasurePoll error[%d] data[%x][%x]  checksum[%x]\r\n",error, data[0], data[1], checksum);

  //-- read two data bytes and one checksum byte --
  pMeasurand->s16.u8H = data[0] = I2c_ReadByte(ACK);
  pMeasurand->s16.u8L = data[1] = I2c_ReadByte(ACK);
  checksum=I2c_ReadByte(NO_ACK);
   //-- verify checksum --
  error |= SHT2x_CheckCrc (data,2,checksum);
  I2c_StopCondition();
  //printf("---- SHT2x_MeasurePoll error[%d] data[%x][%x]  checksum[%x]\r\n",error, data[0], data[1], checksum);

  return error;
}


//==============================================================================
float TEMP_I2C::SHT2x_CalcRH(uint16_t u16sRH)
//==============================================================================
{
	float humidityRH;              // variable for result

  u16sRH &= ~0x0003;          // clear bits [1..0] (status bits)
  //-- calculate relative humidity [%RH] --

  humidityRH = -6.0 + 125.0/65536 * (float)u16sRH; // RH= -6 + 125 * SRH/2^16
  return humidityRH;
}

//==============================================================================
float TEMP_I2C::SHT2x_CalcTemperatureC(uint16_t u16sT)
//==============================================================================
{
	float temperatureC;            // variable for result

  u16sT &= ~0x0003;           // clear bits [1..0] (status bits)

  //-- calculate temperature [캜] --
  temperatureC= -46.85 + 175.72/65536 *(float)u16sT; //T= -46.85 + 175.72 * ST/2^16
  return temperatureC;
}

//==============================================================================
uint8_t TEMP_I2C::SHT2x_GetSerialNumber(uint8_t u8SerialNumber[])
//==============================================================================
{
	uint8_t  error=0;                          //error variable

  //Read from memory location 1
  I2c_StartCondition();
  error |= I2c_WriteByte (I2C_ADR_W);    //I2C address
  error |= I2c_WriteByte (0xFA);         //Command for readout on-chip memory
  error |= I2c_WriteByte (0x0F);         //on-chip memory address

  I2c_StartCondition();
  error |= I2c_WriteByte (I2C_ADR_R);    //I2C address

  u8SerialNumber[5] = I2c_ReadByte(ACK); //Read SNB_3
  I2c_ReadByte(ACK);                     //Read CRC SNB_3 (CRC is not analyzed)
  u8SerialNumber[4] = I2c_ReadByte(ACK); //Read SNB_2
  I2c_ReadByte(ACK);                     //Read CRC SNB_2 (CRC is not analyzed)
  u8SerialNumber[3] = I2c_ReadByte(ACK); //Read SNB_1
  I2c_ReadByte(ACK);                     //Read CRC SNB_1 (CRC is not analyzed)
  u8SerialNumber[2] = I2c_ReadByte(ACK); //Read SNB_0
  I2c_ReadByte(NO_ACK);                  //Read CRC SNB_0 (CRC is not analyzed)
  I2c_StopCondition();

  //Read from memory location 2
  I2c_StartCondition();
  error |= I2c_WriteByte (I2C_ADR_W);    //I2C address
  error |= I2c_WriteByte (0xFC);         //Command for readout on-chip memory
  error |= I2c_WriteByte (0xC9);         //on-chip memory address
  I2c_StartCondition();
  error |= I2c_WriteByte (I2C_ADR_R);    //I2C address
  u8SerialNumber[1] = I2c_ReadByte(ACK); //Read SNC_1
  u8SerialNumber[0] = I2c_ReadByte(ACK); //Read SNC_0
  I2c_ReadByte(ACK);                     //Read CRC SNC0/1 (CRC is not analyzed)
  u8SerialNumber[7] = I2c_ReadByte(ACK); //Read SNA_1
  u8SerialNumber[6] = I2c_ReadByte(ACK); //Read SNA_0
  I2c_ReadByte(NO_ACK);                  //Read CRC SNA0/1 (CRC is not analyzed)
  I2c_StopCondition();
  //printf("###SHT2x_GetSerialNumber error[%d]  u8SerialNumber[%x]\r\n", error, u8SerialNumber[0]);
  return error;
}

#endif
