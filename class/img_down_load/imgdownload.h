/*
 * imgdownload.h
 *
 *  Created on: 2022. 3. 13.
 *      Author: thpark
 */

#ifndef IMG_DOWN_LOAD_IMGDOWNLOAD_H_
#define IMG_DOWN_LOAD_IMGDOWNLOAD_H_
#include "extern.h"
#include "stm32f1xx_hal.h"
////////////test+++++
#define STX     0xFA
#define ETX     0xFB

typedef struct
{
  uint32_t address;
  uint8_t stx;
  uint8_t etx;
  uint8_t x1;
  uint8_t x2;
}TX_BUF;

typedef struct
{
	uint8_t stx;
	uint8_t crc;
	uint8_t etx;
	uint8_t dummy;
	uint32_t address;
	uint8_t data[128];
}RX_BUF;



/////////////test----


class img_down_load
{
public:

	TX_BUF tx_buf;
	img_down_load();
	virtual ~img_down_load();
	RX_BUF get_from_pc(uint32_t address);
	void img_copy_to_flash(uint32_t eprom_add, uint32_t size);
};

#endif /* IMG_DOWN_LOAD_IMGDOWNLOAD_H_ */
