/*
 * imgdownload.cpp
 *
 *  Created on: 2022. 3. 13.
 *      Author: thpark
 */
#include "extern.h"
#include "imgdownload.h"
#include "iwdg.h"
extern UART_HandleTypeDef huart3;

img_down_load *pIMG_DOWN_LOAD;

img_down_load::img_down_load()
{
	// TODO Auto-generated constructor stub

}

img_down_load::~img_down_load()
{
	// TODO Auto-generated destructor stub
}


RX_BUF img_down_load::get_from_pc(uint32_t address){
	//bool ok=false;
	RX_BUF rx_buf;
	uint8_t str_buf[64];
	tx_buf.stx=STX;
	tx_buf.address=address;
	tx_buf.etx=ETX;
	uint8_t trans_buf[100]={0,};
	uint8_t recv_buf[256]={0,};
	rx_buf.etx=0;
	memcpy(trans_buf, &tx_buf, sizeof(tx_buf));
	HAL_UART_Transmit(&huart3,trans_buf, sizeof(tx_buf), 100);

	//HAL_Delay(100);
	memset(recv_buf,0,sizeof(recv_buf));
	HAL_UART_Receive(&huart3, recv_buf, 256,200);

//	sprintf((char*)str_buf,"recv_buf[%x|%x|%x|%x]\r\n",
//			recv_buf[0],recv_buf[1],recv_buf[2],recv_buf[3]);
//	pIli9488->DrawFontTTF16Kr(0, 0, TFT_BLUE, TFT_WHITE, (uint8_t*)str_buf, 0);

	memcpy(&rx_buf, recv_buf, sizeof(rx_buf));
	int sum=0;
	for(int i=0;i<128;i++){
		sum=sum+rx_buf.data[i];
	}
	if((sum&0xff)==rx_buf.crc && rx_buf.stx==0xFA) {
		rx_buf.etx=1;
	}

//	sprintf((char*)str_buf,"[%04x][%x|%x|%x|%x] [%x]\r\n", rx_buf.address,
//			rx_buf.data[0],rx_buf.data[1],rx_buf.data[2],rx_buf.data[3], rx_buf.crc);
//	pIli9488->DrawFontTTF16Kr(0, 0, TFT_BLACK, TFT_WHITE, (uint8_t*)str_buf, 0);
	return rx_buf;

}

void img_down_load::img_copy_to_flash(uint32_t eprom_add, uint32_t size){
	int sector_size;
	int remain;
	int i,ix;
	uint8_t tx[100]={0,};
	uint8_t rx[8192+16]={0,};
	uint8_t str_buf[64];

	uint8_t sector_buf[5000]={0,};
	uint32_t address;
	uint32_t ep_address;
	RX_BUF rx_data;
	 //한번에 128byte 32번 받아서 4096바이트 1섹터씩 writer
	remain=size%W25Q128FV_SUBSECTOR_SIZE;
	sector_size=(remain) ? (size/W25Q128FV_SUBSECTOR_SIZE)+1 : (size/W25Q128FV_SUBSECTOR_SIZE);

	for(i=0; i<sector_size;i++){
		__HAL_IWDG_RELOAD_COUNTER(&hiwdg);
		address=0;
		for(ix=0;ix<32;ix++){
			address=(i*W25Q128FV_SUBSECTOR_SIZE)+(ix*128);
			rx_data.etx=0;
			for(;;){
				rx_data = get_from_pc(address);
				if(rx_data.etx==1) break;
				else{
					sprintf((char*)str_buf,"ERR[%d/%d] [%04x|%x|%x|%x|%x] \r\n",i, ix, rx_data.address,
							rx_data.data[0],rx_data.data[1],rx_data.data[2],rx_data.data[3]);
					pIli9488->DrawFontTTF_Digital16(0, 30, TFT_RED, TFT_WHITE, (uint8_t*)str_buf, 0);
				}
			}
			memcpy(sector_buf+(ix*128),rx_data.data,128);
		}
		sprintf((char*)str_buf,"sector[%02d] [%02x-%02x-%02x-%02x %02x-%02x-%02x-%02x] \r\n",i,
				sector_buf[0],sector_buf[1],sector_buf[2],sector_buf[3],
				sector_buf[4],sector_buf[5],sector_buf[6],sector_buf[7]);
		pIli9488->DrawFontTTF_Digital16(0, 0, TFT_BLACK, TFT_WHITE, (uint8_t*)str_buf, 0);
		//4096byte writer
		ep_address=(i*4096)+eprom_add;
		pFlashClass->SPI_FLASH_WRITER_SECTOR(ep_address, sector_buf, 4096);
	}

}


