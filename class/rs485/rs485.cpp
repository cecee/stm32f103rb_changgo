/****************************************************************************
 *                               Includes
*****************************************************************************/
#include "extern.h"
#include "stdlib.h"
#include "string.h"
#include "rs485.h"
#include <stdio.h>

#define xDBG_RS485

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;

SENSOR_UPDATA o3sensor1;
SENSOR_UPDATA o3sensor2;
SENSOR_UPDATA o3sensor3;

RS485 *pRS485;
//QUEUE qUart1;
uint8_t qUart1_package=0;
uint8_t qUart1_buffer[512];
uint8_t qUart1_cnt=0;
uint8_t qUart1_sz=0xff;

/*****************************************************************************
*                           Static Variables
******************************************************************************/

uint8_t rx_buffer[256];
uint8_t rx_buffer_idx=0;
uint8_t rxSz=0;
uint8_t package_flag=0;
uint8_t UART_FLAG = 0;

extern uint8_t uart1_rx_rs485[256];
extern uint8_t uart1_rx_cnt;

RS485::RS485() {
	HAL_GPIO_WritePin(TXEN1_GPIO_Port, TXEN1_Pin, GPIO_PIN_RESET);

}
RS485::~RS485() {}

uint8_t calcu_sum(uint8_t *data, uint8_t len)
{
    uint8_t i, sum=0;
    for(i=0;i<len;i++)sum=sum+data[i];
    return sum;
}


void RS485::rs485_req(bdID id){
  REQUEST req;
  uint8_t res_len;
  uint8_t sum=0;
  uint8_t len=sizeof(REQUEST);
  uint8_t buf[64]={0,};
  uint16_t reqID=(bdID)id;//&0xFF00;
  uint8_t resLen,resSum;
  uint8_t index=reqID-BD_ZE27_AND_DHT20;//485몇번째것인지
  req.stx=0xFA;
  req.id=reqID;
  req.size=len;
  req.data=0xAA;
  req.dumy=0;
  req.sum=0;
  req.etx='@';
  
#ifdef DBG_RS485
  printf("(11111)rs485_req[%x] index[%x] \r\n",id,  index);
#endif
  Retry_cnt[index]++;
  if(Retry_cnt[index]>SENSOR_RETRY){
	Retry_cnt[index]=100;
	switch(index){
	    case 0:
	    	rs485_ze27_and_dht22.exist=0;
	    	rs485_ze27_and_dht22.sub_sensor0=0;
	    	break;
	    case 1:
	    	rs485_sht20.exist=0;
	    	rs485_sht20.sub_sensor1=0;
	    	rs485_sht20.sub_sensor2=0;
	    	break;
	    case 3:
	    	rs485_wind_speed.exist=0;
	    	rs485_wind_speed.sub_sensor0=0;
	    	rs485_wind_speed.sub_sensor1=0;
	    	rs485_wind_speed.sub_sensor2=0;
	    	break;
	}
  	//ozone_sensor_exist=0;
  	//dth22_sensor_exist=0;
  }
  memcpy(buf, &req, len); 
  sum=calcu_sum(buf, len-1);
  buf[len-2]=sum;
  uart1_rx_cnt=0;
 // memset(uart1_rx_data,0,sizeof(uart1_rx_data));

  HAL_GPIO_WritePin(TXEN1_GPIO_Port, TXEN1_Pin, GPIO_PIN_SET);
  HAL_UART_Transmit(&huart1, buf, len, 100); 
  HAL_GPIO_WritePin(TXEN1_GPIO_Port, TXEN1_Pin, GPIO_PIN_RESET);
  HAL_Delay(100);

#if 0
  memset(buf,0,sizeof(buf));
  //res_len= HAL_UART_Receive(&huart1, buf, 64, 200);
  
#ifdef DBG_RS485
       printf("(2)rx_resp len[%d] buf->[%x][%x][%x][%x] \r\n", res_len, buf[0], buf[1], buf[2], buf[3]);
#endif
  //0xFB reponse stx
  if(buf[0]==0xFB){
    resLen=buf[1];
    if(resLen!=0x0d) return;
    resSum=buf[resLen-2];
    sum=calcu_sum(buf, resLen-2);
    if(sum!=resSum) return;
	memcpy(&RS485_resp, buf, resLen);
	id=(bdID)SWAPBYTE_US(RS485_resp.data.bd_id);
#ifdef DBG_RS485
  // printf("(2)rx_resp id[%x] : [%x][%x][%x][%x] \r\n",id,  buf[0], buf[1], buf[2], buf[3]);
#endif
  switch(id){
    case BD_ZE27_AND_DHT20:
        Retry_cnt[0]=0;
        rs485_ze27_and_dht22.exist=1;
        rs485_ze27_and_dht22.sub_sensor0=RS485_resp.data.o3_ppb;
        rs485_ze27_and_dht22.sub_sensor1=(int)RS485_resp.data.temperature;
        rs485_ze27_and_dht22.sub_sensor2=(int)RS485_resp.data.humidity;
#ifdef DBG_RS485
        printf("\r\n BD_ZE27_AND_DHT20  o3[%d] humidity[%d] temperature[%d]\r\n",
        		rs485_ze27_and_dht22.sub_sensor0,
				rs485_ze27_and_dht22.sub_sensor2,
				rs485_ze27_and_dht22.sub_sensor1);
#endif
        break;
    case BD_SHT20:
        Retry_cnt[1]=0;
        rs485_sht20.sub_sensor0=0;
        rs485_sht20.exist=1;
        rs485_sht20.sub_sensor1=(int)RS485_resp.data.temperature;//ppp 측정값편차(3도)
        rs485_sht20.sub_sensor2=(int)RS485_resp.data.humidity;

#ifdef DBG_RS485
        printf("\r\n BD_SHT20  o3[%d] humidity[%d] temperature[%d]\r\n",
        		rs485_sht20.sub_sensor0,
				rs485_sht20.sub_sensor2,
				rs485_sht20.sub_sensor1);
#endif
        break;
    case BD_WIND:
        Retry_cnt[3]=0;
        rs485_wind_speed.sub_sensor0=(int)RS485_resp.data.o3_ppb;//wind
        rs485_wind_speed.exist=1;
        rs485_wind_speed.sub_sensor1=0;
        rs485_wind_speed.sub_sensor2=0;
#ifdef DBG_RS485
        printf("\r\n BD_WIND  wind_speed[%d] temp[%d] vout[%d]\r\n", rs485_wind_speed.sub_sensor0,	rs485_wind_speed.sub_sensor1,	rs485_wind_speed.sub_sensor2);
#endif
    	break;
    case BD_DTH20:
    case BD_ZE27:
        break;   
    }
  }
  HAL_Delay(50);
#endif
}

void RS485::rs485_res(){
	  uint8_t buf[64]={0,};
	  uint8_t resLen,resSum;
	  uint8_t sum=0;
	  uint16_t id;
	  memcpy(buf,uart1_rx_rs485,64);
    resLen=buf[1];
    if(resLen!=0x0d) return;
    resSum=buf[resLen-2];
    sum=calcu_sum(buf, resLen-2);
    if(sum!=resSum) return;
	memcpy(&RS485_resp, buf, resLen);
	id=(bdID)SWAPBYTE_US(RS485_resp.data.bd_id);
#ifdef DBG_RS485
   printf("(2)rx_resp id[%x] : [%x][%x][%x][%x] \r\n",id,  buf[0], buf[1], buf[2], buf[3]);
#endif
   switch(id){
     case BD_ZE27_AND_DHT20:
         Retry_cnt[0]=0;
         rs485_ze27_and_dht22.exist=1;
         rs485_ze27_and_dht22.sub_sensor0=RS485_resp.data.o3_ppb;
         rs485_ze27_and_dht22.sub_sensor1=(int)RS485_resp.data.temperature;
         rs485_ze27_and_dht22.sub_sensor2=(int)RS485_resp.data.humidity;
 #ifdef DBG_RS485
         printf("\r\n BD_ZE27_AND_DHT20  o3[%d] humidity[%d] temperature[%d]\r\n",rs485_ze27_and_dht22.sub_sensor0,	rs485_ze27_and_dht22.sub_sensor2,rs485_ze27_and_dht22.sub_sensor1);
 #endif
         break;
     case BD_SHT20:
         Retry_cnt[1]=0;
         rs485_sht20.sub_sensor0=0;
         rs485_sht20.exist=1;
         rs485_sht20.sub_sensor1=(int)RS485_resp.data.temperature;//ppp 측정값편차(3도)
         rs485_sht20.sub_sensor2=(int)RS485_resp.data.humidity;

 #ifdef DBG_RS485
         printf("\r\n BD_SHT20  o3[%d] humidity[%d] temperature[%d]\r\n", rs485_sht20.sub_sensor0, rs485_sht20.sub_sensor2,	rs485_sht20.sub_sensor1);
 #endif
         break;
     case BD_WIND:
         Retry_cnt[3]=0;
         rs485_wind_speed.exist=1;
         rs485_wind_speed.sub_sensor0=(int)RS485_resp.data.o3_ppb;//wind
         rs485_wind_speed.sub_sensor1=(int)RS485_resp.data.temperature;;
         rs485_wind_speed.sub_sensor2=(int)RS485_resp.data.humidity;
 #ifdef DBG_RS485
         printf("\r\n BD_WIND  wind_speed[%d] temp[%d] vout[%d]\r\n", rs485_wind_speed.sub_sensor0,	rs485_wind_speed.sub_sensor1,	rs485_wind_speed.sub_sensor2);
 #endif
     	break;
     case BD_DTH20:
     case BD_ZE27:
         break;
     }
}


