#ifndef RS485_H
#define RS485_H
#include "stm32f1xx_hal.h"

//#define MAX_SIZE 512 // max size of queue
#define ORANGE_BOX_ID 0xAAA0
#define BLACK_BOX_ID   0xAAA0
#define RS485_OZONE_BOX_ID 0xB000
#define RS485_I2C_TEMP_BOX_ID 0xB001
#define SENSOR_BOX_ID2 0xB002

#define SENSOR_RETRY 10

enum e485_BOX
{
	RS485_OZONE_BOX=0,
	RS485_I2C_TEMP_BOX,
};

//BOARD ID
typedef enum{
  BD_ZE27_AND_DHT20  = 0xB000,
  BD_SHT20           = 0xB001,
  BD_DTH20           = 0xB002,
  BD_WIND            = 0xB003,
  BD_ZE27            = 0xB004
}bdID;


typedef struct _REQUEST
{
  uint8_t stx;
  uint8_t size;
  uint16_t id;
  uint8_t data;
  uint8_t dumy;
  uint8_t sum;
  uint8_t etx;
}REQUEST;

typedef struct 
{
  uint16_t bd_id;
  uint16_t o3_ppb;
  uint16_t temperature;  
  uint16_t humidity;
  uint8_t error;
}RS485_SENSOR;

typedef struct
{
  uint16_t bd_id;
  uint16_t o3_ppb;
  uint16_t temperature;
  uint16_t humidity;
  uint8_t error;
}SENSOR_DATA;

typedef struct
{
  uint8_t stx; // 1
  uint8_t size;	//1
  SENSOR_DATA data;
  uint8_t sum; //1
  uint8_t etx; //1
}SENSOR_BOX_RS485_RESPONSE;


typedef struct _SENSOR_UPDATA   
{
  uint8_t stx; // 1
  uint8_t size;	//1
  RS485_SENSOR sensor;
  uint8_t sum; //1
  uint8_t etx; //1
}SENSOR_UPDATA;  

//typedef struct _queue
//{
//  int queue[MAX_SIZE+1];
//  int front;
//  int rear;
//}QUEUE;



class RS485
{
  private:
  public:
  SENSOR_BOX rs485_sht20;
  SENSOR_BOX rs485_ze27_and_dht22;
  SENSOR_BOX rs485_wind_speed;
  SENSOR_BOX_RS485_RESPONSE RS485_resp;
  //SENSOR_BOX_RS485_RESPONSE ze27_and_dht22_resp1;
  //SENSOR_BOX_RS485_RESPONSE sht20_res;
  uint8_t Retry_cnt[8]={20,20,20,20,20,20,20,20};
  uint8_t Bd_Exist[8]={0,0,0,0,0,0,0,0};
 // uint8_t ozone_sensor_exist=0;
 // uint8_t dth22_sensor_exist=0;

  RS485();
  virtual ~RS485();
  //void init_qUart1(void);
 // int put_qUart1(int k);
 // int get_qUart1(void);
 // void QueueProcess(void);
  //void led_control(uint8_t* data);
  void rs485_req(bdID id);
  void rs485_res();
};

#endif 
