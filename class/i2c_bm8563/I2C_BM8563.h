/*
 * I2C_BM8563.h
 *
 *  Created on: 2022. 4. 20.
 *      Author: thpark
 */

#ifndef I2C_BM8563_I2C_BM8563_H_
#define I2C_BM8563_I2C_BM8563_H_
#include "extern.h"
#define I2C_BM8563_DEFAULT_ADDRESS 0x51





typedef struct
{
  int8_t weekDay;
  int8_t month;
  int8_t date;
  int16_t year;
} I2C_BM8563_DateTypeDef;

typedef enum{
  I2C_BM8563_ADR_W                = 0xA2,   // RTC I2C address + write bit
  I2C_BM8563_ADR_R                = 0xA3    // RTC I2C address + read bit
}bmI2cHeader;


class I2C_BM8563
{
public:
	I2C_BM8563();
	virtual ~I2C_BM8563();

	void begin(void);
	bool getVoltLow();
	TYPEDEF_TIME_DATA  RTC_GetRTC(void);
	void Set_RTC(struct tm timeinfo);
	RTC_TimeTypeDef getTime();
	RTC_DateTypeDef getDate();
	void setTime(RTC_TimeTypeDef *sTime);
	void setDate(RTC_DateTypeDef *sDate);
	int SetAlarmIRQ(int afterSeconds);
	int SetAlarmIRQ(const I2C_BM8563_TimeTypeDef &I2C_BM8563_TimeStruct);
	int SetAlarmIRQ(const I2C_BM8563_DateTypeDef &I2C_BM8563_DateStruct, const I2C_BM8563_TimeTypeDef &I2C_BM8563_TimeStruct);
	void clearIRQ();
	void disableIRQ();
	void WriteReg(uint8_t reg, uint8_t data);
	uint8_t ReadReg(uint8_t reg);

private:
	uint32_t RTC_DateToBinary(struct tm *datetime);
	void RELEASE_MASTER_SDA(uint8_t input);
	void SET_MASTER_SDA(uint8_t SDA_status);
	void SET_MASTER_SCL(uint8_t SCL_status);
	uint8_t GET_MASTER_SDA(void);
	void I2c_Init();
	void DelayMicroSeconds (uint32_t nbrOfUs);
	void I2c_StartCondition();
	void I2c_StopCondition();
	uint8_t I2c_WriteByte (uint8_t txByte);
	uint8_t I2c_ReadByte (etI2cAck ack);
	uint8_t bcd2ToByte(uint8_t value);
	uint8_t byteToBcd2(uint8_t value);
};

#endif /* I2C_BM8563_I2C_BM8563_H_ */

