/*
 * I2C_BM8563.cpp
 *
 *  Created on: 2022. 4. 20.
 *      Author: thpark
 */
#include "extern.h"
#include "I2C_BM8563.h"


#define  xDBG_I2C_RTC_API

I2C_BM8563 *pHYM8563;
I2C_BM8563::~I2C_BM8563()
{
	// TODO Auto-generated destructor stub
}



I2C_BM8563::I2C_BM8563() {
	I2c_Init();
	begin();
}


void I2C_BM8563::I2c_Init ()
//==============================================================================
{
	SET_MASTER_SDA(LOW);//SDA=LOW;                // Set port as output for configuration
	SET_MASTER_SCL(LOW);//SCL=LOW;                // Set port as output for configuration
	SET_MASTER_SDA(HIGH);//SDA=HIGH;               // I2C-bus idle mode SDA released (input)
	SET_MASTER_SCL(HIGH);//SCL=HIGH;               // I2C-bus idle mode SCL released (input)
}


void  I2C_BM8563::SET_MASTER_SDA(uint8_t SDA_status){
   RELEASE_MASTER_SDA(0);//out put mode
  if(SDA_status==1)  HAL_GPIO_WritePin(RTC_SDA_GPIO_Port, RTC_SDA_Pin, GPIO_PIN_SET);
  else HAL_GPIO_WritePin(RTC_SDA_GPIO_Port, RTC_SDA_Pin, GPIO_PIN_RESET);
  //RELEASE_MASTER_SDA(1);//input mode
}

void  I2C_BM8563::SET_MASTER_SCL(uint8_t SCL_status){
  if(SCL_status==1)  HAL_GPIO_WritePin(RTC_SCL_GPIO_Port, RTC_SCL_Pin, GPIO_PIN_SET);
  else HAL_GPIO_WritePin(RTC_SCL_GPIO_Port, RTC_SCL_Pin, GPIO_PIN_RESET);
}

void I2C_BM8563::RELEASE_MASTER_SDA(uint8_t input){
	if(input){
		//RTC_SDA_GPIO_Port->CRL&=0xFFF0FFFF;//0x4444 4444
		//RTC_SDA_GPIO_Port->CRL|=0x00040000;//4->floating 8->pullup (SHT_SDA_Pin);//0x4444 4444
		RTC_SDA_GPIO_Port->CRH&=0xF0FFFFFF;//0x4444 4444
		RTC_SDA_GPIO_Port->CRH|=0x04000000;//4->floating 8->pullup (SHT_SDA_Pin);//0x4444 4444
	}
	else{
		RTC_SDA_GPIO_Port->CRH&=0xF0FFFFFF;//0x4444 4444
		RTC_SDA_GPIO_Port->CRH|=0x03000000;//0011b maxspeed 50MHZ 0x4444 4444
	}
}


uint8_t I2C_BM8563::GET_MASTER_SDA(void){
	uint8_t ret=0;
	uint16_t tt;
#if 0
	//uint32_t tt;
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = RTC_SDA_Pin;
//	//GPIO_InitStruct.Pull= GPIO_PULLUP;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	HAL_GPIO_Init(RTC_SDA_GPIO_Port, &GPIO_InitStruct);
//	//DelayMicroSeconds(3);
	ret=HAL_GPIO_ReadPin(RTC_SDA_GPIO_Port,RTC_SDA_Pin);
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
//	//GPIO_InitStruct.Pin = SHT_SDA_Pin;
//	GPIO_InitStruct.Pull= GPIO_PULLUP;
	HAL_GPIO_Init(RTC_SDA_GPIO_Port, &GPIO_InitStruct);
	//tt=SHT_SDA_GPIO_Port->CRL;
	//printf("SHT_SDA_GPIO_Port[%08x]\r\n", tt);
#else
	tt=RTC_SDA_GPIO_Port->IDR & 0x4000;
	ret=(tt)? 1:0;
#endif
	return ret;
}
void I2C_BM8563::DelayMicroSeconds (uint32_t nbrOfUs)
//==============================================================================
{
  for(uint32_t i=0; i<nbrOfUs; i++)
  {
	//for(int ix=0;ix<20;ix++)
	  __asm("nop");  //nop's may be added for timing adjustment
	//__asm("nop");
	//__asm("nop");
	//__asm("nop");
  }
}




//==============================================================================
void I2C_BM8563::I2c_StartCondition()
//==============================================================================
{
	SET_MASTER_SDA(HIGH);//SDA=HIGH;
	SET_MASTER_SCL(HIGH);//SCL=HIGH;
	SET_MASTER_SDA(LOW);//SDA=LOW;
	DelayMicroSeconds(10);  // hold time start condition (t_HD;STA)
	SET_MASTER_SCL(LOW);//SCL=LOW;
	DelayMicroSeconds(10);
}

//==============================================================================
void I2C_BM8563::I2c_StopCondition ()
//==============================================================================
{
	SET_MASTER_SDA(LOW);//SDA=LOW;
	SET_MASTER_SCL(LOW);//SCL=LOW;
	SET_MASTER_SCL(HIGH);//SCL=HIGH;
	DelayMicroSeconds(10);  // set-up time stop condition (t_SU;STO)
	SET_MASTER_SDA(HIGH);//SDA=HIGH;
	DelayMicroSeconds(10);
}

//==============================================================================
uint8_t I2C_BM8563::I2c_WriteByte (uint8_t txByte)
//==============================================================================
{
  uint8_t mask,error=0;
  for (mask=0x80; mask>0; mask>>=1)   //shift bit for masking (8 times)
  {
	if ((mask & txByte) == 0)SET_MASTER_SDA(LOW);// SDA=LOW;//masking txByte, write bit to SDA-Line
    else SET_MASTER_SDA(HIGH);//SDA=HIGH;
    DelayMicroSeconds(1);             //data set-up time (t_SU;DAT)
    SET_MASTER_SCL(HIGH);//SCL=HIGH;                         //generate clock pulse on SCL
    DelayMicroSeconds(5);             //SCL high time (t_HIGH)
    SET_MASTER_SCL(LOW);//SCL=LOW;
    DelayMicroSeconds(1);             //data hold time(t_HD;DAT)
  }
#if 1
  //DelayMicroSeconds(5);
  RELEASE_MASTER_SDA(1);
  DelayMicroSeconds(1);
  SET_MASTER_SCL(HIGH);//SCL=HIGH;   //clk #9 for ack
  if (GET_MASTER_SDA() != LOW) error=ACK_ERROR;
  DelayMicroSeconds(1);
  SET_MASTER_SCL(LOW);//SCL=LOW;
  DelayMicroSeconds(20);              //wait time to see byte package on scope
#endif
  return error;                       //return error code

}

//==============================================================================
uint8_t I2C_BM8563::I2c_ReadByte (etI2cAck ack)
//==============================================================================
{
	uint8_t mask,rxByte=0;
 // SDA=HIGH;                           //release SDA-line
  //SET_MASTER_SDA(HIGH);
  RELEASE_MASTER_SDA(1);
  for (mask=0x80; mask>0; mask>>=1)   //shift bit for masking (8 times)
  {
	//RELEASE_MASTER_SDA(1);
	SET_MASTER_SCL(HIGH);//SCL=HIGH;  //start clock on SCL-line

    DelayMicroSeconds(1);             //data set-up time (t_SU;DAT)
    DelayMicroSeconds(3);             //SCL high time (t_HIGH)
    if (GET_MASTER_SDA() != LOW) rxByte=(rxByte | mask); //read bit
    //if (SDA_CONF==1) rxByte=(rxByte | mask); //read bit
    SET_MASTER_SCL(LOW);//SCL=LOW;
    DelayMicroSeconds(1);             //data hold time(t_HD;DAT)
  }
 // RELEASE_MASTER_SDA(0);

  SET_MASTER_SDA(ack);//SDA=ack;      //send acknowledge if necessary
  DelayMicroSeconds(1);               //data set-up time (t_SU;DAT)
  SET_MASTER_SCL(HIGH);//SCL=HIGH;    //clk #9 for ack
  DelayMicroSeconds(5);               //SCL high time (t_HIGH)
  SET_MASTER_SCL(LOW);//SCL=LOW;
 // SET_MASTER_SDA(HIGH);//SDA=HIGH;    //release SDA-line
  RELEASE_MASTER_SDA(1);
  DelayMicroSeconds(20);              //wait time to see byte package on scope
  return rxByte;                      //return error code
}

void I2C_BM8563::begin(void) {
  WriteReg(0,0);
  WriteReg(1,0);
}

bool I2C_BM8563::getVoltLow() {
  uint8_t data = ReadReg(0x02);
  return data & 0x80; // RTCC_VLSEC_MASK
}



uint32_t I2C_BM8563::RTC_DateToBinary(struct tm *datetime) {
	uint32_t iday;
	uint32_t val;
	uint32_t DaysToMonth[13] = { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365 };

	iday = 365 * (datetime->tm_year - 70) + DaysToMonth[datetime->tm_mon - 1]
			+ (datetime->tm_mday - 1);
	iday = iday + (datetime->tm_year - 69) / 4;
	if ((datetime->tm_mon > 2) && ((datetime->tm_year % 4) == 0)) {
		iday++;
	}
	//printf("DAYS[%d]\r\n", iday);
	val = datetime->tm_sec + 60 * datetime->tm_min
			+ 3600 * (datetime->tm_hour + 24 * iday);
	return val;
}



TYPEDEF_TIME_DATA  I2C_BM8563::RTC_GetRTC(void) {
	TYPEDEF_TIME_DATA time_data;
	struct tm timeinfo;
	RTC_TimeTypeDef myTime;
	RTC_DateTypeDef myDate;
	myTime=getTime();
	myDate=getDate();
	//HAL_RTC_GetTime(&hrtc, &myTime, FORMAT_BIN);
	//HAL_RTC_GetDate(&hrtc, &myDate, FORMAT_BIN);

	timeinfo.tm_wday = myDate.WeekDay;
	timeinfo.tm_mon = myDate.Month;
	timeinfo.tm_mday = myDate.Date;
//	 if (myDate.Year & 0x80) {
//		 timeinfo.tm_year = 1900 + myDate.Year;
//	  } else {
//		  timeinfo.tm_year = 2000 + myDate.Year;
//	  }
	 timeinfo.tm_year = 1900 + myDate.Year;
#ifdef DBG_I2C_RTC_API
	 printf("--->getDate--->[%04d-%02d-%02d] myDate.Year[%d]\r\n",timeinfo.tm_year,timeinfo.tm_mon,timeinfo.tm_mday, myDate.Year);
#endif
	 //timeinfo.tm_year = myDate.Year;// + 2000;//STM32 RTC는 녕도는 한바이트처리하고 다시 시작하면 날짜정보는 없어져 버린다.
	timeinfo.tm_hour = myTime.Hours;
	timeinfo.tm_min = myTime.Minutes;
	timeinfo.tm_sec = myTime.Seconds;
	//rtc_current_ltime = mktime(&timeinfo); //unit of second
	time_data.ltime = RTC_DateToBinary(&timeinfo); //unit of second
	//time_data.ltime = mktime(&timeinfo); //unit of second
	time_data.timeinfo=timeinfo;
	//time_data.lsecOneDay =(myTime.Hours*3600)+(myTime.Minutes*60)+myTime.Seconds;
	return time_data;

}

void I2C_BM8563::Set_RTC(struct tm timeinfo) {

	RTC_TimeTypeDef sTime;
	RTC_DateTypeDef sDate;

	sDate.Year  = timeinfo.tm_year;
	sDate.Month = timeinfo.tm_mon+1;
	sDate.Date  = timeinfo.tm_mday;

	sTime.Hours = timeinfo.tm_hour;
	sTime.Minutes = timeinfo.tm_min;
	sTime.Seconds = timeinfo.tm_sec;

#ifdef DBG_I2C_RTC_API
	printf("Set_RTC : %d-%d-%d\r\n", sDate.Year, sDate.Month, sDate.Date );
#endif
	//sDate.WeekDay = RTC_WEEKDAY_MONDAY;
	setTime(&sTime);
	setDate(&sDate);
	//0x32F0x32F2
	//HAL_RTCEx_BKUPWrite(&hrtc, RTC_BKP_DR1, 0x32F2);

}

uint8_t I2C_BM8563::bcd2ToByte(uint8_t value) {
  uint8_t tmp = 0;
  tmp = ((uint8_t)(value & (uint8_t)0xF0) >> (uint8_t)0x4) * 10;
  return (tmp + (value & (uint8_t)0x0F));
}

uint8_t I2C_BM8563::byteToBcd2(uint8_t value) {
  uint8_t bcdhigh = 0;

  while (value >= 10) {
    bcdhigh++;
    value -= 10;
  }

  return ((uint8_t)(bcdhigh << 4) | value);
}
//void I2C_BM8563::getTime(I2C_BM8563_TimeTypeDef* I2C_BM8563_TimeStruct) {
RTC_TimeTypeDef I2C_BM8563::getTime() {
  RTC_TimeTypeDef timeinfo;
  uint8_t buf[3] = {0};
  buf[0] = ReadReg(0x02);
  buf[1] = ReadReg(0x03);
  buf[2] = ReadReg(0x04);
  timeinfo.Seconds = bcd2ToByte(buf[0] & 0x7f);
  timeinfo.Minutes = bcd2ToByte(buf[1] & 0x7f);
  timeinfo.Hours   = bcd2ToByte(buf[2] & 0x3f);
  return timeinfo;
}

RTC_DateTypeDef I2C_BM8563::getDate() {
  RTC_DateTypeDef dateinfo;
  uint8_t buf[4] = {0};
  buf[0] = ReadReg(0x05);//date
  buf[1] = ReadReg(0x06);//weeks
  buf[2] = ReadReg(0x07);//month
  buf[3] = ReadReg(0x08);//year
  dateinfo.Date     = bcd2ToByte(buf[0] & 0x3f);
  dateinfo.WeekDay  = bcd2ToByte(buf[1] & 0x07);
  dateinfo.Month    = bcd2ToByte(buf[2] & 0x1f);
  dateinfo.Year 	= bcd2ToByte(buf[3] & 0xff);
  return dateinfo;
}

void I2C_BM8563::setTime(RTC_TimeTypeDef *sTime) {
	WriteReg(0x02,byteToBcd2(sTime->Seconds));
	WriteReg(0x03,byteToBcd2(sTime->Minutes));
	WriteReg(0x04,byteToBcd2(sTime->Hours));
}

void I2C_BM8563::setDate(RTC_DateTypeDef *sDate) {
	WriteReg(0x05,byteToBcd2(sDate->Date));
	WriteReg(0x06,byteToBcd2(sDate->WeekDay));
	WriteReg(0x07,byteToBcd2(sDate->Month));
	WriteReg(0x08,byteToBcd2(sDate->Year));
}

void I2C_BM8563::WriteReg(uint8_t reg, uint8_t data) {
	uint8_t error=0;   //variable for error code
	I2c_StartCondition();
	error |= I2c_WriteByte(I2C_BM8563_ADR_W);
	error |= I2c_WriteByte(reg);
	error |= I2c_WriteByte(data);
	I2c_StopCondition();
}


uint8_t I2C_BM8563::ReadReg(uint8_t reg) {
  uint8_t error=0;    //variable for error code
  uint8_t rdata=0;
  I2c_StartCondition();
  error |= I2c_WriteByte(I2C_BM8563_ADR_W);
  error |= I2c_WriteByte(reg);
  I2c_StartCondition();
  error |= I2c_WriteByte (I2C_BM8563_ADR_R);
  rdata= I2c_ReadByte(NO_ACK);
  I2c_StopCondition();
  //printf("--->ReadReg error--->[%d][%d]\r\n",error, rdata);
  return rdata;
}
