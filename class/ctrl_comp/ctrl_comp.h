/*
 * CTRLTEMP.h
 *
 *  Created on: Mar 14, 2022
 *      Author: thpark
 */

#ifndef CTRL_TEMP_CTRLTEMP_H_
#define CTRL_TEMP_CTRLTEMP_H_

#include "extern.h"

class COMP_CTRL
{
public:

	COMP_CTRL();
	virtual ~COMP_CTRL();
	void control_comp(int measure);
	//void control_alarm(int measure, int seljung);
};

#endif /* CTRL_TEMP_CTRLTEMP_H_ */
