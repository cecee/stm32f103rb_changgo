/*
 * CTRLTEMP.cpp
 *
 *  Created on: Mar 14, 2022
 *      Author: thpark
 */

#include "extern.h"
#include "ctrl_comp.h"

#define xDBG_COMP
#define DBG_TEMP
#define DBG_TEMP_ALARM

COMP_CTRL *pCOMP;

COMP_CTRL::COMP_CTRL()
{
	// TODO Auto-generated constructor stub
	//memset(&comp_info, 0, sizeof(comp_info));
}

COMP_CTRL::~COMP_CTRL()
{
	// TODO Auto-generated destructor stub
}





void COMP_CTRL::control_comp(int measure)
{

	uint8_t comp_ctrl=0x80;//null nothing to do
	int on_ref  = pDataClass->reg.parm.comp_on_ref;//comp_info.seljung + pDataClass->reg.parm.comp_hysteresis;//comp_info.hysteresis;
	int off_ref = pDataClass->reg.parm.comp_off_ref;
//	int thermostat_target = off_ref - comp_info.super_cool_protect;
	string onoff[2]={"OFF","ON"};
	//Get_CompParameter();
	if (measure >= on_ref)
	{
		comp_ctrl=1; //지연 on
		pDataClass->reg.parm.comp_on_time++;
	}
	else if (measure <= off_ref)
	{
		comp_ctrl=0;
	}
//제상히터보고...
	if(pDataClass->reg.parm.comp_on_at_jaesang==0){
		if(pDataClass->reg.parm.jaesang_heater_on_flag)comp_ctrl=0;
	}
//측정온도가 설정온도보다 낮고 온도편차가 2도(?) 이상나면
	if(pDataClass->reg.sysFlag.b.SUPER_COOL_PROTECT){
		comp_ctrl=0;
	}
//온도센서이상
	if(pDataClass->reg.sysFlag.b.TEMPERATURE_SENSOR_ERROR){
		comp_ctrl=0;
	}
//테스터모드


#ifdef DBG_COMP
		printf("\r\n[%d] COMPRESS[%s] measure[%d] ref[%d] hysteresis[%d] on_ref[%d] off_ref[%d] \r\n",comp_ctrl,  onoff[pDataClass->reg.parm.comp_on_flag].c_str(), measure, comp_info.seljung, comp_info.hysteresis, on_ref, off_ref);
#endif
	switch(comp_ctrl){
	case 0: //off
		if(pDataClass->reg.parm.comp_on_flag==ON){
			pDataClass->reg.parm.comp_on_flag=OFF;
			pSTM32_IO->PutRelayEventVector(COMP_RY,OFF,0);
		}
		break;
	case 1: //지연 on
		if(pDataClass->reg.parm.comp_on_flag==OFF){
			pDataClass->reg.parm.comp_on_flag=ON;
			pDataClass->reg.parm.comp_on_time=0;
			pSTM32_IO->PutRelayEventVector(COMP_RY,ON,pDataClass->reg.parm.comp_on_latency);
		}
		break;
	default:
		break;

	}
	//control_alarm(measure, pDataClass->reg.parm.seljung_temp);

}
