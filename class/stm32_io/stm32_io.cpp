//#include "keypad_adc.h"
#include "extern.h"
#include "adc.h"
#include "tim.h"
//#include "sub_menu_string.h"
#define xDBG_STM32_IO
#define MAX_CTRL_STEP 3
#define SENSOR_NUM 3

#define KEY_UP 			0x01
#define KEY_DN     		0x02
#define KEY_DETAIL     	0x04
#define KEY_HOME_MENU   0x10
#define KEY_SETUP_NEXT  0x20
#define KEY_ITEM        0x40
#define LONG_PRESS_ON_CNT    30
#define LONG_PRESS_OFF_CNT   10


extern RTC_API *pRTC_API;
extern DATA_CLASS *pDataClass;

STM32_IO *pSTM32_IO;

//extern uint8_t menu_sub_cnt;
//extern uint8_t menu_item_cnt_arry[5];
extern uint8_t sub_item_cnt_arry[4][8][4];
//extern uint8_t CONTROL_VOID_MODE;

bool buttonState();

STM32_IO::STM32_IO() {
	memset(&navi_state, 0, sizeof(navi_state));
	vKEY_BUFFER.clear();
	vENCODE_BUFFER.clear();
	vRELAY_CONTROL_EVENT.clear();
}

STM32_IO::~STM32_IO() {
}




void STM32_IO::BEEP_ON(void)  {buzzer_cnt=0; pDataClass->Buzzer=1; HAL_GPIO_WritePin(pBEEP_GPIO_Port, pBEEP_Pin, GPIO_PIN_SET);}
void STM32_IO::BEEP_OFF(void) {HAL_GPIO_WritePin(pBEEP_GPIO_Port, pBEEP_Pin, GPIO_PIN_RESET);}

void STM32_IO::PutRelayEventVector(RELAY relay, uint8_t on, uint16_t count){
	RELAY_CONTROL_EVENT event;
	event.relay=relay;
	event.on=on;
	event.count=count;
	vRELAY_CONTROL_EVENT.push_back(event);
#ifdef DBG_STM32_IO
	printf("[PutRelayEventVector] relay[%d][%d][%d]\r\n", event.relay, event.on, event.count);
#endif
}
void STM32_IO::RelayEventVector_Process(){
	uint8_t i;
	RELAY_CONTROL_EVENT event;
	for(i=0 ; i<vRELAY_CONTROL_EVENT.size();i++){
		event=vRELAY_CONTROL_EVENT[i];
#ifdef DBG_STM32_IO
	printf("[Relay_Event_Vector][%d] relay[%d][%d][%d]\r\n",i, event.relay, event.on, event.count);
#endif
	SET_RELAY_CTRL_REGISTER(event.relay, event.count, event.on);

	}
	vRELAY_CONTROL_EVENT.clear();
}

uint8_t STM32_IO::Get_Key_Buffer(){
	uint8_t key=0;
	if(vKEY_BUFFER.size()){
		key=vKEY_BUFFER[0];
		vKEY_BUFFER.erase(vKEY_BUFFER.begin());
	}
	return key;
}

void STM32_IO::Clear_Key_Buffer(){
	vKEY_BUFFER.clear();
}

int16_t STM32_IO::Get_ENCODE_Buffer(){
	int16_t position=0;
	int16_t sum=0;
	for(int i=0;i<vENCODE_BUFFER.size();i++){
	//if(vENCODE_BUFFER.size()){
		position=vENCODE_BUFFER[0];
		sum=sum+position;
		vENCODE_BUFFER.erase(vENCODE_BUFFER.begin());
	}
	return sum;
}

void STM32_IO::kpad_init(void) {

}


void STM32_IO::Single_KeyProcess(void) {
	MENU_DRAW_EVENT draw_event;
	int item_cnt=0;
#ifdef DBG_STM32_IO
	printf("[BUTTON] Single_KeyProcess navi_state.top_mode[%d]\r\n", navi_state.top_mode);
#endif
	draw_event=KEY_CANCEL_EVENT;

	if (navi_state.top_mode == 0) { //top_mode=0
	 draw_event=KEY_TOP_MENU_CHANGE_EVENT;
	 navi_state.top_title++;
	 navi_state.top_title %=4;//0:temp 1:o3 2:jaesang 3:history(24)
	 navi_state.depth = 0;

	}
	else{
		//sub menu에 History는 item이 없습니다
		if(navi_state.sub>5)return;

		item_cnt =UI_Info.sub_menu_cnt[navi_state.sub];
		if(navi_state.depth) {
			navi_state.item++;
			navi_state.item %= item_cnt;
			navi_state.depth = (navi_state.item==0)? 2: 1;
			draw_event= KEY_TOP_MODE_ITEM_EVENT;
			RotaryEncoder_clear();
		}

	}
	pMain_page->NAVI_ChangeMode(navi_state, draw_event, 0);
	return;
}

void STM32_IO::MENU_KeyProcess(void) {
	MENU_DRAW_EVENT draw_event;
	int item_cnt=0;
#ifdef DBG_STM32_IO
	printf("[BUTTON] Single_KeyProcess navi_state.top_mode[%d]\r\n", navi_state.top_mode);
#endif
	draw_event=KEY_CANCEL_EVENT;
	if (navi_state.top_mode == 0) { //top_mode=0
		//if(navi_state.top_title ==TOP_COMP) return;
		 draw_event=KEY_TOP_MENU_CHANGE_EVENT;
		 //navi_state.top_title= (navi_state.top_title==0)?  3: 0;
		 navi_state.top_title=TOP_COMP;
		 navi_state.depth = 0;
	}
	else{
		draw_event=KEY_HOME_MENU_EVENT;
		memset(&navi_state, 0, sizeof(navi_state));
		navi_state.top_mode = 0;
		navi_state.depth=0;
		pDataClass->SYSTEM_ChangeToNormal();
	}
	pMain_page->NAVI_ChangeMode(navi_state, draw_event, 0);
	return;
}

void STM32_IO::LEFT_RIGHT_KeyProcess(uint8_t left, uint8_t plus) {
	MENU_DRAW_EVENT draw_event;
	int page_cnt=0;
	int lplus;
	int top_title_index;
	lplus=(left)? plus*(-1): plus;
#ifdef DBG_STM32_IO
	printf("[BUTTON] Single_KeyProcess navi_state.top_mode[%d]\r\n", navi_state.top_mode);
#endif
	draw_event=KEY_CANCEL_EVENT;
	if (navi_state.top_mode == 0) { //top_mode=0
		if(navi_state.top_title ==TOP_GRAPH1 && left==0) return;
		else if(navi_state.top_title ==TOP_OZONE && left) return;
		draw_event=KEY_TOP_MENU_CHANGE_EVENT;
		navi_state.top_title=	(left) ? TOP_OZONE: TOP_GRAPH1;
		navi_state.depth = 0;
		pMain_page->NAVI_ChangeMode(navi_state, draw_event, 0);
	}
	else if(navi_state.top_mode == 1){
		//top_mode=1
				if(navi_state.item==0){
					page_cnt=navi_state.sub;
					draw_event=KEY_TOP_MODE_SETUP_NEXT_EVENT;
					page_cnt+= lplus;
					if(page_cnt<0)page_cnt = UI_Info.total_menu_cnt-1;
					page_cnt %= UI_Info.total_menu_cnt;
					navi_state.item = 0;
					navi_state.sub=page_cnt;
					pDataClass->SYSTEM_ChangeToNormal();
					if(navi_state.sub==4)//test mode
					{
						//pDataClass->system_state=SYSTEM_STATE_TEST;
						pDataClass->reg.sysFlag.b.CHECK_RELAY_ON=1;
						for(int i=0;i<8;i++) pMain_page->UI_Data.data_arry[navi_state.sub].idata[i+1]=0;
					}
#ifdef DBG_STM32_IO
//	printf("[BUTTON-1] Double_KeyProcess top_mode[%d] sub[%d] \r\n", navi_state.top_mode, navi_state.sub);
#endif
				}
				else{
					 draw_event= KEY_ITEM_VALUE_UPDN_EVENT;
					 if(pDataClass->reg.sysFlag.b.CHECK_RELAY_ON ){
#ifdef DBG_TEST_MODE
					 printf("TEST_MODE Process~[KEY_UP or DN]===navi_state.depth[%d] item[%d]\r\n", navi_state.depth, navi_state.item);
#endif
					 TEST_ACTION(navi_state, draw_event, lplus);
					}
				}
	#ifdef DBG_STM32_IO
			//	printf("[ENCODER POS](%d) ===navi_state.depth[%d] item[%d]\r\n", encoder_position, navi_state.depth, navi_state.item);
	#endif
				pMain_page->NAVI_ChangeMode(navi_state, draw_event, lplus);

	}

	return;
}


void STM32_IO::UPDN_KeyProcess(uint8_t up, uint8_t plus) {
	int total_item_cnt=0, item_cnt;
	int plus_value= (up)? plus:plus*(-1);
	MENU_DRAW_EVENT draw_event;
	draw_event=KEY_CANCEL_EVENT;
	if (navi_state.top_mode == 0) { //top_mode=0
		if(navi_state.top_title ==TOP_FORCE_JAESANG) return;
		switch(navi_state.top_title){
			case TOP_COMP:
				draw_event=KEY_SELJUNG_UPDN_EVENT;
				pMain_page->UI_Data.target.temp += plus_value;
				pMain_page->UI_Data.target.temp=(pMain_page->UI_Data.target.temp > MAX_CONTROL_TEMP_H)? MAX_CONTROL_TEMP_H:pMain_page->UI_Data.target.temp;
				pMain_page->UI_Data.target.temp=(pMain_page->UI_Data.target.temp < MAX_CONTROL_TEMP_L)? MAX_CONTROL_TEMP_L:pMain_page->UI_Data.target.temp;
				break;
			case TOP_OZONE:
				draw_event=KEY_SELJUNG_UPDN_EVENT;
				pMain_page->UI_Data.target.o3 += (plus_value*10);
				pMain_page->UI_Data.target.o3=(pMain_page->UI_Data.target.o3 > MAX_CONTROL_OZEON_H)? MAX_CONTROL_OZEON_H:pMain_page->UI_Data.target.o3;
				pMain_page->UI_Data.target.o3=(pMain_page->UI_Data.target.o3 < MAX_CONTROL_OZEON_L)? MAX_CONTROL_OZEON_L:pMain_page->UI_Data.target.o3;
				pMain_page->UI_Data.target.o3=(pMain_page->UI_Data.target.o3/10)*10;
				break;
			case TOP_JESANG:
				draw_event=KEY_SELJUNG_UPDN_EVENT;
				pMain_page->UI_Data.target.wind += (plus_value*10);
				pMain_page->UI_Data.target.wind=(pMain_page->UI_Data.target.wind > MAX_CONTROL_WIND_H)? MAX_CONTROL_WIND_H:pMain_page->UI_Data.target.wind;
				pMain_page->UI_Data.target.wind=(pMain_page->UI_Data.target.wind < MAX_CONTROL_WIND_L)? MAX_CONTROL_WIND_L:pMain_page->UI_Data.target.wind;
				pMain_page->UI_Data.target.wind=(pMain_page->UI_Data.target.wind/10)*10;
				break;
			case TOP_GRAPH1:
				break;
			default:break;
		}
		pMain_page->NAVI_ChangeMode(navi_state, draw_event, plus_value);
	}
	else if(navi_state.top_mode == 1){
			//sub menu에 History는 item이 없습니다
			if(navi_state.sub>5)return;
			total_item_cnt =UI_Info.sub_menu_cnt[navi_state.sub];
			item_cnt=navi_state.item;
			if(navi_state.depth) {
				if(up)item_cnt--;
				else item_cnt++;
				if(item_cnt<0)item_cnt=0;
				item_cnt %= total_item_cnt;
				printf("UPDN_KeyProcess[%d/%d] \r\n", item_cnt, total_item_cnt);
				navi_state.item=item_cnt;
				navi_state.depth = (navi_state.item==0)? 2: 1;
				draw_event= KEY_TOP_MODE_ITEM_EVENT;
				//RotaryEncoder_clear();
			}
			pMain_page->NAVI_ChangeMode(navi_state, draw_event, 0);
	}

}

void STM32_IO::JESANG_KeyProcess(void) {
	MENU_DRAW_EVENT draw_event;
	draw_event=KEY_HOME_MENU_EVENT;
	if (navi_state.top_mode == 0) { //top_mode=0
		//강재제상모드 이면 top menu로
		if(pDataClass->reg.sysFlag.b.JAESANG_FORCE_ON){
			navi_state.top_title=TOP_COMP;
			pDataClass->reg.sysFlag.b.CHECK_RELAY_ON=0;
			pDataClass->reg.sysFlag.b.JAESANG_FORCE_ON=0;
			pDataClass->reg.parm.jaesang_heater_on_flag=0;//제상 끝내고 돌아가기
			pSTM32_IO->PutRelayEventVector(HEATER_RY,OFF,0);
			navi_state.depth=0;
		}
		else{
			//draw_event=KEY_HOME_MENU_EVENT;
			navi_state.top_title=TOP_FORCE_JAESANG;
			pDataClass->reg.parm.jaesang_force_on_count=0;
			pDataClass->reg.sysFlag.b.JAESANG_FORCE_ON=1;
			navi_state.depth=0;
		}
		pMain_page->NAVI_ChangeMode(navi_state, draw_event,0);
	}
	else{
	}
	//pMain_page->NAVI_ChangeMode(navi_state, draw_event,0);
	return;
}

void STM32_IO::JESANG_MQ_KeyProcess(uint8_t on) {
	MENU_DRAW_EVENT draw_event;
	draw_event=KEY_HOME_MENU_EVENT;
	navi_state.top_mode=0;
	if(on){
		navi_state.top_title=TOP_FORCE_JAESANG;
		pDataClass->reg.parm.jaesang_force_on_count=0;
		pDataClass->reg.sysFlag.b.JAESANG_FORCE_ON=1;
		navi_state.depth=0;
	}
	else{
		navi_state.top_title=TOP_COMP;
		pDataClass->reg.sysFlag.b.CHECK_RELAY_ON=0;
		pDataClass->reg.sysFlag.b.JAESANG_FORCE_ON=0;
		pDataClass->reg.parm.jaesang_heater_on_flag=0;//제상 끝내고 돌아가기
		pSTM32_IO->PutRelayEventVector(HEATER_RY,OFF,0);
		navi_state.depth=0;
	}
	pMain_page->NAVI_ChangeMode(navi_state, draw_event,0);
	return;
}
void STM32_IO::MENUX10_KeyProcess(void) {
	MENU_DRAW_EVENT draw_event;
#ifdef DBG_STM32_IO
	printf("[BUTTON] Double_KeyProcess top_mode[%d] sub[%d] \r\n", navi_state.top_mode, navi_state.sub);
#endif
	draw_event=KEY_CANCEL_EVENT;
	if (navi_state.top_mode == 0) { //top_mode=0
		draw_event=KEY_TOP_MODE_SETUP_NEXT_EVENT;
		memset(&navi_state, 0, sizeof(navi_state));
		navi_state.top_mode=1;
		navi_state.sub=0;
		navi_state.depth=2;
		pMain_page->NAVI_ChangeMode(navi_state, draw_event,0);
	}
	return;
}




void STM32_IO::Double_KeyProcess(void) {
	MENU_DRAW_EVENT draw_event;
#ifdef DBG_STM32_IO
	printf("[BUTTON] Double_KeyProcess top_mode[%d] sub[%d] \r\n", navi_state.top_mode, navi_state.sub);
#endif
	draw_event=KEY_CANCEL_EVENT;
	if (navi_state.top_mode == 0) { //top_mode=0
		draw_event=KEY_TOP_MODE_SETUP_NEXT_EVENT;
		memset(&navi_state, 0, sizeof(navi_state));
		navi_state.top_mode=1;
		navi_state.sub=0;
		navi_state.depth=2;
		pMain_page->NAVI_ChangeMode(navi_state, draw_event,0);
	}
	else{ //설정에서 더블키
		draw_event=KEY_TOP_MODE_SETUP_NEXT_EVENT;
		navi_state.sub++;
		navi_state.sub %= UI_Info.total_menu_cnt+1;
		navi_state.item = 0;
		//pDataClass->system_state=SYSTEM_STATE_NORMAL;
		pDataClass->SYSTEM_ChangeToNormal();
		//pDataClass->reg.sysFlag.b.TEST_ON=0;
		//pDataClass->reg.sysFlag.b.JAESANG_FORCE_ON=0;
#ifdef DBG_STM32_IO
	printf("[BUTTON-1] Double_KeyProcess top_mode[%d] sub[%d] \r\n", navi_state.top_mode, navi_state.sub);
#endif
		switch(navi_state.sub){
		case 4:
			//pDataClass->system_state=SYSTEM_STATE_TEST;
			pDataClass->reg.sysFlag.b.CHECK_RELAY_ON=1;
			for(int i=0;i<8;i++) pMain_page->UI_Data.data_arry[navi_state.sub].idata[i+1]=0;
			pMain_page->NAVI_ChangeMode(navi_state, draw_event,0);
			break;
		case 6:
			//History
#ifdef DBG_STM32_IO
	printf("[BUTTON] HISTORY process \r\n");
#endif
			draw_event=KEY_TOP_MENU_CHANGE_EVENT;
			navi_state.top_title =3;
			navi_state.depth = 1;
			pMain_page->NAVI_ChangeMode(navi_state, draw_event,0);
			break;
		default:
			pMain_page->NAVI_ChangeMode(navi_state, draw_event,0);
			break;

		}
//		if(navi_state.sub==4){
//			pDataClass->CONTROL_VOID_MODE=1;
//			for(int i=0;i<8;i++) pMain_page->UI_Data.data_arry[navi_state.sub].idata[i+1]=0;
//		}
//		else pDataClass->CONTROL_ABNOMAL_MODE=0;
	}
	RotaryEncoder_clear();

	return;
}

void STM32_IO::RotaryEncoder_clear() {
//	__HAL_TIM_SET_COUNTER(&htim2,0);
}

void STM32_IO::Encoder_UpDnProcess(int16_t position) {

	MENU_DRAW_EVENT draw_event;
	draw_event=KEY_CANCEL_EVENT;

#ifdef DBG_STM32_IO
	printf("\r\nEncoder_UpDnProcess  position[%d] navi_state.top_mode[%d] \r\n",  position, navi_state.top_mode);
#endif
	if (navi_state.top_mode == 0) { //top_mode=0

		switch(navi_state.top_title){
			case TOP_COMP:
				draw_event=KEY_SELJUNG_UPDN_EVENT;
				pMain_page->UI_Data.target.temp += position;
				pMain_page->UI_Data.target.temp=(pMain_page->UI_Data.target.temp > MAX_CONTROL_TEMP_H)? MAX_CONTROL_TEMP_H:pMain_page->UI_Data.target.temp;
				pMain_page->UI_Data.target.temp=(pMain_page->UI_Data.target.temp < MAX_CONTROL_TEMP_L)? MAX_CONTROL_TEMP_L:pMain_page->UI_Data.target.temp;
				break;
			case TOP_OZONE:
				draw_event=KEY_SELJUNG_UPDN_EVENT;
				pMain_page->UI_Data.target.o3 += (position*10);
				pMain_page->UI_Data.target.o3=(pMain_page->UI_Data.target.o3 > MAX_CONTROL_OZEON_H)? MAX_CONTROL_OZEON_H:pMain_page->UI_Data.target.o3;
				pMain_page->UI_Data.target.o3=(pMain_page->UI_Data.target.o3 < MAX_CONTROL_OZEON_L)? MAX_CONTROL_OZEON_L:pMain_page->UI_Data.target.o3;
				pMain_page->UI_Data.target.o3=(pMain_page->UI_Data.target.o3/10)*10;
				break;
			case TOP_JESANG:
				draw_event=KEY_SELJUNG_UPDN_EVENT;
				pMain_page->UI_Data.target.wind += (position*10);
				pMain_page->UI_Data.target.wind=(pMain_page->UI_Data.target.wind > MAX_CONTROL_WIND_H)? MAX_CONTROL_WIND_H:pMain_page->UI_Data.target.wind;
				pMain_page->UI_Data.target.wind=(pMain_page->UI_Data.target.wind < MAX_CONTROL_WIND_L)? MAX_CONTROL_WIND_L:pMain_page->UI_Data.target.wind;
				pMain_page->UI_Data.target.wind=(pMain_page->UI_Data.target.wind/10)*10;
				break;
			case TOP_GRAPH1:
				break;
			default:break;
		}
		//pMain_page->Navi_change_mode(navi_state);
	} else {	//top_mode=1
			if(navi_state.item==0){
				draw_event=KEY_TOP_MODE_SETUP_NEXT_EVENT;
						navi_state.sub+= position;
						navi_state.sub %= UI_Info.total_menu_cnt;
						navi_state.item = 0;
						//pDataClass->system_state=SYSTEM_STATE_NORMAL;
						pDataClass->SYSTEM_ChangeToNormal();
						//pDataClass->reg.sysFlag.b.TEST_ON=0;
						//pDataClass->reg.sysFlag.b.JAESANG_FORCE_ON=0;
				#ifdef DBG_KEY
				//	printf("[BUTTON-1] Double_KeyProcess top_mode[%d] sub[%d] \r\n", navi_state.top_mode, navi_state.sub);
				#endif
						switch(navi_state.sub){
						case 4:
							//pDataClass->system_state=SYSTEM_STATE_TEST;
							pDataClass->reg.sysFlag.b.CHECK_RELAY_ON=1;
							for(int i=0;i<8;i++) pMain_page->UI_Data.data_arry[navi_state.sub].idata[i+1]=0;
							//pMain_page->NAVI_ChangeMode(navi_state, draw_event);
							break;
						default:
							//pMain_page->NAVI_ChangeMode(navi_state, draw_event);
							break;

						}


			}
			else{
				draw_event= KEY_ITEM_VALUE_UPDN_EVENT;
				 //if(pDataClass->system_state==SYSTEM_STATE_TEST ){
				 if(pDataClass->reg.sysFlag.b.CHECK_RELAY_ON ){
#ifdef DBG_TEST_MODE
				 printf("TEST_MODE Process~[KEY_UP or DN]===navi_state.depth[%d] item[%d]\r\n", navi_state.depth, navi_state.item);
#endif
				 	 TEST_ACTION(navi_state, draw_event, position);
				}

			}
#ifdef DBG_STM32_IO
		//	printf("[ENCODER POS](%d) ===navi_state.depth[%d] item[%d]\r\n", encoder_position, navi_state.depth, navi_state.item);
#endif

	}
	pMain_page->NAVI_ChangeMode(navi_state, draw_event, position);
	RotaryEncoder_clear();
	return;

}


void STM32_IO::TEST_ACTION(NAVI_BIT navi, MENU_DRAW_EVENT draw_event, int16_t position) {
	int8_t on=(int8_t)pMain_page->UI_Data.data_arry[navi.sub].idata[navi.item];
	if(navi.item==0)return;
	uint8_t relayNum=navi.item-1;

	//MENU_SET_DATA m_init_data=init_data;
#ifdef DBG_TEST_MODE
		printf("(1)TEST_ACTION ===relayNum[%d] updn[%d] on[%d]\r\n", relayNum, draw_event, on);
#endif
	//on=(on)? 0:1;
	on += position;
	on=(on>0)? 1: 0;//ON-OFF

	switch(relayNum){
	case 0:	case 1:	case 2:	case 3:	case 4:	case 5:
		Public_RELAY_CTRL_Immediately(relayNum, on);
		break;
	case 6://부저테스터
		if(on)HAL_GPIO_WritePin(pBEEP_GPIO_Port, pBEEP_Pin, GPIO_PIN_SET);
		else HAL_GPIO_WritePin(pBEEP_GPIO_Port, pBEEP_Pin, GPIO_PIN_RESET);
		break;
	case 7://공장초기화
		if(on){
			memcpy(&pMain_page->UI_Data.data_arry, &init_data, sizeof(init_data));
			memcpy(&pMain_page->UI_Data.target, &init_system_target, sizeof(init_system_target));
			pDataClass->vRec.clear();
		}
#ifdef DBG_TEST_MODE
		printf("=============ON[%d]====================\r\n",on);
		for(int i=0;i<6;i++){
			for(int ix =0; ix<10;ix++){
				printf("TEST_ACTION data_arry [%d][%d] -[%d]\r\n", i, ix, pMain_page->UI_Data.data_arry[i].idata[ix]);
			}
		}
		printf("TEST_ACTION target [%d][%d][%d]\r\n", pMain_page->UI_Data.target.temp, pMain_page->UI_Data.target.o3, pMain_page->UI_Data.target.wind);
		printf("=================================\r\n");
#endif
		break;
	}
}

void STM32_IO::RELAY_CTRL(uint8_t relay, uint8_t on) {

#ifdef DBG_TEST_MODE
	printf("\t\t-->RELAY_CTRL[%d] on[%d]\r\n",relay, on);
#endif
	switch(relay){
	case 0:
		if(on)HAL_GPIO_WritePin(pRY0_GPIO_Port, pRY0_Pin, GPIO_PIN_SET);
		else HAL_GPIO_WritePin(pRY0_GPIO_Port, pRY0_Pin, GPIO_PIN_RESET);
		break;
	case 1:
		if(on)HAL_GPIO_WritePin(pRY1_GPIO_Port, pRY1_Pin, GPIO_PIN_SET);
		else HAL_GPIO_WritePin(pRY1_GPIO_Port, pRY1_Pin, GPIO_PIN_RESET);
		break;
	case 2:
		if(on)HAL_GPIO_WritePin(pRY2_GPIO_Port, pRY2_Pin, GPIO_PIN_SET);
		else HAL_GPIO_WritePin(pRY2_GPIO_Port, pRY2_Pin, GPIO_PIN_RESET);
		break;
	case 3:
		if(on)HAL_GPIO_WritePin(pRY3_GPIO_Port, pRY3_Pin, GPIO_PIN_SET);
		else HAL_GPIO_WritePin(pRY3_GPIO_Port, pRY3_Pin, GPIO_PIN_RESET);
		break;
	case 4:
		if(on)HAL_GPIO_WritePin(pRY4_GPIO_Port, pRY4_Pin, GPIO_PIN_SET);
		else HAL_GPIO_WritePin(pRY4_GPIO_Port, pRY4_Pin, GPIO_PIN_RESET);
		break;
	case 5:
		if(on)HAL_GPIO_WritePin(pRY5_GPIO_Port, pRY5_Pin, GPIO_PIN_SET);
		else HAL_GPIO_WritePin(pRY5_GPIO_Port, pRY5_Pin, GPIO_PIN_RESET);
		break;

	}

	return;
}

void STM32_IO::Public_RELAY_CTRL_Immediately(uint8_t relay_no, uint8_t on) {
	RELAY relay=(RELAY)relay_no;
	switch(relay){
		case COMP_RY: RELAY_CTRL(COMP_RY,on);	break;
		case HEATER_RY:	RELAY_CTRL(HEATER_RY,on);	break;
		case EVAFAN_RY:	RELAY_CTRL(EVAFAN_RY,on);	break;
		case O3GEN_RY:	RELAY_CTRL(O3GEN_RY,on);	break;
		case O3FAN_RY:	RELAY_CTRL(O3FAN_RY,on);	break;
		case PUMP_DOWN:	RELAY_CTRL(PUMP_DOWN,on);	break;
		case RELAY_ALL:	for(int i=0;i<6;i++) {	RELAY_CTRL(i,on);}	break;
	}
	return;
}

void STM32_IO::SET_RELAY_CTRL_REGISTER(RELAY relay, uint16_t count, uint8_t relay_on) {
	int i;
	uint8_t lRelay=(uint8_t)relay;
	char *str[7]={(char *)"COMP_RY", (char *)"DEFROG_RY", (char *)"EVAFAN_RY", (char *)"O3_GEN_RY", (char *)"O3_FAN_RY",(char *)"",(char *)"PUMPDOWN_RY"};
	char *on[2]={(char *)"OFF", (char *)"ON"};
	relay_on %=2;

#ifdef DBG_STM32_IO
	printf("\t\t-->[%s] [%s] delay[%d]\r\n",str[lRelay], on[relay_on], count);
#endif

	lRelay%=7;
	if(lRelay<6)	{
		pDataClass->reg.relay[lRelay].on_count = count;
		pDataClass->reg.relay[lRelay].on = relay_on;
		pDataClass->reg.relay[lRelay].flag = 1;
	}
	else {
		for(i=0;i<6;i++) {
			pDataClass->reg.relay[i].on_count = count;
			pDataClass->reg.relay[i].on = relay_on;
			pDataClass->reg.relay[i].flag = 1;
		}
	}

}
//----------------------------------------------------------
//	[0][x]
//---------------------------------------------------------
//[0][0]10,//"온도편차",
//[0][1]0,//"온도보정",
//[0][2]30,//"콤프지연",  //켤때만 지연하나????
//[0][3]10,//"EVA팬지연",//끌때만 ???
//[0][4]0,//"콤프작동중 EVA팬"  콤프작동시 항상에바팬 동작 .. 그럼 콤프가 꺼지면 에바도 끄까???,
//[0][5]10,//"이상온도 경보 지속 시간(분)",
//[0][6]10,//이상온도 고온 경보(~+10℃)"
//[0][7]-10,//이상온도 저온 경보(~+10℃)",

//----------------------------------------------------------
//	[1][x] 제상팬 풍속 편차
//---------------------------------------------------------
//[1][0]2, //"lkoooooooiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii제상방법(주기/풍속/복합)",
//[1][1]240,//"제상주기(0-999분)",
//[1][2]20, //"제상시간(0-30분)"
//[1][3]30,//"제상팬 풍속 편차(0-100%)",
//[1][4]1,//"제상중 에바팬(ON/OFF)", //제상이 아닐때는???
//[1][5]30,//"제상후 에바팬 지연(0-600초)",
//[1][6]10,//"제상후 콤프 지연(0-600초)",//제상할때 무조건 컴푸가 꺼져 있고 다시 시작 할때 까지 타임
//[1][7]0,//"제상시 콤프동작(ON/OFF)",
//99

void STM32_IO::EVENT_JAESANG_CTRL_ON(FLAG_STATE lflag) {
	int latency;
	printf("(1) EVENT_DeFrog_Wind=> JAESANG_CTRL_ON[%d]|COMP_START_ON[%d]\r\n",lflag.b.JAESANG_EVENT_FLAG, lflag.b.COMPRES_EVENT_FLAG);

	if(lflag.b.JAESANG_EVENT_FLAG){
		if(lflag.b.COMPRES_EVENT_FLAG){
			SET_RELAY_CTRL_REGISTER(HEATER_RY, 0, ON);
			if(pDataClass->reg.parm.comp_on_at_jaesang){;}
			else{
				SET_RELAY_CTRL_REGISTER(COMP_RY, 0, OFF);
				SET_RELAY_CTRL_REGISTER(EVAFAN_RY, 0, OFF);
			}

		}
		else{
				SET_RELAY_CTRL_REGISTER(COMP_RY, 0, OFF);//
				SET_RELAY_CTRL_REGISTER(HEATER_RY, 0, ON);
				SET_RELAY_CTRL_REGISTER(EVAFAN_RY, 0, OFF);
		}

	}
	else{
		if(lflag.b.COMPRES_EVENT_FLAG){
			latency= pMain_page->UI_Data.data_arry[1].idata[7];//"제상후 콤프 지연
			SET_RELAY_CTRL_REGISTER(COMP_RY, latency, ON);//
			SET_RELAY_CTRL_REGISTER(HEATER_RY, 0, OFF);
			latency= pMain_page->UI_Data.data_arry[1].idata[6];//"제상후 에바팬 지연"
			SET_RELAY_CTRL_REGISTER(EVAFAN_RY, latency, ON);

		}
		else{
			SET_RELAY_CTRL_REGISTER(COMP_RY, 0, OFF);//
			SET_RELAY_CTRL_REGISTER(HEATER_RY, 0, OFF);
			SET_RELAY_CTRL_REGISTER(EVAFAN_RY, 0, OFF);

		}
	}
}


void STM32_IO::EVENT_OZONE_Period(FLAG_STATE lflag) {
	//char str[32];
	int latency= pMain_page->UI_Data.data_arry[2].idata[3];//
	printf("(1) EVENT_OZONE_Period=> OZONE_PERIOD[%d]|OZONE_VALUE_CTRL_ON[%d]\r\n",lflag.b.OZONE_PERIOD_CTRL_ON, lflag.b.OZONE_SENSOR_CTRL_ON );

	if(lflag.b.OZONE_PERIOD_CTRL_ON){
			SET_RELAY_CTRL_REGISTER(O3GEN_RY, latency, ON);//0초 relay 동작
			SET_RELAY_CTRL_REGISTER(O3FAN_RY, 0, ON);//latency초후 relay 동작
	}
	else{//off
			SET_RELAY_CTRL_REGISTER(O3GEN_RY, 0, OFF);//0초 relay 동작
			SET_RELAY_CTRL_REGISTER(O3FAN_RY, latency, OFF);//latency초후 relay 동작
	}
}

void STM32_IO::EVENT_OZONE_SensorCtrl(FLAG_STATE lflag) {

	int latency = pMain_page->UI_Data.data_arry[2].idata[3];
	printf("(E-1) EVENT_OZONE_Value=> OZONE_VALUE_CTRL_ON[%d]|OZONE_PERIOD[%d]\r\n",lflag.b.OZONE_SENSOR_CTRL_ON, lflag.b.OZONE_PERIOD_CTRL_ON );
	if(lflag.b.OZONE_SENSOR_CTRL_ON){
		printf("(E-2) EVENT_OZONE_Value=> OZONE_VALUE_CTRL_ON[%d]|OZONE_PERIOD[%d] latency[%d]\r\n",lflag.b.OZONE_SENSOR_CTRL_ON, lflag.b.OZONE_PERIOD_CTRL_ON, latency );
		SET_RELAY_CTRL_REGISTER(O3GEN_RY, latency, ON);//0초 relay 동작
		SET_RELAY_CTRL_REGISTER(O3FAN_RY, 0, ON);//latency초후 relay 동작
	}
	else{//off
		printf("(E-3) EVENT_OZONE_Value=> OZONE_VALUE_CTRL_ON[%d]|OZONE_PERIOD[%d]\r\n",lflag.b.OZONE_SENSOR_CTRL_ON, lflag.b.OZONE_PERIOD_CTRL_ON, latency );
		SET_RELAY_CTRL_REGISTER(O3GEN_RY, 0, OFF);//0초 relay 동작
		SET_RELAY_CTRL_REGISTER(O3FAN_RY, latency, OFF);//latency초후 relay 동작
	}
}

uint8_t STM32_IO::DOOR_SW_Read(){
return HAL_GPIO_ReadPin(pPC1_GPIO_Port, pPC1_Pin);
}
uint8_t STM32_IO::DP_SW_Read(){
return HAL_GPIO_ReadPin(pPC2_GPIO_Port, pPC2_Pin);
}

