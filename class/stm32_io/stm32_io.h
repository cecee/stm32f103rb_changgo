#ifndef KEYPAD_ADC_H
#define KEYPAD_ADC_H


//#pragma once
//#include "extern.h"
typedef struct {
	RELAY relay;
	uint8_t on;
	uint16_t count;
}RELAY_CONTROL_EVENT;

class STM32_IO
{
  private:
    void RELAY_CTRL(uint8_t relay, uint8_t on);//flag없이 순수동작을 다른데서 못하게~
  public:

	NAVI_BIT navi_state;
	uint8_t cancel_key=0;
	uint8_t on_item_drawing=0;
	uint8_t on_KeyEvent = 0;
	uint8_t off_KeyEvent = 0;
	uint16_t ENCODER_CNT = 0;
	uint8_t ENCODER_DN = 0;
	uint8_t EX_ENCODER_DN=2;
	uint8_t EX_ENCODER_CNT=0;
	int ENCODER_DELTA_CNT=0;
	vector<uint8_t> vKEY_BUFFER;
	vector<int16_t> vENCODE_BUFFER;
	vector<RELAY_CONTROL_EVENT> vRELAY_CONTROL_EVENT;
	int16_t encoder_position=0;
	uint16_t buzzer_cnt;
    STM32_IO();
    virtual ~STM32_IO();
    void BEEP_ON(void);
    void BEEP_OFF(void);

    void kpad_init(void);
    void Get_AdcData();
   // void Get_AdcKeyValue(uint16_t adc0,  uint16_t adc1);

    uint8_t Get_Key_Buffer();
    int16_t Get_ENCODE_Buffer();
    uint16_t Get_Encoder_cnt();

    void Double_KeyProcess(void);
    void Single_KeyProcess(void);
    void Encoder_UpDnProcess(int16_t position);

    void RelayEventVector_Process();
    void PutRelayEventVector(RELAY relay, uint8_t on, uint16_t count);

////++
    void Clear_Key_Buffer();
    void MENU_KeyProcess(void);
    void MENUX10_KeyProcess(void);
    void LEFT_RIGHT_KeyProcess(uint8_t left, uint8_t plus);
    void UPDN_KeyProcess(uint8_t up, uint8_t plus);
    void JESANG_KeyProcess(void);
    void JESANG_MQ_KeyProcess(uint8_t on);
////--
    void SET_RELAY_CTRL_REGISTER(RELAY relay, uint16_t count, uint8_t relay_on);
    void Public_RELAY_CTRL_Immediately(uint8_t relay_no, uint8_t on);
   // void EVENT_DeFrog_Period(FLAG_STATE lflag);
    void EVENT_JAESANG_CTRL_ON(FLAG_STATE lflag);
    void EVENT_OZONE_Period(FLAG_STATE lflag);
    void EVENT_OZONE_SensorCtrl(FLAG_STATE lflag);
    void TEST_ACTION(NAVI_BIT navi, MENU_DRAW_EVENT draw_event, int16_t position);
    void RotaryEncoder_clear();
   // void RELAY_CTRL_ALL_OFF();
    uint8_t DOOR_SW_Read();
    uint8_t DP_SW_Read();
};

#endif
