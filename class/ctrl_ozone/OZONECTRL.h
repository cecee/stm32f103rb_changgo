/*
 * OZONECTRL.h
 *
 *  Created on: Mar 14, 2022
 *      Author: thpark
 */

#ifndef OZONE_OZONECTRL_H_
#define OZONE_OZONECTRL_H_
#include "extern.h"
typedef enum{
  OZONE_PROCESS_NOUN  = 0,
  OZONE_PROCESS_OFF  = 1,
  OZONE_PROCESS_ON   = 2,
}OZONE_PROCESS_SEQ;

typedef enum{
  OZONE_PERIOD_MODE  = 0,
  OZONE_SENSOR_MODE  = 1,
}OZONE_CONTROL_MODE;


class OZONE_CTRL
{
public:
	//OZONE_INFO ozone_info;
	uint8_t period_seq=0;
	uint8_t sensor_seq=0;
	uint16_t period_wait=0;
	uint16_t ozone_current_time=0;
	OZONE_CTRL();
	virtual ~OZONE_CTRL();
	void ctrl_ozone(int measure);
	//void Ozone_mode_exchange();
	uint8_t Ozone_period_control(void);
	uint8_t Ozone_sensor_control(int measure);
};

#endif /* OZONE_OZONECTRL_H_ */
