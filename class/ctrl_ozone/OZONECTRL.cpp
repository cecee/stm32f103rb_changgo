/*
 * OZONECTRL.cpp
 *
 *  Created on: Mar 14, 2022
 *      Author: thpark
 */
#include "extern.h"
#include "OZONECTRL.h"

#define xDBG_OZONE

OZONE_CTRL *pOZONE;
extern DATA_CLASS *pDataClass;


OZONE_CTRL::OZONE_CTRL()
{
	// TODO Auto-generated constructor stub
	//memset(&ozone_info,0,sizeof(ozone_info));

}

OZONE_CTRL::~OZONE_CTRL()
{
	// TODO Auto-generated destructor stub
}

void OZONE_CTRL::ctrl_ozone(int measure){

	uint8_t ozone_ctrl=0x80;

	if(pDataClass->reg.local_ozone_ctrl_mode==0){//주기모드
		ozone_ctrl=Ozone_period_control();

	}
	else{
		ozone_ctrl=Ozone_sensor_control(measure);
	}

	//온도에 따라서
//		//측정온도가 설정온도보다 낮고 온도편차가 2도(?) 이상나면
//			if(pDataClass->reg.sysFlag.b.HANGON_ON_FLAG){
//				ozone_ctrl=1;
//			}

		//ozone센서이상++
			if(pDataClass->RS485_ozone_sensor_exist==0){
				ozone_ctrl=0;
			}
		//ozone센서이상--

		//테스터모드 ++

		//테스터모드 --
#ifdef DBG_OZONE
		printf("\r\nOZONE[%d] mode[%d] measure[%d] \r\n",ozone_ctrl, pDataClass->reg.local_ozone_ctrl_mode, measure);
#endif
			switch(ozone_ctrl){
			case 0: //off
				if(pDataClass->reg.parm.ozone_gen_on==ON){
					pDataClass->reg.parm.ozone_gen_on=OFF;
					pSTM32_IO->PutRelayEventVector(O3GEN_RY,OFF, 0);
					pSTM32_IO->PutRelayEventVector(O3FAN_RY,OFF, pDataClass->reg.parm.ozone_fan_off_delay);
				}
				break;
			case 1: //ozone on
				if(pDataClass->reg.parm.ozone_gen_on==OFF){
					pDataClass->reg.parm.ozone_gen_on=ON;
					pSTM32_IO->PutRelayEventVector(O3GEN_RY,ON, pDataClass->reg.parm.ozone_gen_on_delay);
					pSTM32_IO->PutRelayEventVector(O3FAN_RY,ON, 0);
				}
				break;
			default:
				break;

			}
	//

}

uint8_t OZONE_CTRL::Ozone_period_control(void){

	uint8_t ozone_ctrl=0x80;
	if(pDataClass->reg.parm.ozone_start_time<10){
		//No Control JAESANG
		period_seq=10;
	}
	ozone_current_time++;
	switch((OZONE_PROCESS_SEQ)period_seq)
	{

	case OZONE_PROCESS_NOUN:
		ozone_ctrl=0;
		//pDataClass->reg.sysFlag.b.OZONE_PERIOD_CTRL_ON=0;//OFF
		ozone_current_time=0;
		period_seq=OZONE_PROCESS_OFF;
		break;
	case OZONE_PROCESS_OFF:
		ozone_ctrl=0;
		period_wait++;
		if(period_wait<pDataClass->reg.parm.ozone_start_time){
#ifdef DBG_OZONE
			printf("오존_OFF period_seq[%d] 주기[%d/%d] ozone_current_time[%d]\r\n",period_seq, period_wait, ozone_info.ozone_start_time, ozone_current_time);
#endif
		}
		else{
			ozone_ctrl=1;
			//pDataClass->reg.sysFlag.b.OZONE_PERIOD_CTRL_ON=1;//ON
			period_wait=0;
			period_seq=OZONE_PROCESS_ON;
		}
		break;
	case OZONE_PROCESS_ON:
		period_wait++;
		ozone_ctrl=1;
		if(period_wait<pDataClass->reg.parm.ozone_on_duration){
#ifdef DBG_OZONE
			printf("오존_ON period_seq[%d] 주기[%d/%d]\r\n",period_seq, period_wait,ozone_info.ozone_on_duration);
#endif
		}
		else{
			ozone_ctrl=0;
			period_wait=0;
			period_seq=0; //return 처음으로
		}
		break;
	default:
		break;
	}
   return ozone_ctrl;
}


uint8_t  OZONE_CTRL::Ozone_sensor_control(int measure){
	uint8_t ozone_ctrl=0x80;
	int ref=pDataClass->reg.parm.seljung_ozone;//(int)pMain_page->UI_Data.target.o3;
	int hysteresis=pDataClass->reg.parm.ozone_diff;//pMain_page->UI_Data.data_arry[2].idata[4];
	int on_ref=ref+hysteresis;
	int off_ref=ref;
	if(measure<=on_ref){
#ifdef DBG_OZONE
		printf("OZONE_VALUE_CTRL_ON[ON] measure[%d] ref[%d] hysteresis[%d] on_ref[%d] off_ref[%d]\r\n",measure, ref, hysteresis, on_ref, off_ref );
#endif
		//pDataClass->reg.sysFlag.b.OZONE_SENSOR_CTRL_ON=1;
		ozone_ctrl=1;
	}
	else if(measure>=off_ref){
#ifdef DBG_OZONE
			printf("OZONE_VALUE_CTRL_ON[OFF] measure[%d] ref[%d] hysteresis[%d] on_ref[%d] off_ref[%d]\r\n",measure, ref, hysteresis, on_ref, off_ref );
#endif
		//pDataClass->reg.sysFlag.b.OZONE_SENSOR_CTRL_ON=0;//OFF
		ozone_ctrl=0;
	}
	   return ozone_ctrl;
}

