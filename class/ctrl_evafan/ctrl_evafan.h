/*
 * ctrlevafan.h
 *
 *  Created on: 2022. 5. 18.
 *      Author: thpark
 */

#ifndef CTRL_EVAFAN_CTRLEVAFAN_H_
#define CTRL_EVAFAN_CTRLEVAFAN_H_

//#define EVAFAN_PERIOD    1*MINUTE
#define EVAFAN_DURATION  20*SECOND //5(대기)+10(회측정) 이상



class EVAFAN
{
private:
	int GET_avr_wind(vector<int> vWind_data);
public:
	//EVAFAN_INFO eva_info;
	vector<int> vWind_data;
	EVAFAN();
	virtual ~EVAFAN();
	void ctrl_evafan(int measure_temperature, int measure_wind);
};

#endif /* CTRL_EVAFAN_CTRLEVAFAN_H_ */
