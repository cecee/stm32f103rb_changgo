/*
 * ctrlevafan.cpp
 *
 *  Created on: 2022. 5. 18.
 *      Author: thpark
 */
#include "extern.h"
#include "ctrl_evafan.h"

#define xDBG_EVAFAN

EVAFAN *pEVAFAN;


EVAFAN::EVAFAN()
{
	// TODO Auto-generated constructor stub
//	memset(&eva_info,0,sizeof(eva_info));
	vWind_data.clear();
}

EVAFAN::~EVAFAN()
{
	// TODO Auto-generated destructor stub
}

int EVAFAN::GET_avr_wind(vector<int> vWind_data)
{
	int avr_wind;
	int i,sz;
	int sum=0;
	sz=vWind_data.size();
	for(i=0;i<sz;i++){
		sum=sum + vWind_data[i];
	}
	avr_wind=sum/sz;
	return avr_wind;
}

void EVAFAN::ctrl_evafan(int measure_temperature, int measure_wind)
{
	uint8_t fan_ctrl=0x80;
	int wind_sersor_preiod;
	//eva_info.evafan_off_latency= pMain_page->UI_Data.data_arry[0].idata[4];//"EVA팬지연"
//comp 연동
	if(pDataClass->reg.parm.comp_on_flag) fan_ctrl=1;
	else fan_ctrl=0;
//제상Heater연동

	if(pDataClass->reg.parm.jaesang_heater_on_flag){//제상Heater ON case
		if(pDataClass->reg.parm.comp_on_at_jaesang){//제상시 콤프동작
			if(pDataClass->reg.parm.comp_on_flag) fan_ctrl=1;
			else fan_ctrl=0;
		}
		else {
			fan_ctrl=0;
		}

	}
	else{ //제상Heater OFF case
		if(pDataClass->reg.parm.comp_on_flag) fan_ctrl=1;
		else fan_ctrl=0;
	}
//온도에 따라서
	//측정온도가 설정온도보다 낮고 온도편차가 2도(?) 이상나면
		if(pDataClass->reg.sysFlag.b.SUPER_COOL_PROTECT){
			fan_ctrl=1;
		}

//제상 센서모드 일때++
		if(pDataClass->reg.local_jeasang_ctrl_mode==JS_SENSOR){
			wind_sersor_preiod=pDataClass->reg.parm.jaesang_sensor_period;//pMain_page->UI_Data.data_arry[1].idata[4]*MINUTE;//"제상센서읽는주기(분)"
			//강제 제상일때 센서 읽기 중지하기위하여 카운터 초기화합니다.++
			//강제 제상중이라도 센서모드이면 제상 센서에 따라 제상정지를 할까???
			if(pDataClass->reg.sysFlag.b.JAESANG_FORCE_ON) pDataClass->reg.parm.evafan_period_cnt=EVAFAN_DURATION;
			else pDataClass->reg.parm.evafan_period_cnt++;
			//강제 제상일때 센서 읽기 중지하기위하여 카운터 초기화합니다.--

			if(pDataClass->reg.parm.evafan_period_cnt >=wind_sersor_preiod){
				pDataClass->reg.parm.evafan_period_cnt=0;
				pDataClass->reg.parm.windsensor_measure_flag=0;
			}
			if(pDataClass->reg.parm.evafan_period_cnt < EVAFAN_DURATION){
				fan_ctrl=1;
				if(pDataClass->reg.parm.evafan_period_cnt>5){
					//make wind_avr
					if(pDataClass->reg.parm.windsensor_measure_flag==0){
						vWind_data.push_back(measure_wind);
						if(vWind_data.size()>=10){
							//평균보고 제상 relay on또는 off
							pDataClass->reg.parm.windsensor_measure_flag=1;
							pDataClass->reg.parm.measure_wind_avr=GET_avr_wind(vWind_data);
							vWind_data.clear();
							if(pDataClass->reg.parm.measure_wind_avr < pDataClass->reg.parm.seljung_jaesang) pDataClass->reg.parm.decision_heater_on_flag=1;
							else pDataClass->reg.parm.decision_heater_on_flag=0;
#ifdef DBG_EVAFAN
		printf("\r\n(측정)EVA_FAN[%d] evafan_period_cnt[%d] decision_heater_on_flag[%d] measure_wind_avr[%d]\r\n",fan_ctrl, eva_info.evafan_period_cnt, eva_info.decision_heater_on_flag, eva_info.measure_wind_avr);
#endif
						}
					}
				}
			}

		}
//제상센서모드일때--

	//온도센서이상++
		if(pDataClass->reg.sysFlag.b.TEMPERATURE_SENSOR_ERROR){
			fan_ctrl=0;
		}
	//온도센서이상--

	//테스터모드 ++

	//테스터모드 --

#ifdef DBG_EVAFAN
			printf("\r\nEVA_FAN[%d] evafan_period_cnt[%d/%d] measure[%d] HANGON_ON_FLAG[%d]\r\n",fan_ctrl, eva_info.evafan_period_cnt, wind_sersor_preiod, measure_temperature, pDataClass->reg.sysFlag.b.SUPER_COOL_PROTECT);
			printf(    "EVA_FAN---- decision_heater_on_flag[%d] measure_wind_avr[%d]\r\n", eva_info.decision_heater_on_flag, eva_info.measure_wind_avr);
#endif
		switch(fan_ctrl){
		case 0: //off
			if(pDataClass->reg.parm.evafan_on_flag==ON){
				pDataClass->reg.parm.evafan_on_flag=OFF;
				if(pDataClass->reg.parm.comp_on_flag) pSTM32_IO->PutRelayEventVector(EVAFAN_RY,OFF, pDataClass->reg.parm.evafan_off_latency);
				else pSTM32_IO->PutRelayEventVector(EVAFAN_RY,OFF, 0);
			}
			break;
		case 1: //지연 on
			if(pDataClass->reg.parm.evafan_on_flag==OFF){
				pDataClass->reg.parm.evafan_on_flag=ON;
				pSTM32_IO->PutRelayEventVector(EVAFAN_RY,ON,0);
			}
			break;
		default:
			break;

		}
//

}
