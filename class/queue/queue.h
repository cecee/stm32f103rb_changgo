/*
 * queue.h
 *
 *  Created on: Jul 24, 2022
 *      Author: thpark
 */

#ifndef QUEUE_QUEUE_H_
#define QUEUE_QUEUE_H_

#define MAX_SIZE 512 // max size of queue

typedef struct _QUEUE
{
	int queue[MAX_SIZE+1];
	int front;
	int rear;
}QUEUE;


class queue
{
private:
	void init_qUart3(void);
public:
	QUEUE qUart3;
	uint8_t qUart3_package=0;
	uint8_t qUart3_buffer[256];
	uint8_t qUart3_cnt=0;
	uint8_t qUart3_sz=0xff;
	queue();
	virtual ~queue();
	int put_qUart3(int k);
	int get_qUart3(void);
	int que_prosess(void);

};

#endif /* QUEUE_QUEUE_H_ */
