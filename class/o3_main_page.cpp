#include "extern.h"
#include "o3_main_page.h"
#include <string>
#include <cstdarg>
//#include <format> //c++20에서만 지원
//#include <iostream>
//#include "iwdg.h"
#include "gpio.h"
#include "sub_menu_string.h"

using namespace std;

O3_MAIN_PAGE *pMain_page;
//extern ILI9488_DRV *pIli9488;
extern SPI1_FLASH *pFlashClass;
extern RTC_API *pRTC_API;
extern RS485 *pRS485;
//extern WIND_SENSOR *pWind;
extern MODEM *pMODEM;
extern DATA_CLASS *pDataClass;




O3_MAIN_PAGE::O3_MAIN_PAGE()
{
	memcpy(&UI_Data.data_arry[0].idata[0], &init_data[0].idata[0],
			sizeof(init_data));
	memcpy(&UI_Data.target, &init_system_target, sizeof(SYSTEM_TARGET));
	Operating_bit.comp = 0;
	Operating_bit.defrog = 0;
	Operating_bit.o3_gen = 0;
	Operating_bit.alarm = 0;
	//memset(&system_sensor, 0, sizeof(SYSTEM_SENSOR));
}

O3_MAIN_PAGE::~O3_MAIN_PAGE()
{
	;
}

void O3_MAIN_PAGE::init_console_value(void)
{

}

void O3_MAIN_PAGE::initPage(void)
{
	init_console_value();
	//build date++
	sprintf(UI_Data.sys_info.build_date,"%s %s", __DATE__, __TIME__ );//Build time save
	//build date--

	//pIli9488->ili9488_DrawRGBImage_Flash(0, 0, 0, 480, 320);
	Draw_Home(TOP_COMP);
	//printf("UI========= %s ========\r\n", UI_Data.sys_info.build_date); //);
}

void O3_MAIN_PAGE::draw_graphFrame(int y)
{
	int Hline_ypos[4];
	int len;
	uint16_t graph_bc = GRAPH_BG_COLOR;
	uint16_t graph_fc = GRAPH_FG_COLOR;

	Hline_ypos[0] = y + 24;
	Hline_ypos[1] = y + 24 + (40 * 1);
	Hline_ypos[2] = y + 24 + (40 * 2);
	Hline_ypos[3] = y + 24 + (40 * 3);

	pIli9488->ILI9341_Draw_Filled_Rectangle_Coord(GS_X_START, y,GS_X_START + GS_WIDTH, y + GS_HEIGHT, graph_bc);

	//양쪽수직선 |<--GS_WIDTH-->|
	//pIli9488->ILI9341_Draw_VLineW(xStart, y, 132, 2, TFT_GREY);
	//pIli9488->ILI9341_Draw_VLineW(xStart + GS_WIDTH, y + 0, 132, 2, TFT_GREY);

	pIli9488->DrawFontTTF_Digital16(GS_X_START, Hline_ypos[0] - 24, GRAPH_TMP_COLOR,	graph_bc, (uint8_t*) "10", 0);
	pIli9488->DrawFontTTF_Digital16(GS_X_START, Hline_ypos[1] - 24, GRAPH_TMP_COLOR,	graph_bc, (uint8_t*) "5", 0);
	len = pIli9488->DrawFontTTF_Digital16(GS_X_START, Hline_ypos[2] - 24, GRAPH_TMP_COLOR, graph_bc, (uint8_t*) "0", 0);
	pIli9488->DrawFontTTF_Digital16(len, Hline_ypos[2] - 24, GRAPH_TMP_COLOR,	graph_bc, (uint8_t*) "℃", 0);
	pIli9488->DrawFontTTF_Digital16(GS_X_START, Hline_ypos[3] - 24, GRAPH_TMP_COLOR,	graph_bc, (uint8_t*) "-5", 0);
	pIli9488->DrawFontTTF_Digital16(GS_X_START + GS_WIDTH - 56, Hline_ypos[0] - 24,	GRAPH_O3_COLOR, graph_bc, (uint8_t*) "0.75", 0);
	pIli9488->DrawFontTTF_Digital16(GS_X_START + GS_WIDTH - 56, Hline_ypos[1] - 24,	GRAPH_O3_COLOR, graph_bc, (uint8_t*) "0.50", 0);
	pIli9488->DrawFontTTF_Digital16(GS_X_START + GS_WIDTH - 56, Hline_ypos[2] - 24,	GRAPH_O3_COLOR, graph_bc, (uint8_t*) "0.25", 0);
	pIli9488->DrawFontTTF_Digital16(GS_X_START + GS_WIDTH - 68, Hline_ypos[3] - 24,	GRAPH_O3_COLOR, graph_bc, (uint8_t*) "0ppm", 0);
	pIli9488->DrawFontTTF_Digital16(GS_X_START, y + GS_HEIGHT - 20, graph_fc, graph_bc, (uint8_t*) "24h      18h        12h         6h       0h",	0);

	//수평선
	pIli9488->ILI9488_Draw_HLineW(GS_X_START, Hline_ypos[0]-40, GS_WIDTH, 2,TFT_GRAY);
	pIli9488->ILI9488_Draw_HLineW(GS_X_START, Hline_ypos[0], GS_WIDTH, 2,TFT_LIGHTGREY);
	pIli9488->ILI9488_Draw_HLineW(GS_X_START, Hline_ypos[1], GS_WIDTH, 2,TFT_LIGHTGREY); //12+40
	pIli9488->ILI9488_Draw_HLineW(GS_X_START, Hline_ypos[2], GS_WIDTH, 2,TFT_LIGHTGREY); //y+12+40+40
	pIli9488->ILI9488_Draw_HLineW(GS_X_START, Hline_ypos[3], GS_WIDTH, 2,TFT_GRAY); //12+40+40+40
	//눈금선
	pIli9488->ILI9488_Draw_VLineW(GS_X_START + (108 * 1), Hline_ypos[3] - 14,	14, 2, TFT_GRAY);
	pIli9488->ILI9488_Draw_VLineW(GS_X_START + (108 * 2), Hline_ypos[3] - 14,	14, 2, TFT_GRAY);
	pIli9488->ILI9488_Draw_VLineW(GS_X_START + (108 * 3), Hline_ypos[3] - 14,	14, 2, TFT_GRAY);
	pIli9488->ILI9488_Draw_VLineW(GS_X_START + (108 * 4) - 2,
			Hline_ypos[3] - 14, 14, 2, TFT_GRAY);
}
void O3_MAIN_PAGE::Update_Display_period(uint16_t Xpos, uint16_t Ypos, uint16_t period, uint16_t duration, uint16_t current)
{
	uint16_t len, str_xpos;
	uint16_t mPERIOD_LENGTH=260;

	char temp_str[64] =	{ 0, };
	uint16_t on_length, duration_x, current_x;

	if(current>period) current=0;
	on_length=(mPERIOD_LENGTH*duration)/period;
	duration_x=Xpos+(mPERIOD_LENGTH-on_length);
	current_x=Xpos+(mPERIOD_LENGTH*current)/period;
	sprintf(temp_str,"  %02d:%02d/%02d:%02d",current/60, current%60, period/60, period%60);
	len=pIli9488->GetWidthTTF_Digital16((uint8_t*)temp_str);

	//printf("Update_Display_period====GetWidthTTF16Asc[%d]====\r\n", len);
	str_xpos=(Xpos+mPERIOD_LENGTH)-len;//end 정열
	pIli9488->DrawFontTTF_Digital16(str_xpos, Ypos-24, TFT_BLACK ,TFT_WHITE, (uint8_t*)temp_str, 0);

	pIli9488->ILI9488_Draw_HLineW(Xpos, Ypos, mPERIOD_LENGTH, 20,TFT_GRAY);
	pIli9488->ILI9488_Draw_HLineW(duration_x, Ypos, on_length, 20,MINJI_GREEN);
	if(current_x <= (Xpos + mPERIOD_LENGTH)-4){ //마지막바 못그리게 (넘겨서 그리는 경우제외)
		pIli9488->ILI9488_Draw_HLineW(current_x, Ypos, 4, 20,TFT_RED);
	}
}

void O3_MAIN_PAGE::Update_Display_measured(TOP_MENU top_menu)
{
	int temp = pDataClass->measured_temperature;
	int humidty = pDataClass->measured_humidity;
	uint8_t temp_sensor_exist = pDataClass->RS485_temperature_sensor_exist;
	uint8_t ozone_sensor_exist = pDataClass->RS485_ozone_sensor_exist;

	int o3 = pDataClass->measured_ozone;
	int wind = pDataClass->measured_wind;
	string stemp ="";
	string shumi ="";
	string sth ="";
	string sOzone ="";
	string wind_str[2] =	{ "","" };
	string str_wind_value;
	string str_jaesang_info="";
	uint16_t char_color;

	int len=0;

	if(pDataClass->sys_sec_tick<5){return;}//초기에 이상한값

	//temp=(temp>1000)? 999 : temp;
	//sht20 센서작동하지않으면...
	if (temp_sensor_exist == 0)	{
		stemp="- - -  ";
		shumi="- - -  ";
	}
	else{
		stemp=format("%.1f ", (float)temp / 10);//℃
		shumi=format("%.1f ", (float)humidty / 10);//%
	}

	if (ozone_sensor_exist==0)sOzone="---  ";
	else sOzone=format("%.2f", (float)o3 / 1000);//ppm

	uint16_t bc = TFT_WHITE;
	uint16_t X1=TOP_MEASURE_POS_X;
	uint16_t Y1=TOP_MEASURE_POS_Y;

	//printf("---[%d] stemp[%s]---\r\n",temp, stemp.c_str() );
	//printf("Update_Display_measured ---wind[%d] --\r\n",wind );

	int seljung_wind=0;
	switch (top_menu)
	{
	case TOP_COMP:
		pIli9488->DrawFontTTF_Digital40Num(TOP_MEASURE_POS_X, TOP_MEASURE_POS_Y, TFT_BLACK, bc, (uint8_t*) stemp.c_str(), 0);
		pIli9488->DrawFontTTF_Digital16(TOP_UINT_POS_X, TOP_MEASURE_POS_Y, TFT_BLACK ,bc, (uint8_t*)"℃ ", 0);

		pIli9488->DrawFontTTF_Digital40Num(TOP_MEASURE_POS_X, TOP_MEASURE_POS_Y1, TFT_BLACK, bc, (uint8_t*) shumi.c_str(), 0);
		pIli9488->DrawFontTTF_Digital16(TOP_UINT_POS_X, TOP_MEASURE_POS_Y1, TFT_BLACK ,bc, (uint8_t*)"% ", 0);

		//len=pIli9488->DrawFontTTF_Digital16(X1, TOP_MEASURE_POS_Y+50, TFT_BLACK, bc, (uint8_t*) shumi.c_str(), 0);
		//pIli9488->DrawFontTTF_Digital16(TOP_UINT_POS_X, TOP_MEASURE_POS_Y+50, TFT_BLACK ,bc, (uint8_t*)"% ", 0);
		break;
	case TOP_OZONE:
		if(pDataClass->reg.local_ozone_ctrl_mode==OZONE_PERIOD_MODE){//주기모드
			//sOzone=sOzone+"ppm  ";//+stemp;
			//pOZONE->ozone_current_time
			sOzone=format("%04d  ", pOZONE->ozone_current_time);
			pIli9488->DrawFontTTF_Digital40Num(TOP_MEASURE_POS_X, TOP_MEASURE_POS_Y, TFT_BLACK, bc, (uint8_t*) sOzone.c_str(), 0);

			//len= pIli9488->DrawFontTTF_Digital16(TOP_MEASURE_POS_X-30, TOP_MEASURE_POS_Y, TFT_BLACK ,bc, (uint8_t*) sOzone.c_str(), 0);
			//len= pIli9488->DrawFontTTF_Digital16(TOP_MEASURE_POS_X-30, TOP_MEASURE_POS_Y+24, TFT_BLACK ,bc, (uint8_t*) stemp.c_str(), 0);
			//pIli9488->DrawFontTTF_Digital16(len, TOP_MEASURE_POS_Y+24, TFT_BLACK ,bc, (uint8_t*) "℃  ", 0);
			Update_Display_period(34, 110, pDataClass->reg.parm.ozone_total_period, pDataClass->reg.parm.ozone_on_duration, pOZONE->ozone_current_time);
		}
		else{
			pIli9488->DrawFontTTF_Digital40Num(X1, TOP_MEASURE_POS_Y,	TFT_BLACK, bc, (uint8_t*) sOzone.c_str(), 0);
			pIli9488->DrawFontTTF_Digital16(TOP_UINT_POS_X, TOP_MEASURE_POS_Y, TFT_BLACK ,bc, (uint8_t*)"ppm", 0);
		}
		break;
	case TOP_JESANG:	//wind_str
		switch(pDataClass->reg.local_jeasang_ctrl_mode){
			case JS_SENSOR:
				str_wind_value = (pDataClass->reg.parm.evafan_on_flag) ? format("%04d  ", wind): format("%04d  ", pDataClass->reg.parm.measure_wind_avr);
				char_color= (pDataClass->reg.parm.evafan_on_flag) ?  TFT_BLACK:MINJI_GRAY;

				len= pIli9488->DrawFontTTF_Digital16(TOP_MEASURE_POS_X, TOP_MEASURE_POS_Y+40, TFT_BLACK ,bc, (uint8_t*) stemp.c_str(), 0);
				pIli9488->DrawFontTTF_Digital16(len, TOP_MEASURE_POS_Y+40, TFT_BLACK ,bc, (uint8_t*) "℃  ", 0);
				//measure_wind_avr
				len=pIli9488->DrawFontTTF_Digital40Num(TOP_MENU_START_X + 80, TOP_MEASURE_POS_Y-10, char_color, bc, (uint8_t*)str_wind_value.c_str(), 0);
					pIli9488->DrawFontTTF_Digital16(TOP_UINT_POS_X,        TOP_MEASURE_POS_Y-10, TFT_BLACK ,bc, (uint8_t*) "wd", 0);
				if(pJAESANG->jaesang_sensor_seq==1){
					wind_str[0]=format("%02d:%02d ",pJAESANG->jaesang_sensor_wait/60, pJAESANG->jaesang_sensor_wait%60);
					wind_str[1]=format("%02d:%02d ",pDataClass->reg.parm.jaesang_on_duration/60, pDataClass->reg.parm.jaesang_on_duration%60);
				}
				else{
					seljung_wind=pDataClass->reg.parm.seljung_jaesang;//(pJAESANG->jaesang_max_wind_reference * UI_Data.target.wind)/100;

					wind_str[0]="      ";//TOP_UINT_POS_X
					wind_str[1]=format("%02d:%02d ",seljung_wind, pDataClass->reg.parm.jaesang_max_wind_reference);
				}
				//pIli9488->DrawFontTTF16Kr (TOP_MENU_START_X+80,  Y1 + 35, TFT_BLUE, bc,(uint8_t*)str_jaesang_info.c_str(), 0);
				pIli9488->DrawFontTTF_Digital16(TOP_MENU_START_X,     TOP_MEASURE_POS_Y + 70, TFT_BLACK, bc, (uint8_t*) wind_str[0].c_str(), 0);
				pIli9488->DrawFontTTF_Digital16(TOP_MENU_START_X+160, TOP_MEASURE_POS_Y + 70, TFT_BLACK ,bc, (uint8_t*) wind_str[1].c_str(), 0);
				break;
			case JS_PERIOD:
				//len= pIli9488->DrawFontTTF16Kr(TOP_MEASURE_POS_X, TOP_MEASURE_POS_Y, TFT_BLACK ,bc, (uint8_t*) stemp.c_str(), 0);
				//pIli9488->DrawFontTTF16Kr(len, TOP_MEASURE_POS_Y, TFT_BLACK ,bc, (uint8_t*) "℃  ", 0);
				len=pIli9488->DrawFontTTF_Digital40Num(TOP_MENU_START_X+80, TOP_MEASURE_POS_Y-10, TFT_BLACK, bc, (uint8_t*) stemp.c_str(), 0);
				pIli9488->DrawFontTTF_Digital16(TOP_UINT_POS_X, TOP_MEASURE_POS_Y-10, TFT_BLACK ,bc, (uint8_t*)"℃    ", 0);
				Update_Display_period(34, 110, pDataClass->reg.parm.jaesang_total_peroid, pDataClass->reg.parm.jaesang_on_duration, pDataClass->reg.parm.jaesang_current);

				break;
		}
		break;
	case TOP_GRAPH1:
		break;
	case TOP_FORCE_JAESANG:
		pIli9488->DrawFontTTF_Digital40Num(TOP_MENU_START_X + 80, TOP_MEASURE_POS_Y,	TFT_BLACK, bc, (uint8_t*) stemp.c_str(), 0);
		pIli9488->DrawFontTTF_Digital16(TOP_UINT_POS_X, TOP_MEASURE_POS_Y, TFT_BLACK ,bc, (uint8_t*)"℃ ", 0);

		pIli9488->DrawFontTTF_Digital40Num(TOP_MENU_START_X + 80, TOP_MEASURE_POS_Y+50,	TFT_BLACK, bc, (uint8_t*) shumi.c_str(), 0);
		pIli9488->DrawFontTTF_Digital16(TOP_UINT_POS_X, TOP_MEASURE_POS_Y+52, TFT_BLACK ,bc, (uint8_t*)"% ", 0);

		Update_Display_period(34, 230, pDataClass->reg.parm.jaesang_on_duration, pDataClass->reg.parm.jaesang_on_duration, pDataClass->reg.parm.jaesang_force_on_count);

		if(pDataClass->reg.sysFlag.b.JAESANG_FORCE_ON==0)
		{
			pDataClass->reg.parm.jaesang_heater_on_flag=0;//제상 끝내고 돌아가기
			pSTM32_IO->PutRelayEventVector(HEATER_RY,OFF,0);
			pSTM32_IO->navi_state.top_title=0;
			pMain_page->NAVI_ChangeMode(pSTM32_IO->navi_state, KEY_TOP_MENU_CHANGE_EVENT, 0);
		}
	}
}


void O3_MAIN_PAGE::Update_Display_Seljung(TOP_MENU page)
{
	int len;
	int temp_value = UI_Data.target.temp;
	int o3_value = UI_Data.target.o3;
	int wind_value = UI_Data.target.wind;

	uint16_t X=TOP_SELJUNG_POS_X;
	uint16_t Y=TOP_SELJUNG_POS_Y;
	char temp_str[64] =	{ 0, };
	char ozone_str[64] =	{ 0, };
	char wind_str[64] =	{ 0, };
	string temp[2]={"",""};
	sprintf(temp_str, "%.1f   ", (float) temp_value / 10);
	sprintf(ozone_str, "%.2f  ", (float) o3_value / 1000L);
	sprintf(wind_str, "%02d ", wind_value);
	//printf("------###### Update_Display_Seljung avr[%d]-->wind_value[%d]\r\n",pJAESANG->jaesang_max_wind_reference, wind_value);
	switch (page)
	{
	case TOP_COMP:
		pIli9488->DrawFontTTF_Digital40Num(X, Y, TFT_BLACK, TFT_WHITE, (uint8_t*) temp_str, 0);
		pIli9488->DrawFontTTF_Digital16(TOP_UINT_POS_X, Y, TFT_BLACK, TFT_WHITE,	(uint8_t*) "℃ ", 0);
		break;
	case TOP_OZONE:
		if(pDataClass->reg.local_ozone_ctrl_mode==OZONE_SENSOR_MODE){
			pIli9488->DrawFontTTF_Digital40Num(X, Y,	TFT_BLACK, TFT_WHITE, (uint8_t*) ozone_str, 0);
			pIli9488->DrawFontTTF_Digital16(TOP_UINT_POS_X, Y, TFT_BLACK, TFT_WHITE,	(uint8_t*) "ppm ", 0);
		}
		else if(pDataClass->reg.local_ozone_ctrl_mode==OZONE_PERIOD_MODE){
			len=pIli9488->DrawFontTTF_Digital16(X-30, Y,    TFT_BLACK, TFT_WHITE,(uint8_t*)"작동주기: ", 0);
			pIli9488->DrawFontTTF_Digital16(X-30, Y+24, TFT_BLACK, TFT_WHITE,(uint8_t*)"작동시간: ", 0);
			temp[0]=format("%03d:%02d ",pDataClass->reg.parm.ozone_total_period/60, pDataClass->reg.parm.ozone_total_period%60 );
			temp[1]=format("%03d:%02d ",pDataClass->reg.parm.ozone_on_duration/60, pDataClass->reg.parm.ozone_on_duration%60 );
			pIli9488->DrawFontTTF_Digital16(len, Y, TFT_BLACK, TFT_WHITE,	(uint8_t*) temp[0].c_str(), 0);
			pIli9488->DrawFontTTF_Digital16(len, Y+24, TFT_BLACK, TFT_WHITE,	(uint8_t*) temp[1].c_str(), 0);

		}
		break;
	case TOP_JESANG:
		if(pDataClass->reg.local_jeasang_ctrl_mode==JS_SENSOR){
			//printf("------###### Update_Display pJAESANG->jaesang_info.jaesand_wind_start_measure_cnt[%d]\r\n",pJAESANG->jaesang_info.jaesand_wind_start_measure_cnt);
			len=pIli9488->DrawFontTTF_Digital40Num(X, Y,	TFT_BLACK, TFT_WHITE, (uint8_t*) wind_str, 0);
			pIli9488->DrawFontTTF_Digital16(TOP_UINT_POS_X, Y, TFT_BLACK, TFT_WHITE,	(uint8_t*) "% ", 0);
		}
		else if (pDataClass->reg.local_jeasang_ctrl_mode==JS_PERIOD){
			len=pIli9488->DrawFontTTF_Digital16(X-30, Y,    TFT_BLACK, TFT_WHITE,(uint8_t*)"제상시간: ", 0);
			pIli9488->DrawFontTTF_Digital16(X-30, Y+24, TFT_BLACK, TFT_WHITE,(uint8_t*)"제상주기: ", 0);
			temp[0]=format("%03d:%02d ",pDataClass->reg.parm.jaesang_on_duration/60, pDataClass->reg.parm.jaesang_on_duration%60 );
			temp[1]=format("%03d:%02d ",pDataClass->reg.parm.jaesang_total_peroid/60, pDataClass->reg.parm.jaesang_total_peroid%60 );
			pIli9488->DrawFontTTF_Digital16(len, Y, TFT_BLACK, TFT_WHITE,	(uint8_t*) temp[0].c_str(), 0);
			pIli9488->DrawFontTTF_Digital16(len, Y+24, TFT_BLACK, TFT_WHITE,	(uint8_t*) temp[1].c_str(), 0);
		}
		break;
	case TOP_GRAPH1:
		break;
	}
}

void O3_MAIN_PAGE::Footer_Round()
{
	pIli9488->TFT_fillRoundRect(10,270,220, 40, 10, FOOTER_ROUND_COLOR);
	pIli9488->TFT_fillRoundRect(10+2,270+2, 220-4, 40-4, 10, FOOTER_BG_COLOR);
}

void O3_MAIN_PAGE::OutLineAndFooter()
{
    pIli9488->ILI9488_Draw_HLineW(0, 0, 480, 4, FOOTER_BG_COLOR);
	pIli9488->ILI9488_Draw_VLineW(0, 0, 320, 4, FOOTER_BG_COLOR);
	pIli9488->ILI9488_Draw_VLineW(480-4, 0, 320, 4, FOOTER_BG_COLOR);
	pIli9488->ili9488_FillRect(0, 260, 480, 60, FOOTER_BG_COLOR);
	Footer_Round();
}

void O3_MAIN_PAGE::Footer_Message(char *msg, uint16_t color)
{
	uint16_t str_len;
	uint16_t total_width =220-4;
	uint16_t begin_X =10+2;
	uint16_t posX, posY;
	char str[32]={0,};
	sprintf(str, "  %s  ",msg);//7char 기준
	str_len=pIli9488->GetWidthTTF_Digital16((uint8_t*)str);
	posX= begin_X + (total_width-str_len)/2;
	posY= ALARM_STRING_Y;
	//printf("----Footer_Message [%d]\r\n",str_len);//ALARM_STRING_NORMAL_COLOR
	pIli9488->DrawFontTTF_Digital16(posX, posY, color, FOOTER_BG_COLOR, (uint8_t*)str, 0);
}

void O3_MAIN_PAGE::Draw_History_title(uint8_t kind)
{
	uint16_t bc = TFT_WHITE;
	uint16_t len;
	uint16_t Xpos=GS_X_START+50;
	uint16_t Ypos=GS_Y_START-50;
	if(kind==0){
		len=pIli9488->DrawFontTTF_Digital16(Xpos, Ypos, FOOTER_ROUND_COLOR, bc, (uint8_t*) "24", 0);
		len=pIli9488->DrawFontTTF_Digital16(len,   Ypos, FOOTER_ROUND_COLOR, bc, (uint8_t*) "시간 ", 0);
		len=pIli9488->DrawFontTTF_Digital16(len,  Ypos, FOOTER_ROUND_COLOR, bc, (uint8_t*) "HISTORY ", 0);
    }

}

void O3_MAIN_PAGE::Draw_TopMenu_Form(TOP_MENU page)
{
	//pIli9488->ILI9341_Fill_Screen(TFT_WHITE);
	uint16_t bc = TFT_WHITE;
	uint16_t bar_color=FOOTER_ROUND_COLOR;
	uint16_t Xbar=34;
	uint16_t Ybar=144;
	uint16_t LENTHbar=260;//220;
	uint16_t Xstr=36;
	uint16_t Y1str=TOP_MEASURE_POS_Y;
	uint16_t Y2str=TOP_SELJUNG_POS_Y;
	string str[5]={"온도","오존","제상","설정","습도"};
	switch (page)
	{
	case TOP_COMP:
		pIli9488->ILI9488_Draw_HLineW(Xbar, Ybar, LENTHbar, 4, bar_color);
		pIli9488->DrawFontTTF_Digital16(Xstr, TOP_MEASURE_POS_Y,	TFT_BLACK, bc, (uint8_t*) str[0].c_str(), 0);
		pIli9488->DrawFontTTF_Digital16(Xstr, TOP_MEASURE_POS_Y1,	TFT_BLACK, bc, (uint8_t*) str[4].c_str(), 0);
		pIli9488->DrawFontTTF_Digital16(Xstr, Y2str,	TFT_BLACK, bc, (uint8_t*) str[3].c_str(), 0);
		break;
	case TOP_OZONE:
		pIli9488->ILI9488_Draw_HLineW(Xbar, Ybar, LENTHbar, 4, bar_color);
		pIli9488->DrawFontTTF_Digital16(Xstr, Y1str,	TFT_BLACK, bc, (uint8_t*) str[(TOP_MENU)page].c_str(), 0);
		pIli9488->DrawFontTTF_Digital16(Xstr, Y2str,	TFT_BLACK, bc, (uint8_t*) str[3].c_str(), 0);
		break;
	case TOP_JESANG:
		pIli9488->ILI9488_Draw_HLineW(Xbar, Ybar, LENTHbar, 4, bar_color);
		pIli9488->DrawFontTTF_Digital16(Xstr, Y1str,	TFT_BLACK, bc, (uint8_t*) str[(TOP_MENU)page].c_str(), 0);
		pIli9488->DrawFontTTF_Digital16(Xstr, Y2str,	TFT_BLACK, bc, (uint8_t*) str[3].c_str(), 0);
		break;
	case TOP_GRAPH1:
		break;
	case TOP_FORCE_JAESANG:
		pIli9488->ILI9488_Draw_HLineW(Xbar, Ybar, LENTHbar, 4, bar_color);
		pIli9488->DrawFontTTF_Digital16(Xstr, TOP_MEASURE_POS_Y,  TFT_BLACK, bc, (uint8_t*)str[0].c_str(), 0);
		pIli9488->DrawFontTTF_Digital16(Xstr, TOP_MEASURE_POS_Y1, TFT_BLACK, bc, (uint8_t*)str[4].c_str(), 0);
		break;

	}
}

void O3_MAIN_PAGE::Draw_Home(TOP_MENU page)
{

	pIli9488->ILI9488_Fill_Screen(TFT_WHITE);
	OutLineAndFooter();
	//pIli9488->ili9488_DrawRGBImage_Flash(50, 50, 26, 26, (2048*17));
	//pIli9488->ili9488_DrawEEPROM_Icon(50, 50,(FLASH_ICON)IMG_off_comp);
	//pIli9488->ili9488_DrawRGBImage(0, 0, 80,80,IMG_img1);
	//pIli9488->ili9488_DrawRGBImage(0, 0 ,88,26,(uint8_t *)IMG_on_g_comp);
	//pIli9488->ili9488_DrawRGBImage(0, 0 ,88,26,(uint8_t *)IMG_off_comp);
	printf("+++++++Draw_Home[%d]+++++++++++\r\n", page);
	pDataClass->reg.sysFlag.b.CHECK_RELAY_ON=0;
	switch (page)
	{
	case TOP_COMP:
	case TOP_OZONE:
	case TOP_JESANG:
		Draw_TopMenu_Form(page);
		Update_Display_Seljung(page);
		Update_Display_measured(page);
		Update_Icon(page);
		pDataClass->SYSTEM_ChangeToNormal();
		break;
	case TOP_GRAPH1:
		Draw_History_title(0);
		draw_graphFrame(GS_Y_START);
		draw_RecVector(GS_Y_START);
		pDataClass->SYSTEM_ChangeToNormal();
		break;
	case TOP_FORCE_JAESANG:
		Draw_TopMenu_Form(page);
		Update_Icon(page);
		break;
	}

}

void O3_MAIN_PAGE::draw_RecVector(int y)
{
	printf("draw_RecVector[%d]\r\n", pDataClass->vRec.size());
	int i, sz;
	int Xoffset;
	int Yoffset;
	int Xpos, YTpos, YOpos, YHpos, xdw, ydw;
	int ex_Xpos = 0, ex_YTpos = 0, ex_YOpos = 0, ex_YHpos = 0;
	uint32_t ltime, cur_ltime = 0;
	float temperature;
	uint16_t o3;
	uint16_t humidity;
	uint8_t power_on;
	RECORD_UNIT uValue;
	uint32_t time_gap;

	xdw = 9;
	ydw = 8;
	Xoffset = GS_X_START + GS_WIDTH;
	Yoffset = y + GS_HEIGHT - 24;

	sz = pDataClass->vRec.size();
	cur_ltime= pDataClass->nowTimeInfo.ltime;//현재시간

	for (i = sz - 1; i > 0; i--)
	{
		uValue = pDataClass->vRec[i];
		ltime = uValue.ltime;
		power_on = uValue.power_on;

		//printf("(1)ltime_ref[%d] ltime[%d] ltime_xxx[%d] power_on[%x]\r\n",cur_ltime, ltime,cur_ltime-(3600*24), power_on);
		if(ltime > cur_ltime || ltime < (cur_ltime-(3600*24)) || power_on !=0xAA) continue;
		//printf("(2)ltime_ref[%d] ltime[%d] ltime_xxx[%d] power_on[%x]\r\n",cur_ltime, ltime,cur_ltime-(3600*24), power_on);
		temperature = (float) uValue.temperature / 10;
		if (temperature > GS_DISPLAY_MAX_TEMP)
			temperature = GS_DISPLAY_MAX_TEMP;

		o3 = uValue.o3;
		humidity=uValue.humidity;

		if (o3 > GS_DISPLAY_MAX_O3)	o3 = GS_DISPLAY_MAX_O3;	//0.9ppm
		time_gap = cur_ltime-ltime;
		Xpos = Xoffset -((432*time_gap)/(24*60*60));//24(30분간격으로)h history  9x48=432
		if(Xpos<0 || Xpos>480)continue;
		YTpos = Yoffset - ((temperature + 5) * ydw);
		YOpos = Yoffset - ((10 * o3 * ydw) / 500L);
		YHpos = Yoffset - ((10 * humidity * ydw) / 500L);

		if (Xpos >= GS_X_START)
		{
			pIli9488->Draw_Hollow_Circle(Xpos, YTpos, 2, GRAPH_TMP_COLOR);
			pIli9488->Draw_Hollow_Circle(Xpos, YOpos, 2, GRAPH_O3_COLOR);
			pIli9488->Draw_Hollow_Circle(Xpos, YHpos, 2, TFT_GREEN);
			if (i != sz - 1)
			{
				if(ex_Xpos !=0){
					pIli9488->DrawLine(ex_Xpos, ex_YTpos, Xpos, YTpos,	GRAPH_TMP_COLOR);
					pIli9488->DrawLine(ex_Xpos, ex_YOpos, Xpos, YOpos,	GRAPH_O3_COLOR);
					pIli9488->DrawLine(ex_Xpos, ex_YHpos, Xpos, YHpos,	TFT_GREEN);
				}
			}
			ex_Xpos = Xpos;
			ex_YTpos = YTpos;
			ex_YOpos = YOpos;
			ex_YHpos = YHpos;
		}
		//printf("ltime_ref[%d] ltime[%d] Xpos[%d] YTpos[%d]\r\n",cur_ltime, ltime, Xpos, YTpos);
		//printf("[%d] draw_RecVector[%d/%d] time[%d] temp[%d] o3[%d]\r\n",power_on, i, sz, ltime, temperature, o3);
	}

}





void O3_MAIN_PAGE::NAVI_ChangeMode(NAVI_BIT navi, MENU_DRAW_EVENT draw_event, int16_t position)
{

	if(pIli9488->Ignore_Draw==1)return;

	switch (draw_event)
	{
	case KEY_HOME_MENU_EVENT: //top_mode change
		NAVI_Draw_TopMode(navi);
		break;
	case KEY_TOP_MODE_SETUP_NEXT_EVENT: //change sub
		Navi_Draw_Sub(navi);
		break;
	case KEY_TOP_MODE_ITEM_EVENT: //change sub
		Navi_Draw_Item(navi, navi.item, 1, 1);
		break;
	case KEY_ITEM_VALUE_UPDN_EVENT: //KEY_UP
		Navi_Draw_Item_ValueUpdate(navi, position);
		break;
	case KEY_SELJUNG_UPDN_EVENT: //SetTemperature
		Update_Display_Seljung((TOP_MENU) navi.top_title);
		break;
	case KEY_TOP_MENU_CHANGE_EVENT: //SetOzone
		//Update_Top_Title(navi);
		Draw_Home((TOP_MENU) navi.top_title);
		break;
	default:
		break;

	}
	pFlashClass->SPI_FLASH_UPDATE_SELJUNG();
}

void O3_MAIN_PAGE::NAVI_Draw_TopMode(NAVI_BIT navi)
{
	DRAW_MUTEX = 1;
	if (navi.top_mode==0) Draw_Home((TOP_MENU) navi.top_title);
	else Navi_Draw_Sub(navi);
	DRAW_MUTEX = 0;
}


void O3_MAIN_PAGE::Navi_Draw_Sub(NAVI_BIT navi)
{
	int i, sub, sub_menu_cnt;
	//char str[128] =	{ 0, };
	sub = navi.sub;
	sub_menu_cnt = UI_Info.sub_menu_cnt[sub];
	//draw sub title++
	pIli9488->ILI9341_Draw_Filled_Rectangle_Coord(0, 0, 480, 320, TFT_WHITE);
//draw sub title--
	pSTM32_IO->on_item_drawing = 1;
	pSTM32_IO->cancel_key = 0;

	for (i = 0; i < sub_menu_cnt; i++)
	{
		if (i == 0) Navi_Draw_Item(navi, 0, 1, 0);
		else Navi_Draw_Item(navi, i, 0, 0);
		if (pSTM32_IO->cancel_key)
		{
			pSTM32_IO->cancel_key = 0;
			return;
		}
	}
	pSTM32_IO->on_item_drawing = 0;

}
uint16_t O3_MAIN_PAGE::Navi_Item_Ypos(uint8_t item)
{
	uint16_t item_pos;
	uint16_t ypos = 0;
	item_pos = item;
	ypos = ITEM_START_Y + (item_pos * ITEM_HEIGHT);
	return ypos;
}

char* O3_MAIN_PAGE::MK_ON_OFF_STR(uint8_t sub, uint8_t item_str_pos, uint8_t on)
{
	static char val_str[64];
	if (on)sprintf(val_str, UI_Info.value_arry[sub].str[item_str_pos], "ON");
	else sprintf(val_str, UI_Info.value_arry[sub].str[item_str_pos], "OFF");
	return val_str;
}

char* O3_MAIN_PAGE::MK_OK_ERR_STR(uint8_t sub, uint8_t item_str_pos, uint8_t ok)
{
	static char val_str[64];
	if (ok)sprintf(val_str, UI_Info.value_arry[sub].str[item_str_pos], "OK");
	else sprintf(val_str, UI_Info.value_arry[sub].str[item_str_pos], "Error");
	return val_str;
}

char* O3_MAIN_PAGE::MK_OK_MODE_STR(uint8_t sub, uint8_t item_str_pos, uint8_t mode)
{
	static char val_str[64];
	if (mode)sprintf(val_str, UI_Info.value_arry[sub].str[item_str_pos], "센서");
	else sprintf(val_str, UI_Info.value_arry[sub].str[item_str_pos], "주기");
	return val_str;
}

ITEM_DISPLAY_STRING O3_MAIN_PAGE::Navi_MENU_Temprature(uint8_t item)
{
	ITEM_DISPLAY_STRING dis_str;
	uint8_t sub = 0;
	int A_item_str_pos;
	int B_item_str_pos;
	int C_item_str_pos;
	int sub_menu_cnt = UI_Info.sub_menu_cnt[sub];
	A_item_str_pos = (item > 0) ? item - 1 : sub_menu_cnt - 1; //
	B_item_str_pos = item;
	C_item_str_pos = ((item + 1) >= sub_menu_cnt) ? 0 : item + 1; //

	sprintf(dis_str.A_str, "%s",UI_Info.item_num_arry[sub].str[A_item_str_pos]);
	sprintf(dis_str.B_str, "%s",UI_Info.item_num_arry[sub].str[B_item_str_pos]);
	sprintf(dis_str.C_str, "%s",UI_Info.item_num_arry[sub].str[C_item_str_pos]);

#ifdef DBG_BUTTON
	//printf("item[%d] VALUE A[%s] B[%s] C[%s]\r\n",item, dis_str.B_str);
 	//printf("======>temp_diff[%d] [%.1f]\r\n",UI_Data.data_arry[sub].idata[0], temp_diff);
	//for(int i=0;i<8;i++)printf("[%d][%d]\r\n",i, UI_Data.data_arry[sub].idata[i]);
#endif
	switch (item)
	{
	case 0:	//title
		sprintf(dis_str.A_val, "%s",MK_ON_OFF_STR(sub, A_item_str_pos,UI_Data.data_arry[sub].idata[A_item_str_pos]));
		sprintf(dis_str.B_val, UI_Info.value_arry[sub].str[B_item_str_pos]);
		sprintf(dis_str.C_val, UI_Info.value_arry[sub].str[C_item_str_pos],	UI_Data.data_arry[sub].idata[C_item_str_pos]);
		break;
	case 1:	//comp delay   //온도편차
		//sprintf(dis_str.A_val, UI_Info.value_arry[sub].str[A_item_str_pos],	temp_conpensation);
		sprintf(dis_str.A_val, UI_Info.value_arry[sub].str[A_item_str_pos]);
		sprintf(dis_str.B_val, UI_Info.value_arry[sub].str[B_item_str_pos],	UI_Data.data_arry[sub].idata[B_item_str_pos]);
		sprintf(dis_str.C_val, UI_Info.value_arry[sub].str[C_item_str_pos],	UI_Data.data_arry[sub].idata[C_item_str_pos]);
//		sprintf(dis_str.A_val, UI_Info.value_arry[sub].str[A_item_str_pos]);
//		sprintf(dis_str.B_val, UI_Info.value_arry[sub].str[B_item_str_pos],	temp_diff);
//		sprintf(dis_str.C_val, UI_Info.value_arry[sub].str[C_item_str_pos],	temp_conpensation);
		break;
	case 2:	//comp_delay //온도보정
		sprintf(dis_str.A_val, UI_Info.value_arry[sub].str[A_item_str_pos],	UI_Data.data_arry[sub].idata[A_item_str_pos]);
		sprintf(dis_str.B_val, UI_Info.value_arry[sub].str[B_item_str_pos],	UI_Data.data_arry[sub].idata[B_item_str_pos]);
		sprintf(dis_str.C_val, "%s",MK_ON_OFF_STR(sub, C_item_str_pos,UI_Data.data_arry[sub].idata[C_item_str_pos]));
		break;
	case 3:	//콤프지연
		sprintf(dis_str.A_val, UI_Info.value_arry[sub].str[A_item_str_pos],	UI_Data.data_arry[sub].idata[A_item_str_pos]);
		sprintf(dis_str.B_val, "%s",MK_ON_OFF_STR(sub, B_item_str_pos,UI_Data.data_arry[sub].idata[B_item_str_pos]));
		sprintf(dis_str.C_val, "%s",MK_ON_OFF_STR(sub, C_item_str_pos,UI_Data.data_arry[sub].idata[C_item_str_pos]));
		break;
	case 4:	//에바팬지연
		sprintf(dis_str.A_val, "%s",MK_ON_OFF_STR(sub, A_item_str_pos,UI_Data.data_arry[sub].idata[A_item_str_pos]));
		sprintf(dis_str.B_val, "%s",MK_ON_OFF_STR(sub, B_item_str_pos,UI_Data.data_arry[sub].idata[B_item_str_pos]));
		sprintf(dis_str.C_val, "%s",MK_ON_OFF_STR(sub, C_item_str_pos,UI_Data.data_arry[sub].idata[C_item_str_pos]));
		break;
	case 5:	//콤프작동중에바팬
		sprintf(dis_str.A_val, "%s",MK_ON_OFF_STR(sub, A_item_str_pos,UI_Data.data_arry[sub].idata[A_item_str_pos]));
		sprintf(dis_str.B_val, "%s",MK_ON_OFF_STR(sub, B_item_str_pos,UI_Data.data_arry[sub].idata[B_item_str_pos]));
		sprintf(dis_str.C_val, UI_Info.value_arry[sub].str[C_item_str_pos]);
		break;
	case 6:	//이상지속시간
		break;
	case 7:	//고온경보
		break;
	case 8:	//저온경보
		break;

	}
#ifdef _DBG_BUTTON
 	printf("item[%d] ##POS A:%d B:%d C:%d\r\n",item, A_item_str_pos, B_item_str_pos, C_item_str_pos);
	printf("item[%d] ##STR A:%s B:%s C:%s\r\n",item, dis_str.A_str, dis_str.B_str, dis_str.C_str);
	printf("item[%d] VALUE A:%s B:%s C:%s\r\n",item, dis_str.A_val, dis_str.B_val, dis_str.C_val);
#endif
	return dis_str;
}

ITEM_DISPLAY_STRING O3_MAIN_PAGE::Navi_MENU_Defrog(uint8_t item)
{
	ITEM_DISPLAY_STRING dis_str;
	uint8_t sub = 1;
	int A_item_str_pos;
	int B_item_str_pos;
	int C_item_str_pos;
	int sub_menu_cnt = UI_Info.sub_menu_cnt[sub];
	A_item_str_pos = (item > 0) ? item - 1 : sub_menu_cnt - 1; //
	B_item_str_pos = item;
	C_item_str_pos = ((item + 1) >= sub_menu_cnt) ? 0 : item + 1; //

	sprintf(dis_str.A_str, "%s",UI_Info.item_num_arry[sub].str[A_item_str_pos]);
	sprintf(dis_str.B_str, "%s",UI_Info.item_num_arry[sub].str[B_item_str_pos]);
	sprintf(dis_str.C_str, "%s",UI_Info.item_num_arry[sub].str[C_item_str_pos]);

	switch (item)
	{
	case 0: //title
		sprintf(dis_str.A_val, "%s",MK_ON_OFF_STR(sub, A_item_str_pos,UI_Data.data_arry[sub].idata[A_item_str_pos]));
		sprintf(dis_str.B_val, UI_Info.value_arry[sub].str[B_item_str_pos],	UI_Data.data_arry[sub].idata[B_item_str_pos]);
		sprintf(dis_str.C_val, "%s",MK_OK_MODE_STR(sub, C_item_str_pos,	UI_Data.data_arry[sub].idata[C_item_str_pos]));
		break;
	case 1: //제상방법 str
		sprintf(dis_str.A_val, UI_Info.value_arry[sub].str[A_item_str_pos],	UI_Data.data_arry[sub].idata[A_item_str_pos]);
		sprintf(dis_str.B_val, "%s",MK_OK_MODE_STR(sub, B_item_str_pos,	UI_Data.data_arry[sub].idata[B_item_str_pos]));
		sprintf(dis_str.C_val, UI_Info.value_arry[sub].str[C_item_str_pos],	UI_Data.data_arry[sub].idata[C_item_str_pos]);
		break;
	case 2: //제상주기 int
		sprintf(dis_str.A_val, "%s",MK_OK_MODE_STR(sub, A_item_str_pos,	UI_Data.data_arry[sub].idata[A_item_str_pos]));
		sprintf(dis_str.B_val, UI_Info.value_arry[sub].str[B_item_str_pos],	UI_Data.data_arry[sub].idata[B_item_str_pos]);
		sprintf(dis_str.C_val, UI_Info.value_arry[sub].str[C_item_str_pos],	UI_Data.data_arry[sub].idata[C_item_str_pos]);
		break;
	case 3: //제상시간 int
		sprintf(dis_str.A_val, UI_Info.value_arry[sub].str[A_item_str_pos],	UI_Data.data_arry[sub].idata[A_item_str_pos]);
		sprintf(dis_str.B_val, UI_Info.value_arry[sub].str[B_item_str_pos],	UI_Data.data_arry[sub].idata[B_item_str_pos]);
		sprintf(dis_str.C_val, UI_Info.value_arry[sub].str[C_item_str_pos],	UI_Data.data_arry[sub].idata[C_item_str_pos]);
		break;
	case 4: //jaesang sensor measure period
		sprintf(dis_str.A_val, UI_Info.value_arry[sub].str[A_item_str_pos],	UI_Data.data_arry[sub].idata[A_item_str_pos]);
		sprintf(dis_str.B_val, UI_Info.value_arry[sub].str[B_item_str_pos],	UI_Data.data_arry[sub].idata[B_item_str_pos]);
		sprintf(dis_str.C_val, "%s", MK_ON_OFF_STR(sub, C_item_str_pos,	UI_Data.data_arry[sub].idata[C_item_str_pos]));
		break;
	case 5: //에바팬ON/OFF
		sprintf(dis_str.A_val, UI_Info.value_arry[sub].str[A_item_str_pos],	UI_Data.data_arry[sub].idata[A_item_str_pos]);
		sprintf(dis_str.B_val, "%s",MK_ON_OFF_STR(sub, B_item_str_pos,UI_Data.data_arry[sub].idata[B_item_str_pos]));
		sprintf(dis_str.C_val, UI_Info.value_arry[sub].str[C_item_str_pos],	UI_Data.data_arry[sub].idata[C_item_str_pos]);
		break;
	case 6: //에바팬지여(초)
		sprintf(dis_str.A_val, "%s",
				MK_ON_OFF_STR(sub, A_item_str_pos,
						UI_Data.data_arry[sub].idata[A_item_str_pos]));
		sprintf(dis_str.B_val, UI_Info.value_arry[sub].str[B_item_str_pos],
				UI_Data.data_arry[sub].idata[B_item_str_pos]);
		sprintf(dis_str.C_val, UI_Info.value_arry[sub].str[C_item_str_pos],
				UI_Data.data_arry[sub].idata[C_item_str_pos]);
		break;
	case 7: //콤프지연 (초)
		sprintf(dis_str.A_val, UI_Info.value_arry[sub].str[A_item_str_pos],
				UI_Data.data_arry[sub].idata[A_item_str_pos]);
		sprintf(dis_str.B_val, UI_Info.value_arry[sub].str[B_item_str_pos],
				UI_Data.data_arry[sub].idata[B_item_str_pos]);
		sprintf(dis_str.C_val, "%s",MK_ON_OFF_STR(sub, C_item_str_pos,UI_Data.data_arry[sub].idata[C_item_str_pos]));
		break;
	case 8: //on-off
		sprintf(dis_str.A_val, UI_Info.value_arry[sub].str[A_item_str_pos],	UI_Data.data_arry[sub].idata[A_item_str_pos]);
		sprintf(dis_str.B_val, "%s",MK_ON_OFF_STR(sub, B_item_str_pos,UI_Data.data_arry[sub].idata[B_item_str_pos]));
		sprintf(dis_str.C_val, UI_Info.value_arry[sub].str[C_item_str_pos],	UI_Data.data_arry[sub].idata[C_item_str_pos]);
		break;
	}

#ifdef _DBG_BUTTON
 	printf("item[%d] ##POS A:%d B:%d C:%d\r\n",item, A_item_str_pos, B_item_str_pos, C_item_str_pos);
	printf("item[%d] ##STR A:%s B:%s C:%s\r\n",item, dis_str.A_str, dis_str.B_str, dis_str.C_str);
	printf("item[%d] VALUE A:%s B:%s C:%s\r\n",item, dis_str.A_val, dis_str.B_val, dis_str.C_val);
#endif
	return dis_str;
}

ITEM_DISPLAY_STRING O3_MAIN_PAGE::Navi_MENU_Ozone(uint8_t item)
{

	ITEM_DISPLAY_STRING dis_str;
	uint8_t sub = 2;
	int A_item_str_pos;
	int B_item_str_pos;
	int C_item_str_pos;
	int sub_menu_cnt = UI_Info.sub_menu_cnt[sub];
	A_item_str_pos = (item > 0) ? item - 1 : sub_menu_cnt - 1; //
	B_item_str_pos = item;
	C_item_str_pos = ((item + 1) >= sub_menu_cnt) ? 0 : item + 1; //

	sprintf(dis_str.A_str, "%s",UI_Info.item_num_arry[sub].str[A_item_str_pos]);
	sprintf(dis_str.B_str, "%s",UI_Info.item_num_arry[sub].str[B_item_str_pos]);
	sprintf(dis_str.C_str, "%s",UI_Info.item_num_arry[sub].str[C_item_str_pos]);

	float o3_diff = (float) UI_Data.data_arry[sub].idata[4] / 1000;

	switch (item)
	{
	case 0: //title
		sprintf(dis_str.A_val, UI_Info.value_arry[sub].str[A_item_str_pos],	UI_Data.data_arry[sub].idata[A_item_str_pos]);
		sprintf(dis_str.B_val, UI_Info.value_arry[sub].str[B_item_str_pos],	UI_Data.data_arry[sub].idata[B_item_str_pos]);
		sprintf(dis_str.C_val, "%s",MK_OK_MODE_STR(sub, C_item_str_pos,	UI_Data.data_arry[sub].idata[C_item_str_pos]));
		break;
	case 1: //주기
		sprintf(dis_str.A_val, UI_Info.value_arry[sub].str[A_item_str_pos],	UI_Data.data_arry[sub].idata[A_item_str_pos]);
		sprintf(dis_str.B_val, "%s",MK_OK_MODE_STR(sub, B_item_str_pos,	UI_Data.data_arry[sub].idata[B_item_str_pos]));
		sprintf(dis_str.C_val, UI_Info.value_arry[sub].str[C_item_str_pos],	UI_Data.data_arry[sub].idata[C_item_str_pos]);
		break;
	case 2: //순환팬 int
		sprintf(dis_str.A_val, "%s",MK_OK_MODE_STR(sub, A_item_str_pos,	UI_Data.data_arry[sub].idata[A_item_str_pos]));
		sprintf(dis_str.B_val, UI_Info.value_arry[sub].str[B_item_str_pos],UI_Data.data_arry[sub].idata[B_item_str_pos]);
		sprintf(dis_str.C_val, UI_Info.value_arry[sub].str[C_item_str_pos],UI_Data.data_arry[sub].idata[C_item_str_pos]);
		break;
	case 3: //순환팬 int
		sprintf(dis_str.A_val, UI_Info.value_arry[sub].str[A_item_str_pos],	UI_Data.data_arry[sub].idata[A_item_str_pos]);
		sprintf(dis_str.B_val, UI_Info.value_arry[sub].str[B_item_str_pos],	UI_Data.data_arry[sub].idata[B_item_str_pos]);
		sprintf(dis_str.C_val, UI_Info.value_arry[sub].str[C_item_str_pos],	o3_diff);
		break;
	case 4: //편차 int o3 diff
		sprintf(dis_str.A_val, UI_Info.value_arry[sub].str[A_item_str_pos],	UI_Data.data_arry[sub].idata[A_item_str_pos]);
		sprintf(dis_str.B_val, UI_Info.value_arry[sub].str[B_item_str_pos],	o3_diff);
		sprintf(dis_str.C_val, UI_Info.value_arry[sub].str[C_item_str_pos],	UI_Data.data_arry[sub].idata[C_item_str_pos]);
		break;

	case 5: //주기 int
		sprintf(dis_str.A_val, UI_Info.value_arry[sub].str[A_item_str_pos],	o3_diff);
		sprintf(dis_str.B_val, UI_Info.value_arry[sub].str[B_item_str_pos],	UI_Data.data_arry[sub].idata[B_item_str_pos]);
		sprintf(dis_str.C_val, UI_Info.value_arry[sub].str[B_item_str_pos],	UI_Data.data_arry[sub].idata[C_item_str_pos]);
		break;
	case 6: //시간 int
		sprintf(dis_str.A_val, UI_Info.value_arry[sub].str[A_item_str_pos],	UI_Data.data_arry[sub].idata[A_item_str_pos]);
		sprintf(dis_str.B_val, UI_Info.value_arry[sub].str[B_item_str_pos],	UI_Data.data_arry[sub].idata[B_item_str_pos]);
		sprintf(dis_str.C_val, UI_Info.value_arry[sub].str[C_item_str_pos],	UI_Data.data_arry[sub].idata[C_item_str_pos]);
		break;

//	case 6:
//		sprintf(dis_str.A_er_val_str, UI_Info.value_arry[sub].str[ex_item_pos],
//				UI_Data.data_arry[sub].idata[ex_item_pos - 1]);
//		//-------------------------------------------------------------
//		sprintf(dis_str.B_val, UI_Info.value_arry[sub].str[item_str_pos],
//				UI_Data.data_arry[sub].idata[item]);
//		break;
	}
#ifdef _DBG_BUTTON
 	printf("item[%d] ##POS A:%d B:%d C:%d\r\n",item, A_item_str_pos, B_item_str_pos, C_item_str_pos);
	printf("item[%d] ##STR A:%s B:%s C:%s\r\n",item, dis_str.A_str, dis_str.B_str, dis_str.C_str);
	printf("item[%d] VALUE A:%s B:%s C:%s\r\n",item, dis_str.A_val, dis_str.B_val, dis_str.C_val);
#endif
	return dis_str;
}

ITEM_DISPLAY_STRING O3_MAIN_PAGE::Navi_MENU_Status(uint8_t item)
{

	ITEM_DISPLAY_STRING dis_str;
	uint8_t sub = 3;
	//string telephone_num;
	int sub_menu_cnt = UI_Info.sub_menu_cnt[sub];
	int A_item_str_pos = (item > 0) ? item - 1 : sub_menu_cnt - 1; //
	int B_item_str_pos = item;
	int C_item_str_pos = ((item + 1) >= sub_menu_cnt) ? 0 : item + 1; //

	sprintf(dis_str.A_str, "%s",UI_Info.item_num_arry[sub].str[A_item_str_pos]);
	sprintf(dis_str.B_str, "%s",UI_Info.item_num_arry[sub].str[B_item_str_pos]);
	sprintf(dis_str.C_str, "%s",UI_Info.item_num_arry[sub].str[C_item_str_pos]);

	switch (item)
	{
	case 0:	//title
		sprintf(dis_str.A_val, "%s",MK_ON_OFF_STR(sub, A_item_str_pos,UI_Data.data_arry[sub].idata[A_item_str_pos]));
		sprintf(dis_str.B_val, UI_Info.value_arry[sub].str[B_item_str_pos],	UI_Data.data_arry[sub].idata[B_item_str_pos]);
		sprintf(dis_str.C_val, UI_Info.value_arry[sub].str[C_item_str_pos],	UI_Data.data_arry[sub].idata[C_item_str_pos]);
		break;
	case 1:	//이상 온도 경보 지속 시간 
		sprintf(dis_str.A_val, UI_Info.value_arry[sub].str[A_item_str_pos]);
		sprintf(dis_str.B_val, UI_Info.value_arry[sub].str[B_item_str_pos],	UI_Data.data_arry[sub].idata[B_item_str_pos]);
		sprintf(dis_str.C_val, UI_Info.value_arry[sub].str[C_item_str_pos],	UI_Data.data_arry[sub].idata[C_item_str_pos]);
		break;
	case 2:	//이상 온도 고온경보@ ~ +10
		sprintf(dis_str.A_val, UI_Info.value_arry[sub].str[A_item_str_pos],	UI_Data.data_arry[sub].idata[A_item_str_pos]);
		sprintf(dis_str.B_val, UI_Info.value_arry[sub].str[B_item_str_pos],	UI_Data.data_arry[sub].idata[B_item_str_pos]);
		sprintf(dis_str.C_val, UI_Info.value_arry[sub].str[C_item_str_pos],	UI_Data.data_arry[sub].idata[C_item_str_pos]);
		break;
	case 3: //이상온도 저온경보@ ~ -10
		sprintf(dis_str.A_val, UI_Info.value_arry[sub].str[A_item_str_pos],	UI_Data.data_arry[sub].idata[A_item_str_pos]);
		sprintf(dis_str.B_val, UI_Info.value_arry[sub].str[B_item_str_pos],	UI_Data.data_arry[sub].idata[B_item_str_pos]);
		sprintf(dis_str.C_val, UI_Info.value_arry[sub].str[C_item_str_pos],	(float)UI_Data.data_arry[sub].idata[C_item_str_pos]/10);
		break;
	case 4: //온도 편차@ ~ +10//(float) UI_Data.data_arry[sub].idata[1] / 10
		sprintf(dis_str.A_val, UI_Info.value_arry[sub].str[A_item_str_pos],	UI_Data.data_arry[sub].idata[A_item_str_pos]);
		sprintf(dis_str.B_val, UI_Info.value_arry[sub].str[B_item_str_pos],	(float)UI_Data.data_arry[sub].idata[B_item_str_pos]/10);
		sprintf(dis_str.C_val, UI_Info.value_arry[sub].str[C_item_str_pos],	(float)UI_Data.data_arry[sub].idata[C_item_str_pos]/10);
		break;
	case 5: //온도보정(-5 ~ +5)
		sprintf(dis_str.A_val, UI_Info.value_arry[sub].str[A_item_str_pos],	(float)UI_Data.data_arry[sub].idata[A_item_str_pos]/10);
		sprintf(dis_str.B_val, UI_Info.value_arry[sub].str[B_item_str_pos],	(float)UI_Data.data_arry[sub].idata[B_item_str_pos]/10);
		sprintf(dis_str.C_val, UI_Info.value_arry[sub].str[C_item_str_pos],	UI_Data.data_arry[sub].idata[C_item_str_pos]);
		break;
	case 6: //과냉방지(@설정)
		sprintf(dis_str.A_val, UI_Info.value_arry[sub].str[A_item_str_pos],	(float)UI_Data.data_arry[sub].idata[A_item_str_pos]/10);
		sprintf(dis_str.B_val, UI_Info.value_arry[sub].str[B_item_str_pos],	UI_Data.data_arry[sub].idata[B_item_str_pos]);
		sprintf(dis_str.C_val, "%s",MK_ON_OFF_STR(sub, C_item_str_pos,UI_Data.data_arry[sub].idata[C_item_str_pos]));
		break;
	case 7: //부저
		sprintf(dis_str.A_val, UI_Info.value_arry[sub].str[A_item_str_pos],	UI_Data.data_arry[sub].idata[A_item_str_pos]);
		sprintf(dis_str.B_val, "%s",MK_ON_OFF_STR(sub, B_item_str_pos,UI_Data.data_arry[sub].idata[B_item_str_pos]));
		sprintf(dis_str.C_val, UI_Info.value_arry[sub].str[C_item_str_pos],	UI_Data.data_arry[sub].idata[C_item_str_pos]);
		break;
	case 8: //max-wind
		sprintf(dis_str.A_val, "%s",MK_ON_OFF_STR(sub, A_item_str_pos,UI_Data.data_arry[sub].idata[A_item_str_pos]));
		sprintf(dis_str.B_val, UI_Info.value_arry[sub].str[B_item_str_pos],	UI_Data.data_arry[sub].idata[B_item_str_pos]);
		sprintf(dis_str.C_val, UI_Info.value_arry[sub].str[C_item_str_pos],	UI_Data.data_arry[sub].idata[C_item_str_pos]);
		break;

	}
#ifdef _DBG_BUTTON
 	printf("item[%d] ##POS A:%d B:%d C:%d\r\n",item, A_item_str_pos, B_item_str_pos, C_item_str_pos);
	printf("item[%d] ##STR A:%s B:%s C:%s\r\n",item, dis_str.A_str, dis_str.B_str, dis_str.C_str);
	printf("item[%d] VALUE A:%s B:%s C:%s\r\n",item, dis_str.A_val, dis_str.B_val, dis_str.C_val);
#endif
	return dis_str;
}

ITEM_DISPLAY_STRING O3_MAIN_PAGE::Navi_MENU_SystemCheck(uint8_t item)
{
	ITEM_DISPLAY_STRING dis_str;
	uint8_t sub = 4;
	int sub_menu_cnt = UI_Info.sub_menu_cnt[sub];
	int A_item_str_pos = (item > 0) ? item - 1 : sub_menu_cnt - 1; //
	int B_item_str_pos = item;
	int C_item_str_pos = ((item + 1) >= sub_menu_cnt) ? 0 : item + 1; //
	sprintf(dis_str.A_str, "%s",UI_Info.item_num_arry[sub].str[A_item_str_pos]);
	sprintf(dis_str.B_str, "%s",UI_Info.item_num_arry[sub].str[B_item_str_pos]);
	sprintf(dis_str.C_str, "%s",UI_Info.item_num_arry[sub].str[C_item_str_pos]);

	switch (item)
	{
	case 0: //title
		sprintf(dis_str.A_val, "%s",MK_ON_OFF_STR(sub, A_item_str_pos,	UI_Data.data_arry[sub].idata[A_item_str_pos]));
		sprintf(dis_str.B_val, UI_Info.value_arry[sub].str[B_item_str_pos],	UI_Data.data_arry[sub].idata[B_item_str_pos]);
		sprintf(dis_str.C_val, "%s",MK_ON_OFF_STR(sub, C_item_str_pos, UI_Data.data_arry[sub].idata[C_item_str_pos]));
		break;
	case 1: //sensor1
		sprintf(dis_str.A_val, UI_Info.value_arry[sub].str[A_item_str_pos],	UI_Data.data_arry[sub].idata[A_item_str_pos]);
		sprintf(dis_str.B_val, "%s",MK_ON_OFF_STR(sub, B_item_str_pos,UI_Data.data_arry[sub].idata[B_item_str_pos]));
		sprintf(dis_str.C_val, "%s",MK_ON_OFF_STR(sub, C_item_str_pos,UI_Data.data_arry[sub].idata[C_item_str_pos]));
		break;
	case 2: //sensor2
	case 3: //humidty
	case 4: //o3
	case 5: //wind
	case 6: //hp/lp
	case 7: //tel
		sprintf(dis_str.A_val, "%s",MK_ON_OFF_STR(sub, A_item_str_pos,UI_Data.data_arry[sub].idata[A_item_str_pos]));
		sprintf(dis_str.B_val, "%s",MK_ON_OFF_STR(sub, B_item_str_pos,UI_Data.data_arry[sub].idata[B_item_str_pos]));
		sprintf(dis_str.C_val, "%s",MK_ON_OFF_STR(sub, C_item_str_pos,UI_Data.data_arry[sub].idata[C_item_str_pos]));
		break;
	case 8: //bz
		sprintf(dis_str.A_val, "%s",MK_ON_OFF_STR(sub, A_item_str_pos,UI_Data.data_arry[sub].idata[A_item_str_pos]));
		sprintf(dis_str.B_val, "%s",MK_ON_OFF_STR(sub, B_item_str_pos,UI_Data.data_arry[sub].idata[B_item_str_pos]));
		sprintf(dis_str.C_val, UI_Info.value_arry[sub].str[C_item_str_pos],	UI_Data.data_arry[sub].idata[C_item_str_pos]);
		break;

	}
	return dis_str;
}

ITEM_DISPLAY_STRING O3_MAIN_PAGE::Navi_MENU_TimeSet(uint8_t item)
{

	ITEM_DISPLAY_STRING dis_str;
	struct tm timeinfo;
	uint8_t sub = 5;
	int sub_menu_cnt = UI_Info.sub_menu_cnt[sub];
	int A_item_str_pos = (item > 0) ? item - 1 : sub_menu_cnt - 1; //
	int B_item_str_pos = item;
	int C_item_str_pos = ((item + 1) >= sub_menu_cnt) ? 0 : item + 1; //

	sprintf(dis_str.A_str, "%s",UI_Info.item_num_arry[sub].str[A_item_str_pos]);
	sprintf(dis_str.B_str, "%s",UI_Info.item_num_arry[sub].str[B_item_str_pos]);
	sprintf(dis_str.C_str, "%s",UI_Info.item_num_arry[sub].str[C_item_str_pos]);

	//timeinfo=pDataClass->current_local_timeinfo;
	int year, month, day, hour, minute, keep;
	//char *lRevision="2022-01-01 00:00";

	year = UI_Data.data_arry[sub].idata[1];
	timeinfo.tm_year = year;

	month = UI_Data.data_arry[sub].idata[2];
	timeinfo.tm_mon = month;

	day = UI_Data.data_arry[sub].idata[3];
	timeinfo.tm_mday = day;

	hour = UI_Data.data_arry[sub].idata[4];
	hour = (hour % 24); //0-23
	timeinfo.tm_hour = hour;

	minute = UI_Data.data_arry[sub].idata[5];
	minute = (minute % 60); //0-59
	timeinfo.tm_min = minute;

	timeinfo.tm_sec = 0;

	//keep = UI_Data.data_arry[5].idata[6];
	//keep = (keep % 100); //0-99

	//max_wind = UI_Data.data_arry[5].idata[8];

	//printf("====>Navi_MENU_TimeSet year{%d}\r\n", year);
	//printf("====>Navi_MENU_TimeSet build_date{%s}\r\n", UI_Data.sys_info.build_date);
//////
//	pHYM8563->Set_RTC(timeinfo);


	switch (item)
	{
	case 0: //title
		//sprintf(dis_str.A_val, UI_Info.value_arry[sub].str[A_item_str_pos],	max_wind);
		sprintf(dis_str.A_str, "%s%s",	UI_Info.item_num_arry[sub].str[A_item_str_pos], UI_Data.sys_info.build_date); sprintf(dis_str.A_val, "%s", "");
		sprintf(dis_str.B_val, UI_Info.value_arry[sub].str[B_item_str_pos]);
		sprintf(dis_str.C_val, UI_Info.value_arry[sub].str[C_item_str_pos],	year);
		break;
	case 1: //year
		sprintf(dis_str.A_val, UI_Info.value_arry[sub].str[A_item_str_pos]);
		sprintf(dis_str.B_val, UI_Info.value_arry[sub].str[B_item_str_pos],	year);
		sprintf(dis_str.C_val, UI_Info.value_arry[sub].str[C_item_str_pos],	month);

		break;
	case 2: //month
		sprintf(dis_str.A_val, UI_Info.value_arry[sub].str[A_item_str_pos],	year);
		sprintf(dis_str.B_val, UI_Info.value_arry[sub].str[B_item_str_pos],	month);
		sprintf(dis_str.C_val, UI_Info.value_arry[sub].str[C_item_str_pos],	day);
		break;
	case 3: //day
		sprintf(dis_str.A_val, UI_Info.value_arry[sub].str[A_item_str_pos],	month);
		sprintf(dis_str.B_val, UI_Info.value_arry[sub].str[B_item_str_pos],	day);
		sprintf(dis_str.C_val, UI_Info.value_arry[sub].str[C_item_str_pos],	hour);
		break;
	case 4: //hour
		sprintf(dis_str.A_val, UI_Info.value_arry[sub].str[A_item_str_pos],	day);
		sprintf(dis_str.B_val, UI_Info.value_arry[sub].str[B_item_str_pos],	hour);
		sprintf(dis_str.C_val, UI_Info.value_arry[sub].str[C_item_str_pos],	minute);
		break;
	case 5: //minitue
		sprintf(dis_str.A_val, UI_Info.value_arry[sub].str[A_item_str_pos],	hour);
		sprintf(dis_str.B_val, UI_Info.value_arry[sub].str[B_item_str_pos],	minute);
		sprintf(dis_str.C_val, "%s",MK_ON_OFF_STR(sub, C_item_str_pos,	UI_Data.data_arry[sub].idata[C_item_str_pos]));
		break;
	case 6: //설정OK
		sprintf(dis_str.A_val, UI_Info.value_arry[sub].str[A_item_str_pos],	minute);
		sprintf(dis_str.B_val, "%s",MK_ON_OFF_STR(sub, B_item_str_pos,	UI_Data.data_arry[sub].idata[B_item_str_pos]));
		sprintf(dis_str.C_str, "%s%s",	UI_Info.item_num_arry[sub].str[C_item_str_pos],(char*)pMODEM->modem_info.telephon_number.c_str()); sprintf(dis_str.C_val, "%s", "");
		break;
	case 7: //tel-num
		sprintf(dis_str.A_val, "%s",MK_ON_OFF_STR(sub, A_item_str_pos,	UI_Data.data_arry[sub].idata[A_item_str_pos]));
		sprintf(dis_str.B_str, "%s%s",	UI_Info.item_num_arry[sub].str[B_item_str_pos],(char*)pMODEM->modem_info.telephon_number.c_str()); sprintf(dis_str.B_val, "%s", "");
		sprintf(dis_str.C_str, "%s%s",	UI_Info.item_num_arry[sub].str[C_item_str_pos], UI_Data.sys_info.build_date); sprintf(dis_str.B_val, "%s", "");
		break;
	case 8: //build
		 sprintf(dis_str.A_str, "%s%s",	UI_Info.item_num_arry[sub].str[A_item_str_pos],(char*)pMODEM->modem_info.telephon_number.c_str()); sprintf(dis_str.A_val, "%s", "");
		 sprintf(dis_str.B_str, "%s%s",	UI_Info.item_num_arry[sub].str[B_item_str_pos], UI_Data.sys_info.build_date); sprintf(dis_str.B_val, "%s", "");
	 	 sprintf(dis_str.C_val, UI_Info.value_arry[sub].str[C_item_str_pos],	UI_Data.data_arry[sub].idata[C_item_str_pos]);
		break;
	}
	return dis_str;
}

void O3_MAIN_PAGE::Navi_Draw_Item(NAVI_BIT navi, uint8_t item, uint8_t selected,
		uint8_t renewal)
{
	ITEM_DISPLAY_STRING dis_str;
	uint8_t sub = navi.sub;

	int xpos = ITEM_START_X;
	int sub_menu_cnt = UI_Info.sub_menu_cnt[sub];
	int A_pos = (item > 0) ? item - 1 : sub_menu_cnt - 1; //
	int B_pos = item;
	int C_pos = ((item + 1) >= sub_menu_cnt) ? 0 : item + 1; //

	int A_Ypos = ITEM_START_Y + (A_pos * ITEM_HEIGHT);
	int B_Ypos = ITEM_START_Y + (B_pos * ITEM_HEIGHT);
	int C_Ypos = ITEM_START_Y + (C_pos * ITEM_HEIGHT);

	//ex_item_pos = (item == 0) ? max_item_cnt : ex_item_str_pos;
	//ex_ypos = ITEM_START_Y + (ex_item_pos * ITEM_HEIGHT);
	//next_ypos = ITEM_START_Y + (ex_item_pos * ITEM_HEIGHT);

	//printf(">>>Navi_Draw_Item sub[%d] item[%d] max_item_cnt[%d]\r\n",sub, navi.item, max_item_cnt);
	MENU_PAGE page = (MENU_PAGE) sub;
	switch (page)
	{
	case COMP_PAGE:   //냉장시스템제어
		dis_str = Navi_MENU_Temprature(item);
		break;
	case JAESANG_PAGE:   //제상제어
		dis_str = Navi_MENU_Defrog(item);
		break;
	case OZONE_PAGE:   //오존제어
		dis_str = Navi_MENU_Ozone(item);
		break;
	case SELJUNG_PAGE:
		dis_str = Navi_MENU_Status(item);
		break;
	case CHECK_PAGE:
		dis_str = Navi_MENU_SystemCheck(item);
		break;
	case CLOCK_PAGE:
		dis_str = Navi_MENU_TimeSet(item);
		break;
	}
	if (selected)
	{
		if (renewal)
		{
			//이전거 지우기
			pIli9488->DrawFontTTF_Digital16(xpos, A_Ypos, TFT_BLACK, ITEM_BG_COLOR,(uint8_t*) dis_str.A_str, 0);
			pIli9488->DrawFontTTF_Digital16(SET_VALUE_REF_X, A_Ypos, TFT_BLACK,ITEM_BG_COLOR, (uint8_t*) dis_str.A_val, 0);
			pIli9488->DrawFontTTF_Digital16(xpos, C_Ypos, TFT_BLACK, ITEM_BG_COLOR,(uint8_t*) dis_str.C_str, 0);
			pIli9488->DrawFontTTF_Digital16(SET_VALUE_REF_X, C_Ypos, TFT_BLACK,ITEM_BG_COLOR, (uint8_t*) dis_str.C_val, 0);
		}
		//현재거 쓰기
		pIli9488->DrawFontTTF_Digital16(xpos, B_Ypos, ITEM_SELECTED_COLOR,ITEM_BG_COLOR, (uint8_t*) dis_str.B_str, 0);
		pIli9488->DrawFontTTF_Digital16(SET_VALUE_REF_X, B_Ypos, ITEM_SELECTED_COLOR,	ITEM_BG_COLOR, (uint8_t*) dis_str.B_val, 0);
	}
	else
	{
		pIli9488->DrawFontTTF_Digital16(xpos, B_Ypos, TFT_BLACK, ITEM_BG_COLOR,(uint8_t*) dis_str.B_str, 0);
		pIli9488->DrawFontTTF_Digital16(SET_VALUE_REF_X, B_Ypos, TFT_BLACK,ITEM_BG_COLOR, (uint8_t*) dis_str.B_val, 0);
	}
//DrawFontTTF16Asc
}

void O3_MAIN_PAGE::Navi_Draw_Item_ValueUpdate(NAVI_BIT navi, int16_t position)
{

	uint8_t sub, item;
	int value;
	ITEM_DISPLAY_STRING dis_str;
	int ypos;
	sub = navi.sub;
	item = navi.item;   //타이틀 건너뛰고
	ypos = Navi_Item_Ypos(item);   //item 0 --> 1에 그림

	value = UI_Data.data_arry[sub].idata[item];

	MENU_PAGE page = (MENU_PAGE) sub;

	printf("(xxxx)>>>Navi_Draw_Item sub[%d] item[%d] value[%d] position[%d]\r\n",	sub, item, value, position);
	switch (page)
	{
	case COMP_PAGE: //system
		switch (item)
		{
		case 0:
			break;
		case 1: //comp delay
			value += position;
			if (value < 0)value = 0;
			else if (value > MAX_COMP_DELAY)value = MAX_COMP_DELAY;
			UI_Data.data_arry[COMP_PAGE].idata[item] = value;
//			value += position;
//			if (value < 0)	value = 0;
//			else if (value > MAXIUM_TEMP_DIFF)
//				value = MAXIUM_TEMP_DIFF;
//			UI_Data.data_arry[sub].idata[item] = value;
//			printf(	"(2)>>>Navi_Draw_Item sub[%d] item[%d] value[%d] position[%d]\r\n",	sub, item, value, position);

			break;
		case 2:	//에바팬딜레이
			value += position;
			if (value < 0)	value = 0;
			else if (value > MAX_EVA_FAN_DELAY)	value = MAX_EVA_FAN_DELAY;
			UI_Data.data_arry[COMP_PAGE].idata[item] = value;
//온도보정
//			value += position;
//			if (value < -50)value = -50;
//			else if (value > MAXIUM_TEMP_DIFF)	value = MAXIUM_TEMP_DIFF;
//			UI_Data.data_arry[sub].idata[item] = value;
			break;
		case 3:
			value=(position>0)? 1:0;
			UI_Data.data_arry[COMP_PAGE].idata[item] = value;
			break;

		case 4:
			//DP 작동상태 No Action
//			//에바팬딜레이
//			value += position;
//			if (value < 0)	value = 0;
//			else if (value > MAX_EVA_FAN_DELAY)	value = MAX_EVA_FAN_DELAY;
//			UI_Data.data_arry[sub].idata[item] = value;
			break;
		case 5:	//펌프다운 on/off
			value=(position>0)? 1:0;
			UI_Data.data_arry[COMP_PAGE].idata[item] = value;
			break;

		case 6:
//			value += position;
//			if (value < 0)	value = 0;
//			else if (value > MAX_TEMP_ALARM_JISOK)
//				value = MAX_TEMP_ALARM_JISOK;
//			UI_Data.data_arry[sub].idata[item] = value;
			break;

		case 7:
//			value += position;
//			if (value < 0)	value = 0;
//			else if (value > MAX_HI_TEMP_ALARM)
//				value = MAX_HI_TEMP_ALARM;
//			UI_Data.data_arry[sub].idata[item] = value;
			break;
		case 8:
//			value += position;
//
//			if (value > 0)
//				value = 0;
//			else if (value <= MAX_LO_TEMP_ALARM)
//				value = MAX_LO_TEMP_ALARM;
//			UI_Data.data_arry[sub].idata[item] = value;
			break;

		}
		dis_str = Navi_MENU_Temprature(item);
		break;

	case JAESANG_PAGE: //defrog
		switch (item)
		{
		case 0:
			break;
		case 1: //주기/기류/복합에서 -->주기/기류만
			value += position;
			value=(value>0)? 1: 0;//ON-OFF
			UI_Data.data_arry[JAESANG_PAGE].idata[item] = value;
			break;
		case 2:
			value += position;
			if (value < 0)
				value = 0;
			else if (value > MAX_DEF_PERIODE)
				value = MAX_DEF_PERIODE;
			UI_Data.data_arry[JAESANG_PAGE].idata[item] = value;
			break;
		case 3:
			value += position;
			if (value < 0)
				value = 0;
			else if (value > MAX_DEF_TIME)
				value = MAX_DEF_TIME;
			UI_Data.data_arry[JAESANG_PAGE].idata[item] = value;
			break;

		case 4://제상센서주기(1~99분)
			value += position;
			if (value < 1)	value = 1;
			else if (value > 99) value = 99;
			UI_Data.data_arry[JAESANG_PAGE].idata[item] = value;
			break;
		case 5:
			value += position;
			value=(value>0)? 1: 0;//ON-OFF
			UI_Data.data_arry[JAESANG_PAGE].idata[item] = value;
			break;

		case 6:
			value += position;
			if (value < 0)	value = 0;
			else if (value > MAX_EVA_FAN_DELAY_AF_DEF)
				value = MAX_EVA_FAN_DELAY_AF_DEF;
			UI_Data.data_arry[JAESANG_PAGE].idata[item] = value;
			break;

		case 7:
			value += position;
			if (value < 0)value = 0;
			else if (value > MAX_DEF_AFT_COMP_DELAY)
				value = MAX_DEF_AFT_COMP_DELAY;
			UI_Data.data_arry[JAESANG_PAGE].idata[item] = value;
			break;
		case 8:
			value=(position>0)? 1:0;
			UI_Data.data_arry[JAESANG_PAGE].idata[item] = value;
			break;

		}
		dis_str = Navi_MENU_Defrog(item);
		break;

	case OZONE_PAGE: //ozone ctrl
		switch (item)
		{
		case 0:
			break;
		case 1: //주기/편차/복합
			value=(position>0)? 1:0;
			UI_Data.data_arry[OZONE_PAGE].idata[item] = value;
			break;
		case 2: //오존발생전 순환팬
			value += position;
			if (value < 0)	value = 0;
			else if (value > MAX_O3_GEN_DELAY)	value = MAX_O3_GEN_DELAY;
			UI_Data.data_arry[OZONE_PAGE].idata[item] = value;
			break;
		case 3: //오존발생후 순환팬
			value += position;
			if (value < 0)	value = 0;
			else if (value > MAX_O3_FAN_DELAY)	value = MAX_O3_FAN_DELAY;
			UI_Data.data_arry[OZONE_PAGE].idata[item] = value;
			break;
		case 4: //오존 diff
			value += position * 10; //0.01 step
			value = (value / 10) * 10; //10단위로 정리
			if (value < 0)value = 0;
			else if (value > MAX_O3_DIFF)
			value = MAX_O3_DIFF;
			UI_Data.data_arry[OZONE_PAGE].idata[item] = value;
			break;
		case 5:
			value += position;
			if (value < 0) value = 0;
			else if (value > MAX_O3_GEN_PERIODE)
			value = MAX_O3_GEN_PERIODE;
			UI_Data.data_arry[OZONE_PAGE].idata[item] = value;
			break;
		case 6:
			value += position;
			if (value < 0) value = 0;
			else if (value > MAX_O3_GEN_TIME)
				value = MAX_O3_GEN_TIME;
			UI_Data.data_arry[OZONE_PAGE].idata[item] = value;
			break;

		}
		dis_str = Navi_MENU_Ozone(item);
		break;

	case SELJUNG_PAGE: //시스템상태
		switch (item) {
		case 0: break;
		case 1://지속시간
			value += position;
			if (value < 0)	value = 0;
			else if (value > MAX_TEMP_ALARM_JISOK)	value = MAX_TEMP_ALARM_JISOK;
			UI_Data.data_arry[sub].idata[item] = value;
			break;
		case 2://고온경보 (+)
			value += position;
			if (value < 0)	value = 0;
			else if (value > MAX_HI_TEMP_ALARM)
				value = MAX_HI_TEMP_ALARM;
			UI_Data.data_arry[sub].idata[item] = value;
			break;
		case 3://저온경보 (-)
			value += position;
			if (value > 0) value = 0;
			else if (value <= MAX_LO_TEMP_ALARM) value = MAX_LO_TEMP_ALARM;
			UI_Data.data_arry[sub].idata[item] = value;
			break;
		case 4://온도편차(히스테리시스)
			value += position;
			if (value < 0)	value = 0;
			else if (value > MAXIUM_TEMP_DIFF)	value = MAXIUM_TEMP_DIFF;
			UI_Data.data_arry[sub].idata[item] = value;
			//printf(	"온도편차(히스테리시스)>>>Navi_Draw_Item sub[%d] item[%d] value[%d] position[%d]\r\n",	sub, item, value, position);
			break;
		case 5:	//온도보정(+1)
			value += position;
			if (value < -50)value = -50;
			else if (value > MAXIUM_TEMP_DIFF)	value = MAXIUM_TEMP_DIFF;
			UI_Data.data_arry[sub].idata[item] = value;
			break;
		case 6: //과냉방지
			value += position;
			if (value < 0)	value = 0;
			else if (value > MAX_TEMP_KEEP)	value = MAX_TEMP_KEEP;
			UI_Data.data_arry[sub].idata[item] = value;
			break;
		case 7://부저
			value=(position>0)? 1:0;
			UI_Data.data_arry[sub].idata[item] = value;
			break;
		case 8://max-wind
			value += position;
			if (value < 0) value = 0;
			UI_Data.data_arry[sub].idata[item] = value;
			break;
		}
		dis_str = Navi_MENU_Status(item);
		break;

	case CHECK_PAGE:
		//printf("CHECK_PAGE [%d] position[%d]\r\n",UI_Data.data_arry[sub].idata[item], position);
		switch (item)
		{
		case 0:
			break;
		case 1:	case 2:	case 3:	case 4:	case 5:	case 6:	case 7:	case 8:
			value=(position>0)? 1:0;
			UI_Data.data_arry[sub].idata[item] = value;
			break;
		}
		dis_str = Navi_MENU_SystemCheck(item);
		break;
	case CLOCK_PAGE: //time set
		switch (item)
		{
		case 0:
			break;
		case 1:
			value += position;
			printf("(1)DATE_YEAR [%d]\r\n",value);
			//if (value < 0)	value = 2022;
			//else if (value > MAX_DATE_YEAR)	value = MAX_DATE_YEAR;

			UI_Data.data_arry[5].idata[1] = value;
			printf("(2)DATE_YEAR [%d]\r\n",UI_Data.data_arry[5].idata[1]);
			break;
		case 2:
			value += position;
			if (value < 1)	value = 1;
			else if (value > MAX_DATE_MONTH)
			value = MAX_DATE_MONTH;
			//value%=MAX_DATE_MONTH;//1-12
			UI_Data.data_arry[sub].idata[item] = value;
			break;
		case 3:
			value += position;
			if (value < 1)value = 1;
			else if (value > MAX_DATE_DAY)
				value = MAX_DATE_DAY;
			//value%=MAX_DATE_DAY;//1-31
			UI_Data.data_arry[sub].idata[item] = value;
			break;

		case 4:
			value += position;
			if (value < 0)value = 0;
			value %= MAX_TIME_HOUR; //0-23
			UI_Data.data_arry[sub].idata[item] = value;
			break;
		case 5:
			value += position;
			if (value < 0)value = 0;
			value %= MAX_TIME_MINUTE; //0-59
			UI_Data.data_arry[sub].idata[item] = value;
			break;
		case 6:
			value=(position>0)? 1:0;
			UI_Data.data_arry[sub].idata[item] = value;
			break;
		case 7:	break;
		case 8:	break;
		}
		dis_str = Navi_MENU_TimeSet(item);
		break;

	}
	pIli9488->ILI9341_Draw_Filled_Rectangle_Coord(SET_VALUE_REF_X, ypos, 480,	ypos + 30, ITEM_BG_COLOR);
	pIli9488->DrawFontTTF_Digital16(SET_VALUE_REF_X, ypos, ITEM_SELECTED_COLOR, ITEM_BG_COLOR, (uint8_t*) dis_str.B_val, 0);

}

void O3_MAIN_PAGE::Update_StateMessage(TOP_MENU top_menu,uint16_t bg_color){

	uint8_t string_id=0;
	uint16_t color=ALARM_STRING_NORMAL_COLOR;
	string str[7]={"정상 동작중","강제 제상중", "이상고온 오류","이상저온 오류","항온 동작중","온도센서이상","OPEN Door"};
	if(pDataClass->reg.sysFlag.b.TEMPERATURE_SENSOR_ERROR) string_id=5;
	else if(pDataClass->reg.sysFlag.b.CHANGGO_DOOR) string_id=6;
	else if(top_menu==TOP_FORCE_JAESANG)	string_id=1;
	else if(pDataClass->reg.sysFlag.b.HIGH_TEMPER_ALARM_ON) string_id=2;
	else if(pDataClass->reg.sysFlag.b.LOW_TEMPER_ALARM_ON) string_id=3;
	else if(pDataClass->reg.sysFlag.b.SUPER_COOL_PROTECT) string_id=4;



	switch(string_id){
		case 0:
			color=(pDataClass->reg_alarm[0].toggle) ? ALARM_STRING_NORMAL_COLOR: TFT_YELLOW;
			break;
		case 1://강제제상
			color=(pDataClass->reg_alarm[0].toggle) ? ALARM_STRING_NORMAL_COLOR: TFT_YELLOW;
			break;
		case 2:
			color=(pDataClass->reg_alarm[0].toggle) ? ALARM_STRING_NORMAL_COLOR: TFT_RED;
			break;
		case 3:
			color=(pDataClass->reg_alarm[0].toggle) ? ALARM_STRING_NORMAL_COLOR: TFT_BLUE;
			break;
		case 4:
		case 5:
			color=(pDataClass->reg_alarm[0].toggle) ? ALARM_STRING_NORMAL_COLOR: TFT_RED;
			break;
		case 6:
			color=(pDataClass->reg_alarm[0].toggle) ? ALARM_STRING_NORMAL_COLOR: TFT_RED;
			break;
	}
	Footer_Message((char*)str[string_id].c_str(), color);

}

//pIli9488->ili9488_DrawRGBImage(0,0,80,80,IMG_img1);
void O3_MAIN_PAGE::Update_Relay_Icon(uint16_t bg_color)
{
	uint8_t i;
	uint16_t Y=0;
	uint16_t X=ICON_START_X;
	uint16_t Xstr=ICON_START_X+30;
	string str[6]={"콤프","제상","에바팬","오존","오존팬","알람"};
	uint8_t temp_alarm = (pDataClass->reg.sysFlag.b.HIGH_TEMPER_ALARM_ON || pDataClass->reg.sysFlag.b.LOW_TEMPER_ALARM_ON) ? 1 : 0;
	uint8_t Tsensor_alarm = pDataClass->reg.sysFlag.b.TEMPERATURE_SENSOR_ERROR;
	for (i = 0; i < 5; i++)
	{
		Y=ICON_START_Y + (ICON_BETWIN_Y * i);

		if (pDataClass->reg.relay[i].on_count)
		{
			if (pDataClass->reg.relay[i].on){//지연후 ON
				pIli9488->DrawFontTTF_Digital16(Xstr,Y, MINJI_GREEN, TFT_WHITE,	(uint8_t*)str[i].c_str(), 0);
				pIli9488->ili9488_DrawEEPROM_Icon(X, Y,(FLASH_ICON)IMG_off_comp+i);
			}
			else
			{
				//지연후 OFF
				pIli9488->DrawFontTTF_Digital16(Xstr,Y, MINJI_GRAY, TFT_WHITE,	(uint8_t*)str[i].c_str(), 0);
				pIli9488->ili9488_DrawEEPROM_Icon(X, Y,(FLASH_ICON)IMG_on_g_comp+i);
			}
		}
		else //reg.relay[i].on_count==0 일때
		{
			if (pDataClass->reg.relay[i].on){
				//FIX ON
				pIli9488->DrawFontTTF_Digital16(Xstr,Y, MINJI_GREEN, TFT_WHITE,	(uint8_t*)str[i].c_str(), 0);
				pIli9488->ili9488_DrawEEPROM_Icon(X, Y,IMG_on_g_comp+i);
			}
			else{
				//FIX OFF
				pIli9488->DrawFontTTF_Digital16(Xstr,Y, MINJI_GRAY, TFT_WHITE,(uint8_t*)str[i].c_str(), 0);
				pIli9488->ili9488_DrawEEPROM_Icon(X, Y,IMG_off_comp+i);
			}
		}
	}
	//Alarm 표시
	if (temp_alarm || Tsensor_alarm){//온도에러 또는 센서 에러
		pDataClass->reg.sysFlag.b.ALARM_LED_ON=1;
		Y=ICON_START_Y + (ICON_BETWIN_Y * 5);
		pIli9488->ili9488_DrawEEPROM_Icon(X, Y,IMG_on_r_alarm);
		pIli9488->DrawFontTTF_Digital16(Xstr,Y, MINJI_RED, TFT_WHITE,	(uint8_t*)str[5].c_str(), 0);
	}
	else{
		pDataClass->reg.sysFlag.b.ALARM_LED_ON=0;
		Y=ICON_START_Y + (ICON_BETWIN_Y * 5);
		pIli9488->ili9488_DrawEEPROM_Icon(X, Y,IMG_off_alarm);
		pIli9488->DrawFontTTF_Digital16(Xstr,Y, MINJI_GRAY, TFT_WHITE,	(uint8_t*)str[5].c_str(), 0);
	}
}


void O3_MAIN_PAGE::Update_StateMark()
{
	uint16_t PosX[3]={400, 425, 450};
	uint16_t modem_state_color=TFT_LIGHTGREY;
	uint16_t ozone_mode_color =TFT_LIGHTGREY;
	uint16_t jaesang_mode_color =TFT_LIGHTGREY;
	switch(pMODEM->modem_process_seq){
		case MODEM_SEQ_IMEI:
			modem_state_color=TFT_LIGHTGREY; break;
		case MODEM_SEQ_TIME:
			modem_state_color=TFT_DARKGREY; break;
		case MODEM_SEQ_CONN:
			modem_state_color=TFT_YELLOW; break;
		case MODEM_SEQ_MQTT:
			modem_state_color=MINJI_GREEN; break;
		default: modem_state_color= TFT_LIGHTGREY; break;
	}

	ozone_mode_color=   (pDataClass->reg.local_ozone_ctrl_mode)   ? MINJI_GREEN:TFT_LIGHTGREY;
	jaesang_mode_color= (pDataClass->reg.local_jeasang_ctrl_mode) ? MINJI_GREEN:TFT_LIGHTGREY;

	pIli9488->Draw_Filled_Circle(PosX[0], 250, 5, ozone_mode_color);
	pIli9488->Draw_Filled_Circle(PosX[1], 250, 5, jaesang_mode_color);
	pIli9488->Draw_Filled_Circle(PosX[2], 250, 5, modem_state_color);
}

void O3_MAIN_PAGE::Update_Icon(TOP_MENU top_menu)
{
	Operating_bit.alarm = (pDataClass->reg.sysFlag.b.HIGH_TEMPER_ALARM_ON || pDataClass->reg.sysFlag.b.LOW_TEMPER_ALARM_ON) ? 1 : 0;
	uint16_t bg_color = FOOTER_BG_COLOR;

	switch (top_menu)
	{
		case TOP_COMP:
		case TOP_OZONE:
		case TOP_JESANG:
		case TOP_FORCE_JAESANG:
			Update_Relay_Icon(bg_color);//반완성그림
			Update_StateMessage(top_menu, bg_color);
			Update_StateMark();
			break;
		case TOP_GRAPH1:
			Update_StateMessage(top_menu, bg_color);
			break;
	}
}


void O3_MAIN_PAGE::Draw_Calrendar(uint16_t X, uint16_t Y)
{
	struct tm ltimeinfo;

	uint8_t lhour; //=timeinfo.tm_hour;
	char str_time[64];
	char str_date[64];
	uint8_t top_title = pSTM32_IO->navi_state.top_title;
	uint16_t bc = FOOTER_BG_COLOR;

	if (pDataClass->nowTimeInfo.ltime == 0)
		return;
	ltimeinfo = pDataClass->nowTimeInfo.timeinfo;

	lhour = ltimeinfo.tm_hour;
	sprintf(str_date, "%02d/%02d ", ltimeinfo.tm_mon, ltimeinfo.tm_mday);
	if (lhour >= 12)
	{
		lhour -= 12;
		sprintf(str_time, "PM %02d:%02d:%02d  ", lhour, ltimeinfo.tm_min,ltimeinfo.tm_sec);
	}
	else sprintf(str_time, "AM %02d:%02d:%02d  ", lhour, ltimeinfo.tm_min, ltimeinfo.tm_sec);
	pIli9488->DrawFontTTF_Digital16(X, Y, TFT_WHITE, bc, (uint8_t*) str_time,	0); //210,298

}

void O3_MAIN_PAGE::update_DrawMainMenu()
{
	if (pMain_page->DRAW_MUTEX)	return;
	if (pSTM32_IO->navi_state.top_mode == 0)
	{
		Update_Display_measured((TOP_MENU) pSTM32_IO->navi_state.top_title);
		Update_Icon((TOP_MENU) pSTM32_IO->navi_state.top_title);
		Draw_Calrendar(300, 280);
	}
}

void O3_MAIN_PAGE::get_cpuid() {
	pMODEM->modem_info.cpuid=format("%08X%08X%08X", Get_Unique32(0),Get_Unique32(1),Get_Unique32(2));//%
	printf(":%s\r\n", pMODEM->modem_info.cpuid.c_str());
	//printf("Get_Unique32 : %08X", Get_Unique32(0));
	//printf(":%08X", Get_Unique32(1));
	//printf(":%08X\r\n", Get_Unique32(2));
}
