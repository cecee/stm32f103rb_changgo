#ifndef O3_MAIN_PAGE_H
#define O3_MAIN_PAGE_H

#define Get_Unique8(x) ((x >= 0 && x < 12) ? (*(uint8_t *) (UID_BASE + (x))) : 0)
#define Get_Unique16(x) ((x >= 0 && x < 6) ? (*(uint16_t *) (UID_BASE + 2 * (x))) : 0)
#define Get_Unique32(x) ((x >= 0 && x < 3) ? (*(uint32_t *) (UID_BASE + 4 * (x))) : 0)

#define BG_COLOR TFT_BLUE
#define FOOTER_BG_COLOR 0x31C9
#define FOOTER_ROUND_COLOR 0xFD20

#define MINJI_GRAY  0x8c51
#define MINJI_RED   0xfa49
#define MINJI_GREEN 0x0ea0

//BOX DRAW
#define BOX_BG_COLOR  TFT_GREENYELLOW
#define BOX_BG_HIGHT  94
#define BOX_BG_TOP_MARGIN  17
#define BOX_BG_BOTTOM  BOX_BG_TOP_MARGIN + BOX_BG_HIGHT + 24

#define TEMP_BOX_X 24
#define TEMP_BOX_Y 17
#define O3_BOX_X   252
#define O3_BOX_Y   17
#define SET_VALUE_Y TEMP_BOX_Y+60
//GRAPH SHAPE
#define GRAPH_BG_COLOR TFT_WHITE //TFT_GREENYELLOW
#define GRAPH_FG_COLOR TFT_BLACK
#define GRAPH_TMP_COLOR TFT_RED
#define GRAPH_O3_COLOR  TFT_BLUE

#define GS_WIDTH   432
#define GS_HEIGHT  168
#define GS_X_GAP   GS_WIDTH/48   //9 48개 포인터
#define GS_X_START 24   //24
#define GS_Y_START 70   //24
//#define GS_Y_GAP   GS_HEIGHT/3   //54

#define GS_DISPLAY_MAX_TEMP 13   //13도
#define GS_DISPLAY_MAX_O3   900   //900ppb (0.9ppm)

//--------LED POSITION -------
#define ICON_START_Y 27
#define ICON_START_X 354

#define MENU_LED_X 430
#define ICON_BETWIN_Y 33
#define MENU_LED_RAD 16

#define OP_LED_X 40
#define OP_LED_Y 306
#define OP_LED_RAD 10
#define OP_LED_ON_COLOR  TFT_RED
#define OP_LED_OFF_COLOR TFT_GREENYELLOW

//-------MENU POSITION--------
#define ITEM_START_X 20
#define ITEM_TITLE_Y 12
#define ITEM_START_Y 20 //타이틀에서 이격
#define ITEM_HEIGHT 32

//--------TOP_MENU----------
#define TOP_MEASURE_POS_X  130
#define TOP_MEASURE_POS_Y  40 //55
#define TOP_MEASURE_POS_Y1 90 //55
#define TOP_SELJUNG_POS_X TOP_MEASURE_POS_X
#define TOP_SELJUNG_POS_Y 167

#define TOP_MENU_START_X  34
#define TOP_MENU_CURRENT_Y 84
#define TOP_MENU_SELJUNG_Y 214
#define TOP_MENU_CENTER_HLINE_X 38
#define TOP_MENU_CENTER_HLINE_Y 168
#define TOP_MENU_CENTER_HLINE_LENGTH 260

#define TOP_UINT_POS_X 250
//---------------------------
#define PERIOD_BEGIN_X TOP_MENU_START_X
#define PERIOD_BEGIN_Y TOP_MEASURE_POS_Y+70
//#define PERIOD_LENGTH 260

//-------ALARM STRING-------
#define ALARM_STRING_X  TOP_MENU_START_X
#define ALARM_STRING_Y  280
#define ALARM_STRING_NORMAL_COLOR    TFT_WHITE
//#define ALARM_STRING_ABNORMAL_COLOR  TFT_RED
//--------------------------

#define ITEM_SELECTED_COLOR TFT_RED
#define ITEM_BG_COLOR 		TFT_WHITE

#define SET_VALUE_REF_X  320

#define MAXIUM_TEMP_DIFF 50
#define MAX_COMP_DELAY   240
#define MAX_EVA_FAN_DELAY    60
#define MAX_TEMP_ALARM_JISOK  120
#define MAX_HI_TEMP_ALARM  10
#define MAX_LO_TEMP_ALARM  -20

#define MAX_DEF_PERIODE 480
#define MAX_DEF_TIME    30
#define MAX_EVA_FAN_DELAY_AF_DEF  600
#define MAX_DEF_AFT_COMP_DELAY  600

#define MAX_O3_GEN_DELAY  240
#define MAX_O3_FAN_DELAY  240
#define MAX_O3_DIFF  100
#define MAX_O3_GEN_PERIODE  120
#define MAX_O3_GEN_TIME  120

#define MAX_DATE_YEAR   2050
#define MAX_DATE_MONTH  12
#define MAX_DATE_DAY    31
#define MAX_TIME_HOUR   24
#define MAX_TIME_MINUTE 60
#define MAX_TEMP_KEEP   60

#define MAX_CONTROL_TEMP_H  500
#define MAX_CONTROL_TEMP_L  -100
#define MAX_CONTROL_OZEON_H 500
#define MAX_CONTROL_OZEON_L 10
#define MAX_CONTROL_WIND_H 90
#define MAX_CONTROL_WIND_L 10

enum FLASH_ICON
{
	IMG_on_r_comp=0,
	IMG_on_r_defrost,
	IMG_on_r_evafan,
	IMG_on_r_ozone,
	IMG_on_r_ozonefan,
	IMG_on_r_alarm,
	IMG_on_g_comp,
	IMG_on_g_defrost,
	IMG_on_g_evafan,
	IMG_on_g_ozone,
	IMG_on_g_ozonefan,
	IMG_on_g_alarm,
	IMG_off_comp, //12
	IMG_off_defrost,
	IMG_off_evafan,
	IMG_off_ozone,
	IMG_off_ozonefan,
	IMG_off_alarm,
};


class O3_MAIN_PAGE
{
  private:
  public:
	//uint8_t exTopMode=0;
	uint8_t DRAW_MUTEX=0;
	MENU_SET_DATA UI_Data;
	SYSTEM_RUNNING_BIT Operating_bit;

    O3_MAIN_PAGE();
    virtual ~O3_MAIN_PAGE();
    void init_console_value(void);
    void initPage(void);
    void Draw_History_title(uint8_t kind);
    void Draw_Home(TOP_MENU page);
    void Draw_TopMenu_Form(TOP_MENU page);
   // void Update_GangJaeJesang(void);

    void Update_Icon(TOP_MENU top_menu);
    void Update_Display_Seljung(TOP_MENU page);
    void Update_Display_measured(TOP_MENU top_menu);
    void draw_LED_MENU_STRING(TOP_MENU page);
    void Update_Display_period(uint16_t Xpos, uint16_t Ypos, uint16_t period, uint16_t duration, uint16_t current);
   // void Update_MeasureWindow(void);
    void draw_graphFrame(int y);
    void draw_RecVector(int y);
    //void Update_Display_ozone();
    //void Update_Display_SetOzone(uint8_t top_menu);
    void Update_Display_Set_UnderLine();
    void Footer_Message(char *msg, uint16_t color);
    void Update_StateMark();
    //void Update_Relay_LED(uint16_t bc_color);

    //void Draw_TemperatureBox(void);
    //void Draw_OzoneBox(void);
    void NAVI_ChangeMode(NAVI_BIT navi, MENU_DRAW_EVENT draw_event, int16_t position);
    void NAVI_Draw_TopMode(NAVI_BIT navi);
    void Navi_Draw_Sub(NAVI_BIT navi);
    void Navi_Draw_Item(NAVI_BIT navi, uint8_t item, uint8_t selected, uint8_t renewal);
    void Navi_Draw_Item_ValueUpdate(NAVI_BIT navi, int16_t position);
    uint16_t Navi_Item_Ypos(uint8_t item);
    char* MK_ON_OFF_STR(uint8_t sub, uint8_t item_str_pos,uint8_t on);
    char* MK_OK_ERR_STR(uint8_t sub, uint8_t item_str_pos,uint8_t ok);
    char* MK_OK_MODE_STR(uint8_t sub, uint8_t item_str_pos,uint8_t mode);
    ITEM_DISPLAY_STRING Navi_MENU_Temprature(uint8_t item);
    ITEM_DISPLAY_STRING Navi_MENU_Defrog(uint8_t item);
    ITEM_DISPLAY_STRING Navi_MENU_Ozone(uint8_t item);
    ITEM_DISPLAY_STRING Navi_MENU_Status(uint8_t item);
    ITEM_DISPLAY_STRING Navi_MENU_SystemCheck(uint8_t item);
    ITEM_DISPLAY_STRING Navi_MENU_TimeSet(uint8_t item);
    void Draw_Calrendar(uint16_t X, uint16_t Y);
    void update_DrawMainMenu();
    void Update_Relay_Icon(uint16_t bg_color);
    void OutLineAndFooter();
    void Footer_Round();
    void Update_StateMessage(TOP_MENU top_menu, uint16_t bg_color);
    void get_cpuid();
};


#endif
