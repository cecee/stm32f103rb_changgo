/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "iwdg.h"
#include "rtc.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "extern.h"

//#include "../../class/TEST_CLASS.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define xIMG_DOWN_FROM_PC
#define KEY_ACTION_DELAY 10 //1000ms delay after key
#define LEFT_KEY  1
#define RIGHT_KEY 0

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
#ifdef __cplusplus
extern "C" int _write(int32_t file, uint8_t *ptr, int32_t len)
{
#else
int _write(int32_t file, uint8_t *ptr, int32_t len) {
#endif
	if (HAL_UART_Transmit(&huart3, ptr, len, len) == HAL_OK)	return len;
	else return 0;
}

//int fputc(int ch, FILE *f)
//{
//	uint8_t temp[1]={ch};
//	HAL_UART_Transmit(&huart3, temp, 1, 2);
//	return(ch);
//}

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

uint8_t OneSecond_RTC_IRQ = 0;
int key_action_delay = 0;
//uint8_t rx_data;
uint8_t TOGGLE = 0;
extern uint8_t Encoder_Event;
extern eButtonEvent key;
void Main_IRQ_KEY_Process();

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

void delay_ms(uint16_t time)
{
	uint16_t i=0;
   while(time--)
   {
      i=12000;  // Define your own
      while(i--) ;
   }
}


void Main_IRQ_KEY_Process()
{
	eButtonEvent key;
	//int16_t count;
	key = (eButtonEvent) pSTM32_IO->Get_Key_Buffer();
	if(key==0) return;
	switch (key)
	{
	case NO_PRESS:
		break;
//	case SINGLE_PRESS:
//		//printf("[GET] SINGLE_PRESS \r\n");
//		//pKEY->Single_KeyProcess();
//		break;
//	case LONG_PRESS:
//		//printf("[GET] LONG_PRESS \r\n");
//		//pKEY->Long_KeyProcess();
//		break;
//	case DOUBLE_PRESS:
//		printf("[GET] DOUBLE_PRESS \r\n");
//		//pKEY->Double_KeyProcess();
//		break;
	case ADC_POWERX1_KEY:
		printf("[GET] ePOWERX1_KEY \r\n");
		//pKEY->Double_KeyProcess();
		if(pDataClass->reg.sysFlag.b.POWER_ON){
			pDataClass->reg.sysFlag.b.POWER_ON=0;
			pDataClass->SYSTEM_POWER(0);
		}
		else {
			pDataClass->reg.sysFlag.b.POWER_ON=1;
			pDataClass->SYSTEM_POWER(1);
		}
		break;
	case ADC_POWERX10_KEY:
		break;

	case ADC_MENUX1_KEY:
		printf("[GET] eMENU_KEY \r\n");
		pSTM32_IO->MENU_KeyProcess();
		break;
	case ADC_MENUX10_KEY:
		printf("[GET] eMENUX10_KEY \r\n");
		pSTM32_IO->Clear_Key_Buffer();
		pSTM32_IO->MENUX10_KeyProcess();
		break;
	case ADC_LEFTX1_KEY:
		printf("[GET] eLEFT_KEY \r\n");
		pSTM32_IO->LEFT_RIGHT_KeyProcess(LEFT_KEY,1);
		break;
	case ADC_LEFTX10_KEY:
		printf("[GET] eLEFT_KEY \r\n");
		pSTM32_IO->LEFT_RIGHT_KeyProcess(LEFT_KEY,10);
		break;
	case ADC_LEFTX100_KEY:
		printf("[GET] eLEFT_KEY \r\n");
		pSTM32_IO->LEFT_RIGHT_KeyProcess(LEFT_KEY,100);
		break;
	case ADC_RIGHTX1_KEY:
		printf("[GET] eRIGHTX1_KEY \r\n");
		pSTM32_IO->LEFT_RIGHT_KeyProcess(RIGHT_KEY,1);
		break;
	case ADC_RIGHTX10_KEY:
		printf("[GET] eRIGHTX10_KEY \r\n");
		pSTM32_IO->LEFT_RIGHT_KeyProcess(RIGHT_KEY,10);
		break;
	case ADC_RIGHTX100_KEY:
		printf("[GET] eRIGHTX100_KEY \r\n");
		pSTM32_IO->LEFT_RIGHT_KeyProcess(RIGHT_KEY,100);
		break;
	case ADC_UPX1_KEY:
		printf("[GET] eUPX1_KEY \r\n");
		pSTM32_IO->UPDN_KeyProcess(1, 1);
		break;
	case ADC_UPX10_KEY:
		printf("[GET] eUPX1_KEY \r\n");
		pSTM32_IO->UPDN_KeyProcess(1,10);
		break;
	case ADC_UPX100_KEY:
		printf("[GET] eUPX1_KEY \r\n");
		pSTM32_IO->UPDN_KeyProcess(1, 50);
		break;
	case ADC_DNX1_KEY:
		printf("[GET] eDNX1_KEY \r\n");
		pSTM32_IO->UPDN_KeyProcess(0, 1);
		break;
	case ADC_DNX10_KEY:
		printf("[GET] eDNX1_KEY \r\n");
		pSTM32_IO->UPDN_KeyProcess(0, 10);
		break;
	case ADC_DNX100_KEY:
		printf("[GET] eDNX1_KEY \r\n");
		pSTM32_IO->UPDN_KeyProcess(0, 50);
		break;
	case eJESANG_KEY:
		printf("[GET] eJESANG_KEY \r\n");
		pSTM32_IO->JESANG_KeyProcess();
		break;
	case MQ_JESANG_ON_KEY:
		printf("[GET] MQ_JESANG_ON_KEY \r\n");
		pSTM32_IO->JESANG_MQ_KeyProcess(1);
		break;
	case MQ_JESANG_OFF_KEY:
		printf("[GET] MQ_JESANG_OFF_KEY \r\n");
		pSTM32_IO->JESANG_MQ_KeyProcess(0);
		break;
	case MQ_LCD_ON_KEY:
		printf("[GET] MQ_LCD_ON_KEY \r\n");
		pDataClass->reg.sysFlag.b.POWER_ON=1;
		pDataClass->SYSTEM_POWER(1);
		break;
	case MQ_LCD_OFF_KEY:
		printf("[GET] MQ_LCD_OFF_KEY \r\n");
		pDataClass->reg.sysFlag.b.POWER_ON=0;
		pDataClass->SYSTEM_POWER(0);
		break;
	}
	key_action_delay = KEY_ACTION_DELAY;
	pSTM32_IO->Clear_Key_Buffer();

	key = NO_PRESS;
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ADC1_Init();
  MX_RTC_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  MX_USART3_UART_Init();
  MX_SPI1_Init();
  MX_TIM1_Init();
  MX_TIM3_Init();
  MX_IWDG_Init();
  /* USER CODE BEGIN 2 */

	pIli9488 = new ILI9488_DRV();
	pRS485 = new RS485();
	pFlashClass = new SPI1_FLASH();
	pMain_page = new O3_MAIN_PAGE();
	pSTM32_IO = new STM32_IO();
	pCOMP = new COMP_CTRL();
	pOZONE = new OZONE_CTRL();
	pIMG_DOWN_LOAD = new img_down_load();
	pDataClass = new DATA_CLASS();
	pJAESANG = new JAESANG();
	pMODEM = new MODEM();
	pRTC_API = new RTC_API();
	pHYM8563 = new I2C_BM8563();//rtc
	pEVAFAN = new EVAFAN();
	pQUEUE = new queue();

	//pSHT20 = new TEMP_I2C();
	//pSHT20->SHT20_init();

	printf("\r\n\r\n");
	printf("===========[START]=============\r\n");
	pIli9488->ILI9488_DRV_Init();
#ifndef IMG_DOWN_FROM_PC
	pFlashClass->SPI_FLASH_RETRIEVE_SELJUNG();
#endif
	pDataClass->Get_PushedRecData_FromEEPROM();
	//-----------
	//pRtc_api->Set_RTC(22,02,05,21,10,0);
	//-----------

	pMain_page->initPage();
	printf("==> Build : %s \r\n", pMain_page->UI_Data.sys_info.build_date);
	printf("==> MENU_SET_DATA [%d]\r\n", sizeof(MENU_SET_DATA));	//328byte
#ifndef IMG_DOWN_FROM_PC
	//pDataClass->reg.sysFlag.b.POWER_ON=1;
	pDataClass->SYSTEM_POWER(1);
	pDataClass->SET_CTRL_REGISTER(RELAY_ALL, 0, OFF);	//all relay off

	HAL_ADCEx_Calibration_Start(&hadc1);
	HAL_TIM_Base_Start_IT(&htim1);
	HAL_TIM_Base_Start_IT(&htim3);
	HAL_RTCEx_SetSecond_IT(&hrtc);
	pMODEM->modem_init();
	//__HAL_TIM_SET_COUNTER(&htim2, 0); //clear count
//	HAL_TIM_Encoder_Start_IT(&htim2, TIM_CHANNEL_ALL);
	__HAL_UART_ENABLE_IT(&huart1,UART_IT_RXNE);
	__HAL_UART_ENABLE_IT(&huart2,UART_IT_RXNE);

	// HAL_NVIC_EnableIRQ(USART2_IRQn);
	// HAL_UART_Receive_IT(&huart2, &rx_data, 1);
#endif
	__HAL_IWDG_START(&hiwdg);//40KHZ LCLK 128분주-3125 10�??????????????????????????????????????? (312.5*10)
	//HAL_ADC_Start_IT(&hadc1);
	//HAL_ADC_Start_DMA(&hadc1, (uint32_t *)ADC1Result, 7);
	pMain_page->get_cpuid();

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	while (1)
	{
		//printf("========================\r\n");	//328byte

		__HAL_IWDG_RELOAD_COUNTER(&hiwdg);
#ifdef IMG_DOWN_FROM_PC
		pIMG_DOWN_LOAD->img_copy_to_flash(0, 2048*18);//18�??????????????????????? ICON
		while(1);
#endif
		Main_IRQ_KEY_Process();

		if (OneSecond_RTC_IRQ)
		{
			OneSecond_RTC_IRQ = 0;
			if(key_action_delay==0) {
				pDataClass->OneSecond_prosess();
#ifdef DBG_ONE_SEC
			printf("~OneSecond_prosess\r\n");
#endif
			}

		}
//		if(pMODEM->modem_uart_process.b.mq_discon && pMODEM->modem_process_seq==3){
//			printf("(1)===MQTT SETTING===\r\n");
//			pMODEM->MQ_OPEN_URL();
//			HAL_Delay(300);
//			pMODEM->MQ_CONNECTION();
//			HAL_Delay(10000);
//			//pMODEM->MQ_SUBSCRIBE();
//			//HAL_Delay(2000);
//			printf("(2)===MQTT SETTING===\r\n");
//			//pMODEM->modem_uart_process.b.mq_discon=0;
//		}
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	}
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_ADC;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_HSE_DIV128;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV6;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */


/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	__disable_irq();
	while (1)
	{
	}
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

