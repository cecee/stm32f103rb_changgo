/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    stm32f1xx_it.c
  * @brief   Interrupt Service Routines.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_it.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "extern.h"
//#include "../../class/wind_sensor/wind_sensor.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define DEBOUNCE_TIMEOUT 5
#define BUZZER_TIMEOUT 10
#define LONG_PRESS_MIN 300 //300
#define DOUBLE_PRESS_MAX 50 //25
//#define BEEP_OFF() HAL_GPIO_WritePin(pBEEP_GPIO_Port, pBEEP_Pin, GPIO_PIN_RESET);
//#define BEEP_ON()  {buzzer_cnt=0;HAL_GPIO_WritePin(pBEEP_GPIO_Port, pBEEP_Pin, GPIO_PIN_SET);}
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
extern STM32_IO *pSTM32_IO;
//extern WIND_SENSOR *pWind;
extern DATA_CLASS *pDataClass;
extern ADC_HandleTypeDef hadc1;
extern int key_action_delay;

uint8_t uart1_rx_rs485[256];
uint8_t uart2_rx_modem[512];

uint8_t uart1_rx_cnt=0;//485
uint16_t uart2_rx_cnt=0;//modem

uint8_t uart1_rx_tmp=0;
uint8_t uart2_rx_tmp=0;

uint16_t Update_protect_cnt=0;


uint8_t TIM1_KeyEvent=0;
uint8_t Encoder_Event=0;

uint16_t encoder_read_tick=0;
int16_t encoder_old_position = 0;
int16_t encoder_difference=0;

//uint16_t buzzer_cnt=0;
uint16_t t1_ADC_key_tick=0;
eButtonEvent key;

uint8_t double_press_cnt=0;
uint16_t tick_all=0;
uint16_t debounce_cnt=0;
uint16_t key_tick_cnt[8];

uint8_t adcIndex=0;
uint16_t AdcValue[8];

PRESS_KEY KeyData;

bool is_key_start=false;
int indx = 0;
int speed =0;
/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
extern RTC_HandleTypeDef hrtc;
extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim3;
extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;
/* USER CODE BEGIN EV */
//void BEEP_ON(void);
//void BEEP_OFF(void);
//
//void BEEP_ON(void)  {buzzer_cnt=0; pDataClass->Buzzer=1; HAL_GPIO_WritePin(pBEEP_GPIO_Port, pBEEP_Pin, GPIO_PIN_SET);}
//void BEEP_OFF(void) {HAL_GPIO_WritePin(pBEEP_GPIO_Port, pBEEP_Pin, GPIO_PIN_RESET);}
void Get_AdcKeyValue(uint16_t adc0,  uint16_t adc1);
void Get_AdcData();
void Get_KeyDefine(PRESS_KEY KeyData);

/* USER CODE END EV */

/******************************************************************************/
/*           Cortex-M3 Processor Interruption and Exception Handlers          */
/******************************************************************************/
/**
  * @brief This function handles Non maskable interrupt.
  */
void NMI_Handler(void)
{
  /* USER CODE BEGIN NonMaskableInt_IRQn 0 */

  /* USER CODE END NonMaskableInt_IRQn 0 */
  /* USER CODE BEGIN NonMaskableInt_IRQn 1 */
  while (1)
  {
  }
  /* USER CODE END NonMaskableInt_IRQn 1 */
}

/**
  * @brief This function handles Hard fault interrupt.
  */
void HardFault_Handler(void)
{
  /* USER CODE BEGIN HardFault_IRQn 0 */

	printf("-------HardFault_Handler---------\r\n");
	//return;
  /* USER CODE END HardFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_HardFault_IRQn 0 */
    /* USER CODE END W1_HardFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Memory management fault.
  */
void MemManage_Handler(void)
{
  /* USER CODE BEGIN MemoryManagement_IRQn 0 */

  /* USER CODE END MemoryManagement_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_MemoryManagement_IRQn 0 */
    /* USER CODE END W1_MemoryManagement_IRQn 0 */
  }
}

/**
  * @brief This function handles Prefetch fault, memory access fault.
  */
void BusFault_Handler(void)
{
  /* USER CODE BEGIN BusFault_IRQn 0 */

  /* USER CODE END BusFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_BusFault_IRQn 0 */
    /* USER CODE END W1_BusFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Undefined instruction or illegal state.
  */
void UsageFault_Handler(void)
{
  /* USER CODE BEGIN UsageFault_IRQn 0 */

  /* USER CODE END UsageFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_UsageFault_IRQn 0 */
    /* USER CODE END W1_UsageFault_IRQn 0 */
  }
}

/**
  * @brief This function handles System service call via SWI instruction.
  */
void SVC_Handler(void)
{
  /* USER CODE BEGIN SVCall_IRQn 0 */

  /* USER CODE END SVCall_IRQn 0 */
  /* USER CODE BEGIN SVCall_IRQn 1 */

  /* USER CODE END SVCall_IRQn 1 */
}

/**
  * @brief This function handles Debug monitor.
  */
void DebugMon_Handler(void)
{
  /* USER CODE BEGIN DebugMonitor_IRQn 0 */

  /* USER CODE END DebugMonitor_IRQn 0 */
  /* USER CODE BEGIN DebugMonitor_IRQn 1 */

  /* USER CODE END DebugMonitor_IRQn 1 */
}

/**
  * @brief This function handles Pendable request for system service.
  */
void PendSV_Handler(void)
{
  /* USER CODE BEGIN PendSV_IRQn 0 */

  /* USER CODE END PendSV_IRQn 0 */
  /* USER CODE BEGIN PendSV_IRQn 1 */

  /* USER CODE END PendSV_IRQn 1 */
}

/**
  * @brief This function handles System tick timer.
  */
void SysTick_Handler(void)
{
  /* USER CODE BEGIN SysTick_IRQn 0 */

  /* USER CODE END SysTick_IRQn 0 */
  HAL_IncTick();
  /* USER CODE BEGIN SysTick_IRQn 1 */

  /* USER CODE END SysTick_IRQn 1 */
}

/******************************************************************************/
/* STM32F1xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f1xx.s).                    */
/******************************************************************************/

/**
  * @brief This function handles RTC global interrupt.
  */
void RTC_IRQHandler(void)
{
  /* USER CODE BEGIN RTC_IRQn 0 */

  /* USER CODE END RTC_IRQn 0 */
  HAL_RTCEx_RTCIRQHandler(&hrtc);
  /* USER CODE BEGIN RTC_IRQn 1 */
 //printf("--RTC_IRQHandler--\r\n");
  OneSecond_RTC_IRQ=1;
  /* USER CODE END RTC_IRQn 1 */
}

/**
  * @brief This function handles EXTI line[9:5] interrupts.
  */
void EXTI9_5_IRQHandler(void)
{
  /* USER CODE BEGIN EXTI9_5_IRQn 0 */
	// printf("EXTI9_5_IRQHandler~~ \r\n");
  /* USER CODE END EXTI9_5_IRQn 0 */
  HAL_GPIO_EXTI_IRQHandler(pKEY_MENU_Pin);
  /* USER CODE BEGIN EXTI9_5_IRQn 1 */

  /* USER CODE END EXTI9_5_IRQn 1 */
}

/**
  * @brief This function handles TIM1 update interrupt.
  */
void TIM1_UP_IRQHandler(void)
{
  /* USER CODE BEGIN TIM1_UP_IRQn 0 */

  /* USER CODE END TIM1_UP_IRQn 0 */
  HAL_TIM_IRQHandler(&htim1);
  /* USER CODE BEGIN TIM1_UP_IRQn 1 */

  /* USER CODE END TIM1_UP_IRQn 1 */
}

/**
  * @brief This function handles TIM3 global interrupt.
  */
void TIM3_IRQHandler(void)
{
  /* USER CODE BEGIN TIM3_IRQn 0 */

  /* USER CODE END TIM3_IRQn 0 */
  HAL_TIM_IRQHandler(&htim3);
  /* USER CODE BEGIN TIM3_IRQn 1 */

  /* USER CODE END TIM3_IRQn 1 */
}

/**
  * @brief This function handles USART1 global interrupt.
  */
void USART1_IRQHandler(void)
{
  /* USER CODE BEGIN USART1_IRQn 0 */

  /* USER CODE END USART1_IRQn 0 */
  HAL_UART_IRQHandler(&huart1);
  /* USER CODE BEGIN USART1_IRQn 1 */
  HAL_UART_Receive_IT(&huart1, &uart1_rx_tmp, 1);
  /* USER CODE END USART1_IRQn 1 */
}

/**
  * @brief This function handles USART2 global interrupt.
  */
void USART2_IRQHandler(void)
{
  /* USER CODE BEGIN USART2_IRQn 0 */
//modem
	uint8_t uart_data;
	uint32_t tmp_flag = 0, tmp_it_source = 0;
	tmp_flag =  __HAL_UART_GET_FLAG(&huart2,UART_FLAG_RXNE);
	tmp_it_source = __HAL_UART_GET_IT_SOURCE(&huart2, UART_IT_RXNE);

	if ((tmp_it_source != RESET) && (tmp_flag != RESET)){

		uart_data=huart2.Instance->DR;
		//printf("++ %x ++ \r\n", uart_data);
//		if(pMODEM->modem_uart_process.b.on_tx){
//			uart2_rx_modem[uart2_rx_cnt++]=uart_data;
//			if(uart2_rx_cnt>500){
//				//printf("uart2_rx_modem =>%s\r\n",uart2_rx_modem);
//				uart2_rx_cnt=500;
//			}
//		}
//		else
		//if(pMODEM->modem_uart_process.b.on_rx)
		{
			if(uart_data=='+'){
				uart2_rx_cnt=0;
				memset(&uart2_rx_modem, 0, sizeof(uart2_rx_modem));
				pMODEM->modem_uart_process.b.on_rx=1;
			}
			uart2_rx_modem[uart2_rx_cnt++]=uart_data;
		}

	}
	__HAL_UART_CLEAR_PEFLAG(&huart2);
	return;

  /* USER CODE END USART2_IRQn 0 */
  HAL_UART_IRQHandler(&huart2);
  /* USER CODE BEGIN USART2_IRQn 1 */
  //HAL_UART_Receive_IT(&huart2, &uart2_rx_tmp, 1);
 // printf("USART2_IRQHandler\r\n");
  /* USER CODE END USART2_IRQn 1 */
}

/* USER CODE BEGIN 1 */
//========================================================


void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){ //interrupt handler
	//For Debounce

	 printf("HAL_GPIO_EXTI_Callback~~ \r\n");

	debounce_cnt=0;
	for(;;){
		asm("NOP");
		if(debounce_cnt > DEBOUNCE_TIMEOUT )break;
	}
	//HAL_NVIC_DisableIRQ(EXTI15_10_IRQn);
    if(GPIO_Pin == pKEY_MENU_Pin){ //check interrupt for specific pin
            /* do down something */
    	//if(buzzer_cnt)return;
    	if(!HAL_GPIO_ReadPin(pKEY_MENU_GPIO_Port, pKEY_MENU_Pin)){
    		pSTM32_IO->BEEP_ON();
        	printf("#KEY_MENU DN~~tick_all[%d] is_key_start[%d] double_press_cnt[%d]\r\n",tick_all, is_key_start, double_press_cnt);
        	if(!is_key_start){
        		//printf("0\r\n");
        		is_key_start=true;
        		double_press_cnt=0;
        		tick_all=0;
        		memset(key_tick_cnt,0,sizeof(key_tick_cnt));
        	}
        	else{
        		//printf("1\r\n");
        		key_tick_cnt[double_press_cnt]=tick_all;
        		double_press_cnt++;

        	}
        }
    }
    //HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);
	//__enable_irq();
}
void Get_KeyDefine(PRESS_KEY KeyData){
	uint8_t key_value, lvalue;
	uint16_t press_cnt,x1_cnt, x10_cnt=0, x100_cnt=0;
	uint8_t beep=0;
	lvalue=KeyData.value.u8;
	press_cnt=KeyData.pressCnt;
	x1_cnt=x10_cnt=x100_cnt=press_cnt%10;
	if(press_cnt>200){
		x1_cnt=0;
		x10_cnt=0;
		//x100_cnt=press_cnt%10;
	}
	else if(press_cnt>100){
		x1_cnt=0;
		//x10_cnt=press_cnt%10;
		x100_cnt=0;
	}
	else {
		//x1_cnt=press_cnt%10;
		x10_cnt=0;
		x100_cnt=0;
	}

	if(press_cnt>0){
		 	 key_value=NO_PRESS;
			switch(lvalue){
			case 0x01://power //no repeat
				//if(press_cnt<2)	key_value=ePOWER_KEY;
				beep=1;
				if(x1_cnt==1)		key_value=ADC_POWERX1_KEY;
				else if(x10_cnt==1){
					//beep=1;
					key_value=ADC_POWERX10_KEY;
				}
				break;
			case 0x10://left
				beep=1;
				if(x1_cnt==1)		key_value=ADC_LEFTX1_KEY;
				else if(x10_cnt==1)	key_value=ADC_LEFTX10_KEY;
				else if(x100_cnt==1)key_value=ADC_LEFTX100_KEY;
				break;
			case 0x20://right
				beep=1;
				if(x1_cnt==1)		key_value=ADC_RIGHTX1_KEY;
				else if(x10_cnt==1)	key_value=ADC_RIGHTX10_KEY;
				else if(x100_cnt==1)key_value=ADC_RIGHTX100_KEY;
				break;
			case 0x04://up
				beep=1;
				if(x1_cnt==1)key_value=ADC_UPX1_KEY;
				else if(x10_cnt==1)	key_value=ADC_UPX10_KEY;
				else if(x100_cnt==1)key_value=ADC_UPX100_KEY;
				break;
			case 0x02://menu //no repeat
				beep=1;
				if(x1_cnt==1)	key_value=ADC_MENUX1_KEY;
				else if(x10_cnt==1)	key_value=ADC_MENUX10_KEY;
				break;
			case 0x08://dn
				beep=1;
				if(x1_cnt==1)key_value=ADC_DNX1_KEY;
				else if(x10_cnt==1)	key_value=ADC_DNX10_KEY;
				else if(x100_cnt==1)key_value=ADC_DNX100_KEY;
				break;

			}
			if(key_value!=NO_PRESS){
				if(beep)pSTM32_IO->BEEP_ON();
				//89pIli9488->Ignore_Draw=1;
				printf("Get_KeyDefine key_value[%d]\r\n", key_value);
				pSTM32_IO->vKEY_BUFFER.push_back(key_value);
			}
    	}
}

void Get_AdcKeyValue(uint16_t adc0,  uint16_t adc1)
{
	if(adc0>4000 && adc1>4000){
		//if(KeyData.exvalue.u8 !=0)printf("exvalue[%x][%04d]\r\n",KeyData.exvalue.u8, KeyData.pressCnt);
		memset(&KeyData, 0, sizeof(KeyData));
		return;
	}
	KeyData.pressCnt++;
	KeyData.value.u8=0;
	if(adc0<100){
		KeyData.value.b.power=1;
	}
	else if(adc0<2000){
		KeyData.value.b.left=1;
	}
	else if(adc0<4000){
		KeyData.value.b.up=1;
	}
	if(adc1<100){
		KeyData.value.b.menu=1;
	}
	else if(adc1<2000){
		KeyData.value.b.dn=1;
	}
	else if(adc1<4000){
		KeyData.value.b.right=1;
	}
	if(KeyData.exvalue.u8!=KeyData.value.u8) KeyData.pressCnt=1;
	KeyData.exvalue.u8=KeyData.value.u8;
	//printf("AdcValue[%x][%04d]\r\n",KeyData.value.u8, KeyData.pressCnt);
	Get_KeyDefine(KeyData);

}


void Get_AdcData()
{
//AIN1_0 KEY   AIN0
  HAL_ADC_Start(&hadc1);
  HAL_ADC_PollForConversion(&hadc1, 2);
  AdcValue[0]=HAL_ADC_GetValue(&hadc1);

 //AIN1_1 KEY //AIN1
  HAL_ADC_Start(&hadc1);
  HAL_ADC_PollForConversion(&hadc1, 2);
  AdcValue[1]=HAL_ADC_GetValue(&hadc1);

//AIN1_13 AIN13
  HAL_ADC_Start(&hadc1);
  HAL_ADC_PollForConversion(&hadc1, 2);
  AdcValue[2]=HAL_ADC_GetValue(&hadc1);
  //AIN1_14 //AIN14
  HAL_ADC_Start(&hadc1);
  HAL_ADC_PollForConversion(&hadc1, 2);
  AdcValue[3]=HAL_ADC_GetValue(&hadc1);
#if 0
  //AIN1_12 dummy//AIN14(TC2)
  HAL_ADC_Start(&hadc1);
  HAL_ADC_PollForConversion(&hadc1, 2);
  AdcValue[4]=HAL_ADC_GetValue(&hadc1);
  //AIN1_13 tc1  //  HAL_ADC_Start(&hadc1);
  HAL_ADC_Start(&hadc1);
  HAL_ADC_PollForConversion(&hadc1, 2);
  AdcValue[5]=HAL_ADC_GetValue(&hadc1);
 //AIN1_14 tc1  //  HAL_ADC_Start(&hadc1);
  HAL_ADC_Start(&hadc1);
  HAL_ADC_PollForConversion(&hadc1, 2);
  AdcValue[6]=HAL_ADC_GetValue(&hadc1);
#endif
  Get_AdcKeyValue(AdcValue[0], AdcValue[1]);
#ifdef DBG_ADC
 // printf("AdcValue[%04d][%04d][%04d][%04d][%04d][%04d][%04d]\r\n", AdcValue[0], AdcValue[1], AdcValue[2], AdcValue[3], AdcValue[4], AdcValue[6], AdcValue[6]);
#endif
}


void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  //printf("TIMER~~ \r\n");
	//uint8_t lvalue;
	//int press_cnt;
	if(htim->Instance==TIM1){//TIM1 10ms
    //HAL_GPIO_TogglePin(pOUT_TEST_GPIO_Port, pOUT_TEST_Pin);//test
    t1_ADC_key_tick++;
    tick_all++;
    pSTM32_IO->buzzer_cnt++;
    debounce_cnt++;
    if((pMODEM->modem_uart_process.b.on_rx)) pMODEM->modem_uart_process.uart_recv_try_cnt++;
    if(pMODEM->modem_uart_process.uart_recv_try_cnt> pMODEM->modem_uart_process.uart_recv_timeout){
    	//printf("@@@@@modem_rx:\r\n[%d]%s\r\n",pMODEM->modem_uart_process.b.on_rx, uart2_rx_modem);
    	uart2_rx_cnt=0;
    	pMODEM->modem_uart_process.uart_recv_try_cnt=0;
		if(pMODEM->modem_uart_process.b.on_rx) {
			pMODEM->modem_uart_process.b.on_rx=0;
			pMODEM->MqttSubscribe_parse((char *)uart2_rx_modem);
			//printf("@@@@@modem_rx:\r\n%s\r\n",uart2_rx_modem);
		}
		//memset(uart2_rx_data,0,sizeof(uart2_rx_data));
    }

    if(pDataClass->Buzzer && pSTM32_IO->buzzer_cnt>BUZZER_TIMEOUT){
    	pSTM32_IO->buzzer_cnt=0;
    	pDataClass->Buzzer=false;
    	pSTM32_IO->BEEP_OFF();
    }

    if(tick_all>10000)tick_all=10000;
    if( tick_all> LONG_PRESS_MIN && is_key_start){
    	//continue press
    	//printf("[%d][%d][%d][%d]\r\n", key_tick_cnt[0], key_tick_cnt[1], key_tick_cnt[2], key_tick_cnt[3]);
    	if(!HAL_GPIO_ReadPin(pKEY_MENU_GPIO_Port, pKEY_MENU_Pin)){
    		printf("---[LONG_PRESS]LONG_PRESS~~ tick_all->%d\r\n", tick_all);
    		pSTM32_IO->vKEY_BUFFER.push_back(LONG_PRESS);
    		//key=LONG_PRESS;
    		printf("---LONG_PRESS~~ \r\n");
    	}
    	is_key_start=false;
		double_press_cnt=0;
   		tick_all=0;
    }
    else if( tick_all> DOUBLE_PRESS_MAX && is_key_start){
    	//check release key
    	if(HAL_GPIO_ReadPin(pKEY_MENU_GPIO_Port, pKEY_MENU_Pin)){
    		//printf("DOUBLE_PRESS_MAX [%d][%d][%d][%d]\r\n", key_tick_cnt[0], key_tick_cnt[1], key_tick_cnt[2], key_tick_cnt[3]);
    		is_key_start=false;
    		tick_all=0;
    		if( key_tick_cnt[0]>0 && key_tick_cnt[0]>20){
    			pSTM32_IO->vKEY_BUFFER.push_back(DOUBLE_PRESS);
    			printf("---push DOUBLE_PRESS]~~ \r\n");
    		}
    		else {
    			pSTM32_IO->vKEY_BUFFER.push_back(eJESANG_KEY);
    			printf("---eJESANG_KEY_PRESS]~~ \r\n");
     		}
    		//press_tick=0;
    	}
    }

    if(t1_ADC_key_tick>3){
      encoder_read_tick++;
      t1_ADC_key_tick=0;
#ifdef xDBG_ADC
 // printf("AdcValue[%04d][%04d][%04d][%04d][%04d][%04d][%04d]\r\n", AdcValue[0], AdcValue[1], AdcValue[2], AdcValue[3], AdcValue[4], AdcValue[6], AdcValue[6]);
#endif
      Get_AdcData();
    }
  }
  if(htim->Instance==TIM3){//TIM2 100ms
	  HAL_GPIO_TogglePin(pOUT_TEST_GPIO_Port, pOUT_TEST_Pin);//test
	  // printf("key_action_delay[%04d]\r\n",key_action_delay);
	  if(key_action_delay>0) key_action_delay--;
  }
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	uint8_t uart1_rx_endptr=0;
	if(huart->Instance == USART1){
		//printf("rx1[%x][%x]\r\n",uart1_rx_cnt, uart1_rx_tmp);
		uart1_rx_rs485[uart1_rx_cnt++]=uart1_rx_tmp;
		if(uart1_rx_rs485[0]==0xFB && uart1_rx_cnt==2)uart1_rx_endptr=uart1_rx_rs485[1];
		if(uart1_rx_tmp=='@' && uart1_rx_cnt==0x0d){
			uart1_rx_cnt=0;
			pRS485->rs485_res();
		}
		if(uart1_rx_cnt>60)	uart1_rx_cnt=60;
	}
//	else if(huart->Instance == USART2){
//		//printf("%c",uart2_rx_tmp);
//		if(pMODEM->modem_uart_process.b.on_tx){
//			uart2_rx_modem[uart2_rx_cnt++]=uart2_rx_tmp;
//			if(uart2_rx_cnt>500){
//				printf("=>%s\r\n",uart2_rx_modem);
//				uart2_rx_cnt=500;
//			}
//		}
//		else{
//			if(uart2_rx_tmp=='+'){
//				uart2_rx_cnt=0;
//				pMODEM->modem_uart_process.b.on_rx=1;
//			}
//			uart2_rx_modem[uart2_rx_cnt++]=uart2_rx_tmp;
//		}
//
//	}
}


/* USER CODE END 1 */

