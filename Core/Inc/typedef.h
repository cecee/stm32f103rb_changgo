#ifndef TYPEDEF_H
#define TYPEDEF_H

#include "stm32f1xx_hal.h"
#include <vector>
#include <ctime>
#include <string>
#include <cstdarg>

using namespace std;

enum TOP_MENU{
	TOP_COMP=0,
	TOP_OZONE,
	TOP_JESANG,
	TOP_GRAPH1,
	TOP_FORCE_JAESANG,
};

enum ENUM_FUNCTION {
	NO_FUNCTION=0,
	DEFROG_HEATER_PERIODE_ON,
	DEFROG_HEATER_PERIODE_OFF,
	DEFROG_HEATER_WIND_ON,
	DEFROG_HEATER_WIND_OFF,
	COMP_VALUE_ON,
	COMP_VALUE_OFF,
	OZONE_PERIODE_ON,
	OZONE_PERIODE_OFF,
	OZONE_VALUE_ON,
	OZONE_VALUE_OFF,
};

enum RELAY {
	COMP_RY=0,
	HEATER_RY,
	EVAFAN_RY,
	O3GEN_RY,
	O3FAN_RY,
	PUMP_DOWN,
	RELAY_ALL,
};

enum MENU_PAGE {
	COMP_PAGE=0,
	JAESANG_PAGE,
	OZONE_PAGE,
	SELJUNG_PAGE,
	CHECK_PAGE,
	CLOCK_PAGE,
};


enum MENU_DRAW_EVENT {
	KEY_HOME_MENU_EVENT=0,
	KEY_SET_MODE_SETUP_NEXT_EVENT,//1
	KEY_TOP_MODE_SETUP_NEXT_EVENT,//2
	KEY_TOP_MODE_ITEM_EVENT,//3
	KEY_ITEM_VALUE_UPDN_EVENT,//4
	KEY_TOP_MODE_DN_EVENT,//5
	KEY_SELJUNG_UPDN_EVENT,
	KEY_TOP_MENU_CHANGE_EVENT,
	KEY_SET_MODE_DUMMY_EVENT,
	KEY_CANCEL_EVENT,
};

enum MODEM_PROCESS_STATE {
	MODEM_SEQ_INIT=0,
	MODEM_SEQ_IMEI,//1
	MODEM_SEQ_TIME,//2
	MODEM_SEQ_CONN,//3
	MODEM_SEQ_MQTT,//4
	MODEM_SEQ_FAILE,//
	MODEM_SEQ_POWERON,
	MODEM_SEQ_RESET,
};

typedef struct {
	uint8_t comp:1;
	uint8_t defrog:1;
	uint8_t eva_fan:1;
	uint8_t o3_gen:1;
	uint8_t alarm:1;
}SYSTEM_RUNNING_BIT;

typedef struct {
	uint8_t POWER_ON:1;
	uint8_t CHECK_RELAY_ON:1;
	uint8_t JAESANG_FORCE_ON:1;
	uint8_t JAESANG_EVENT_FLAG:1;
	uint8_t COMPRES_EVENT_FLAG:1;
	uint8_t OZONE_PERIOD_CTRL_ON:1;
	uint8_t OZONE_SENSOR_CTRL_ON:1;
	uint8_t HIGH_TEMPER_ALARM_ON:1;
	uint8_t LOW_TEMPER_ALARM_ON:1;
	uint8_t SUPER_COOL_PROTECT:1;//항온시작 창고외부 온도가 내부온도 보다 낮다.(바깥이 너무추워서 창고안 온도 올려야한다)
	uint8_t TEMPERATURE_SENSOR_ERROR:1;
	uint8_t OZONE_SENSOR_ERROR:1;
	uint8_t WIND_SENSOR_ERROR:1;
	uint8_t CHANGGO_DOOR:1;
	uint8_t DP_VALVE:1;
	uint8_t ALARM_LED_ON:1; //bit15
}FLAG_BIT;

typedef union _FLAG_STATE
{
  FLAG_BIT b;
  uint16_t u16;
}FLAG_STATE;


typedef struct {
	uint8_t r;
	uint8_t g;
	uint8_t b;
}color_t;

typedef struct {
	uint16_t ltime;
}_PERIOD;

typedef struct {
	_PERIOD period[2];
}SYSTEM_PERIOD;

typedef struct {
	int on_count :  12;
	uint8_t on : 1 ;
	uint8_t flag  : 1;//0: clear 1: on_flag 2: off_flag
	uint8_t toggle  : 1;//0: clear 1: on_flag 2: off_flag
}REGISTER;

typedef struct {
	uint8_t comp_on_flag;
	uint8_t comp_yendong_evafan;
	uint8_t evafan_on_flag;
	uint8_t windsensor_measure_flag;
	uint8_t decision_heater_on_flag;
	uint16_t evafan_period_cnt;
	int seljung_temp;
	uint16_t seljung_jaesang;

	int comp_on_ref;
	int comp_off_ref;
	int seljung_ozone;
	int seljung_wind;
	int seljung_buzzer;
	int comp_on_time;
	int comp_hysteresis;
	int comp_on_latency;
	int evafan_off_latency;
	int super_cool_protect;
	int super_cool_ref;
	int abnormal_elap_time;
	int abnormal_high_temp;
	int abnormal_low_temp;
	int temp_compensation;
	int measure_wind_avr;
	int jaesang_max_wind_reference;

	uint8_t jaesang_heater_on_flag;
	uint8_t jaesang_mode;
	uint8_t comp_on_at_jaesang;
	uint16_t jaesang_force_on_count;
	uint16_t jaesang_Current_State_TimeOut;
	uint16_t jaesang_total_peroid;
	uint16_t jaesang_on_duration;
	uint16_t jaesang_current;
	int jaesang_sensor_period;
	uint16_t jaesand_wind_start_measure_cnt;
	uint32_t before_jaesang_tick;

	uint8_t  ozone_gen_on;
	uint8_t  ozone_control_mode;
	int ozone_diff;
	uint16_t ozone_gen_on_delay;
	uint16_t ozone_fan_off_delay;
	uint16_t ozone_on_duration=0;
	uint16_t ozone_total_period=0;
	uint16_t ozone_start_time=0;
	uint16_t ozone_current_time=0;

}SYSTEM_PARAMETER;

typedef struct {
	SYSTEM_PARAMETER parm;
	SYSTEM_PERIOD period;
	FLAG_STATE sysFlag;
	REGISTER relay[6];
	uint8_t local_jeasang_ctrl_mode;
	uint8_t local_ozone_ctrl_mode;
}STATUS_REGISTER;




typedef struct {
  uint32_t ltime;
  int16_t temperature;
  uint16_t humidity;
  uint16_t o3;
  uint8_t power_on;
}RECORD_UNIT;

typedef struct {
  uint16_t crc;
  char build_date[28];
}SYSTEM_INFORMATION;

typedef struct {
  int temp;
  uint16_t o3;
  uint16_t wind;
}SYSTEM_TARGET;

typedef struct _RELAY_BIT
{
  uint8_t RY1:1;
  uint8_t RY2:1;
  uint8_t RY3:1;
  uint8_t RY4:1;
  uint8_t RY5:1;
  uint8_t RY6:1;
}RELAY_BIT;

typedef union _RELAY_DATA
{
	RELAY_BIT b;
	uint8_t u8;
}RELAY_DATA;

typedef struct _NAVI_BIT
{
    uint16_t  top_mode: 2;
    uint16_t  top_title: 4;
    uint16_t  sub: 4;
    uint16_t  item: 4;
    uint16_t  depth: 2;
}NAVI_BIT;

typedef struct
{
  int idata[10];
}MENU_DATA;

typedef struct
{
	SYSTEM_INFORMATION sys_info;
	SYSTEM_TARGET target;
	MENU_DATA data_arry[6];
}MENU_SET_DATA;

typedef struct
{
	char A_str[64];
	char A_val[64];
	char B_str[64];
	char B_val[64];
	char C_str[64];
	char C_val[64];
}ITEM_DISPLAY_STRING;

typedef struct
{
	struct tm timeinfo;
	uint32_t ltime;
	//uint32_t lsecOneDay;
}TYPEDEF_TIME_DATA;

typedef struct
{
	uint8_t exist;
	int16_t sub_sensor0;
	int16_t sub_sensor1;
	int16_t sub_sensor2;
}SENSOR_BOX;

// I2C acknowledge
typedef enum{
  ACK                      = 0,
  NO_ACK                   = 1,
}etI2cAck;

typedef enum{
  LOW                      = 0,
  HIGH                     = 1,
}etI2cLevel;

// Error codes
typedef enum{
  ACK_ERROR                = 0x01,
  TIME_OUT_ERROR           = 0x02,
  CHECKSUM_ERROR           = 0x04,
  UNIT_ERROR               = 0x08
}etError;

typedef struct
{
  int8_t hours;
  int8_t minutes;
  int8_t seconds;
} I2C_BM8563_TimeTypeDef;


typedef enum
{
    NO_PRESS=0,
    SINGLE_PRESS,
    LONG_PRESS,
    DOUBLE_PRESS,
	ADC_POWERX1_KEY,
	ADC_POWERX10_KEY,
	ADC_MENUX1_KEY,
	ADC_MENUX10_KEY,
	ADC_LEFTX1_KEY,
	ADC_LEFTX10_KEY,
	ADC_LEFTX100_KEY,
	ADC_RIGHTX1_KEY,
	ADC_RIGHTX10_KEY,
	ADC_RIGHTX100_KEY,
	ADC_UPX1_KEY,
	ADC_UPX10_KEY,
	ADC_UPX100_KEY,
	ADC_DNX1_KEY,
	ADC_DNX10_KEY,
	ADC_DNX100_KEY,
	eJESANG_KEY,
	MQ_JESANG_ON_KEY,
	MQ_JESANG_OFF_KEY,
	MQ_LCD_ON_KEY,
	MQ_LCD_OFF_KEY
} eButtonEvent;

typedef struct _KEY_BIT
{
    uint8_t  power: 1;
    uint8_t  menu: 1;
    uint8_t  up: 1;
    uint8_t  dn: 1;
    uint8_t  left: 1;
    uint8_t  right: 1;
    uint8_t  jesang: 1;
    uint8_t  xx: 1;
}KEY_BIT;

typedef union _KEY_DATA
{
  KEY_BIT b;
  uint8_t u8;
}KEY_DATA;

typedef struct _KEY_PAD
{
  KEY_DATA  value;
  KEY_DATA  exvalue;
  int  pressCnt;
}PRESS_KEY;

//missing string printf
//this is safe and convenient but not exactly efficient
inline std::string format(const char* fmt, ...){
    int size = 512;
    char* buffer = 0;
    buffer = new char[size];
    va_list vl;
    va_start(vl, fmt);
    int nsize = vsnprintf(buffer, size, fmt, vl);
    if(size<=nsize){ //fail delete buffer and try again
        delete[] buffer;
        buffer = 0;
        buffer = new char[nsize+1]; //+1 for /0
        nsize = vsnprintf(buffer, size, fmt, vl);
    }
    std::string ret(buffer);
    va_end(vl);
    delete[] buffer;
    return ret;
}

#endif
