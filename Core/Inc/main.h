/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define pRY4_Pin GPIO_PIN_13
#define pRY4_GPIO_Port GPIOC
#define pOUT_TEST_Pin GPIO_PIN_0
#define pOUT_TEST_GPIO_Port GPIOC
#define SPI1_CS_Pin GPIO_PIN_4
#define SPI1_CS_GPIO_Port GPIOA
#define pKEY_MENU_Pin GPIO_PIN_5
#define pKEY_MENU_GPIO_Port GPIOC
#define pKEY_MENU_EXTI_IRQn EXTI9_5_IRQn
#define RTC_SDA_Pin GPIO_PIN_14
#define RTC_SDA_GPIO_Port GPIOB
#define RTC_SCL_Pin GPIO_PIN_15
#define RTC_SCL_GPIO_Port GPIOB
#define pBEEP_Pin GPIO_PIN_6
#define pBEEP_GPIO_Port GPIOC
#define TXEN1_Pin GPIO_PIN_8
#define TXEN1_GPIO_Port GPIOA
#define pMODEM_EN_Pin GPIO_PIN_11
#define pMODEM_EN_GPIO_Port GPIOA
#define pRY5_Pin GPIO_PIN_15
#define pRY5_GPIO_Port GPIOA
#define pRY0_Pin GPIO_PIN_10
#define pRY0_GPIO_Port GPIOC
#define pRY1_Pin GPIO_PIN_11
#define pRY1_GPIO_Port GPIOC
#define pRY2_Pin GPIO_PIN_12
#define pRY2_GPIO_Port GPIOC
#define pRY3_Pin GPIO_PIN_2
#define pRY3_GPIO_Port GPIOD
#define pPC1_Pin GPIO_PIN_8
#define pPC1_GPIO_Port GPIOB
#define pPC2_Pin GPIO_PIN_9
#define pPC2_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
