#pragma once

#ifndef EXTERN_H_
#define EXTERN_H_

#include "stdio.h"
#include <string.h>

#include "../../class/rtc/rtc_api.h"
#include "../../class/lcd/driver/ili9488_drv.h"
#include "../../class/o3_main_page.h"
#include "../../class/data_class.h"
#include "../../class/sub_menu_string.h"
#include "../../class/spi_flash/spi_flash.h"
//#include "../../class/wind_sensor/wind_sensor.h"
#include "../../class/rs485/rs485.h"
#include "../../class/modem/modem.h"
#include "../../class/ctrl_jaesang_heater/jaesang_heater.h"
#include "../../class/ctrl_evafan/ctrl_evafan.h"
#include "../../class/img_down_load/imgdownload.h"
#include "../../class/ctrl_ozone/OZONECTRL.h"
#include "../../class/ctrl_comp/ctrl_comp.h"
#include "../../class/i2c_bm8563/I2C_BM8563.h"
#include "../../class/stm32_io/stm32_io.h"
#include "../../class/queue/queue.h"
//#include "../../class/temp_i2c/TEMPI2C.h"

#define DBG_EVENT
#define xDBG_ONE_SEC
#define xDBG_BUTTON

#define xDBG_TEST_MODE
#define xDBG_ADC

#define MINUTE  60
#define SECOND  1
#define SYSTEM_INIT_TIMEOUT 10*SECOND



#define SWAPBYTE_US(X) ((((X) & 0xFF00)>>8) | (((X) & 0x00FF)<<8))
#define BUILD_UINT16(loByte, hiByte) \
          ((int)(((loByte) & 0x00FF) + (((hiByte) & 0x00FF) << 8)))
#define HI_UINT16(a) (((a) >> 8) & 0xFF)
#define LO_UINT16(a) ((a) & 0xFF)
//#define CHECK_BIT(var,pos) ((var) & (1<<(pos)))
#define _ClearBit(Data, loc)   ((Data) &= ~(0x1<<(loc)))             // 한 bit Clear
#define _SetBit(Data, loc)     ((Data) |= (0x01 << (loc)))           // 한 bit Set
#define _InvertBit(Data, loc)  ((Data) ^= (0x1 << (loc)))             // 한 bit 반전
#define _CheckBit(Data, loc)   ((Data) & (0x01 << (loc)))            // 비트 검사
#define ON 1
#define OFF 0

#define RET_OK 1
#define RET_ERR 0
//10K Thermist
#define OHM_LIMIT_LO 0     //0R  OHM
#define OHM_LIMIT_HI 40000 //40K OHM

extern ILI9488_DRV *pIli9488;
extern RS485 *pRS485;
extern SPI1_FLASH *pFlashClass;
extern O3_MAIN_PAGE *pMain_page;
extern STM32_IO *pSTM32_IO;
extern COMP_CTRL *pCOMP;
extern OZONE_CTRL *pOZONE;
extern EVAFAN *pEVAFAN;
extern img_down_load *pIMG_DOWN_LOAD;
extern DATA_CLASS *pDataClass;
extern JAESANG *pJAESANG;
extern MODEM *pMODEM;
extern RTC_API *pRTC_API;
extern I2C_BM8563 *pHYM8563;
extern queue *pQUEUE;

//extern TEMP_I2C *pSHT20;
//extern WIND_SENSOR *pWind;
extern const MENU_STRING_INFO UI_Info;

extern uint8_t OneSecond_RTC_IRQ;
//extern uint8_t Update_protect_flag;
extern uint16_t Update_protect_cnt;
extern uint8_t update_graph;
extern uint8_t on_KeyEvent;
extern uint8_t off_KeyEvent;
//extern int16_t encoder_position;
extern vector<RECORD_UNIT> vRec;
//extern const char *sub_menu_name[5];

extern uint8_t TIM1_KeyEvent;
#endif
