################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../class/temp_i2c/TEMPI2C.cpp 

OBJS += \
./class/temp_i2c/TEMPI2C.o 

CPP_DEPS += \
./class/temp_i2c/TEMPI2C.d 


# Each subdirectory must supply rules for building sources it contributes
class/temp_i2c/%.o class/temp_i2c/%.su: ../class/temp_i2c/%.cpp class/temp_i2c/subdir.mk
	arm-none-eabi-g++ "$<" -mcpu=cortex-m3 -std=gnu++14 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -Ofast -ffunction-sections -fdata-sections -fno-exceptions -fno-rtti -fno-use-cxa-atexit -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-class-2f-temp_i2c

clean-class-2f-temp_i2c:
	-$(RM) ./class/temp_i2c/TEMPI2C.d ./class/temp_i2c/TEMPI2C.o ./class/temp_i2c/TEMPI2C.su

.PHONY: clean-class-2f-temp_i2c

