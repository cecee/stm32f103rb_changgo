################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../class/data_class.cpp \
../class/o3_main_page.cpp 

OBJS += \
./class/data_class.o \
./class/o3_main_page.o 

CPP_DEPS += \
./class/data_class.d \
./class/o3_main_page.d 


# Each subdirectory must supply rules for building sources it contributes
class/%.o class/%.su: ../class/%.cpp class/subdir.mk
	arm-none-eabi-g++ "$<" -mcpu=cortex-m3 -std=gnu++14 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -Ofast -ffunction-sections -fdata-sections -fno-exceptions -fno-rtti -fno-use-cxa-atexit -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-class

clean-class:
	-$(RM) ./class/data_class.d ./class/data_class.o ./class/data_class.su ./class/o3_main_page.d ./class/o3_main_page.o ./class/o3_main_page.su

.PHONY: clean-class

