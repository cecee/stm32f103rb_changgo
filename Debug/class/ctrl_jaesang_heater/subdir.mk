################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../class/ctrl_jaesang_heater/jaesang_heater.cpp 

OBJS += \
./class/ctrl_jaesang_heater/jaesang_heater.o 

CPP_DEPS += \
./class/ctrl_jaesang_heater/jaesang_heater.d 


# Each subdirectory must supply rules for building sources it contributes
class/ctrl_jaesang_heater/%.o class/ctrl_jaesang_heater/%.su: ../class/ctrl_jaesang_heater/%.cpp class/ctrl_jaesang_heater/subdir.mk
	arm-none-eabi-g++ "$<" -mcpu=cortex-m3 -std=gnu++14 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -Ofast -ffunction-sections -fdata-sections -fno-exceptions -fno-rtti -fno-use-cxa-atexit -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-class-2f-ctrl_jaesang_heater

clean-class-2f-ctrl_jaesang_heater:
	-$(RM) ./class/ctrl_jaesang_heater/jaesang_heater.d ./class/ctrl_jaesang_heater/jaesang_heater.o ./class/ctrl_jaesang_heater/jaesang_heater.su

.PHONY: clean-class-2f-ctrl_jaesang_heater

