################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../class/temp_sensor/wind_sensor.cpp 

OBJS += \
./class/temp_sensor/wind_sensor.o 

CPP_DEPS += \
./class/temp_sensor/wind_sensor.d 


# Each subdirectory must supply rules for building sources it contributes
class/temp_sensor/%.o: ../class/temp_sensor/%.cpp class/temp_sensor/subdir.mk
	arm-none-eabi-g++ "$<" -mcpu=cortex-m3 -std=gnu++14 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -Ofast -ffunction-sections -fdata-sections -fno-exceptions -fno-rtti -fno-use-cxa-atexit -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-class-2f-temp_sensor

clean-class-2f-temp_sensor:
	-$(RM) ./class/temp_sensor/wind_sensor.d ./class/temp_sensor/wind_sensor.o

.PHONY: clean-class-2f-temp_sensor

