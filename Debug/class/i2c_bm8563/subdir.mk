################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../class/i2c_bm8563/I2C_BM8563.cpp 

OBJS += \
./class/i2c_bm8563/I2C_BM8563.o 

CPP_DEPS += \
./class/i2c_bm8563/I2C_BM8563.d 


# Each subdirectory must supply rules for building sources it contributes
class/i2c_bm8563/%.o class/i2c_bm8563/%.su: ../class/i2c_bm8563/%.cpp class/i2c_bm8563/subdir.mk
	arm-none-eabi-g++ "$<" -mcpu=cortex-m3 -std=gnu++14 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -Ofast -ffunction-sections -fdata-sections -fno-exceptions -fno-rtti -fno-use-cxa-atexit -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-class-2f-i2c_bm8563

clean-class-2f-i2c_bm8563:
	-$(RM) ./class/i2c_bm8563/I2C_BM8563.d ./class/i2c_bm8563/I2C_BM8563.o ./class/i2c_bm8563/I2C_BM8563.su

.PHONY: clean-class-2f-i2c_bm8563

