################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../class/img_down_load/imgdownload.cpp 

OBJS += \
./class/img_down_load/imgdownload.o 

CPP_DEPS += \
./class/img_down_load/imgdownload.d 


# Each subdirectory must supply rules for building sources it contributes
class/img_down_load/%.o class/img_down_load/%.su: ../class/img_down_load/%.cpp class/img_down_load/subdir.mk
	arm-none-eabi-g++ "$<" -mcpu=cortex-m3 -std=gnu++14 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -Ofast -ffunction-sections -fdata-sections -fno-exceptions -fno-rtti -fno-use-cxa-atexit -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-class-2f-img_down_load

clean-class-2f-img_down_load:
	-$(RM) ./class/img_down_load/imgdownload.d ./class/img_down_load/imgdownload.o ./class/img_down_load/imgdownload.su

.PHONY: clean-class-2f-img_down_load

