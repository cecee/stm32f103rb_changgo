################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../class/lcd/driver/lcd_io_gpio8.c 

CPP_SRCS += \
../class/lcd/driver/ili9488_drv.cpp 

C_DEPS += \
./class/lcd/driver/lcd_io_gpio8.d 

OBJS += \
./class/lcd/driver/ili9488_drv.o \
./class/lcd/driver/lcd_io_gpio8.o 

CPP_DEPS += \
./class/lcd/driver/ili9488_drv.d 


# Each subdirectory must supply rules for building sources it contributes
class/lcd/driver/%.o class/lcd/driver/%.su: ../class/lcd/driver/%.cpp class/lcd/driver/subdir.mk
	arm-none-eabi-g++ "$<" -mcpu=cortex-m3 -std=gnu++14 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -Ofast -ffunction-sections -fdata-sections -fno-exceptions -fno-rtti -fno-use-cxa-atexit -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
class/lcd/driver/%.o class/lcd/driver/%.su: ../class/lcd/driver/%.c class/lcd/driver/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-class-2f-lcd-2f-driver

clean-class-2f-lcd-2f-driver:
	-$(RM) ./class/lcd/driver/ili9488_drv.d ./class/lcd/driver/ili9488_drv.o ./class/lcd/driver/ili9488_drv.su ./class/lcd/driver/lcd_io_gpio8.d ./class/lcd/driver/lcd_io_gpio8.o ./class/lcd/driver/lcd_io_gpio8.su

.PHONY: clean-class-2f-lcd-2f-driver

